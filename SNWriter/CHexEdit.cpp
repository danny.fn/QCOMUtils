// CHexEdit.cpp : 实现文件
//

#include "stdafx.h"
#include "SNWriter.h"
#include "CHexEdit.h"


// CHexEdit

IMPLEMENT_DYNAMIC(CHexEdit, CEdit)

CHexEdit::CHexEdit()
{
    m_nMode = -1;
    m_bIMEI15Enable = 0;
}

CHexEdit::~CHexEdit()
{
}

void CHexEdit::OnModeChange()
{
    SetWindowText(_T(""));
    switch(m_nMode){
    case CHEXEDIT_MODE_MEID:
        SetLimitText(CHEXEDIT_LIM_MEID);
        break;
    case CHEXEDIT_MODE_ESN:
        SetLimitText(CHEXEDIT_LIM_ESN);
        break;
    default:
    case CHEXEDIT_MODE_IMEI:
        SetLimitText(CHEXEDIT_LIM_IMEI+m_bIMEI15Enable);
        break;
    }
}

BEGIN_MESSAGE_MAP(CHexEdit, CEdit)
    ON_WM_CHAR()
END_MESSAGE_MAP()



// CHexEdit 消息处理程序



void CHexEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
    if(nChar >= ' ')
    {
        if(m_nMode == CHEXEDIT_MODE_IMEI)
        {
            if(nChar<'0' || nChar >'9')
            {
                return;
            }
        }
        else
        {
            if((nChar<'0' || nChar >'9') && (nChar<'a' || nChar >'f') && (nChar<'A' || nChar >'F'))
            {
                return;
            }
        }
    }

    CEdit::OnChar(nChar, nRepCnt, nFlags); 
}
