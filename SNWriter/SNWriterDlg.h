// SNWriterDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "Diagnostic.h"
#include "CHexEdit.h"
#include "COMManager.h"

#define SETFOCUS_TIMER_ID   0x80000001
#define SETFOCUS_TIME       10

#define SN_INI_APP_NAME           _T("SN_SET")
#define SN_INI_MODE_KEY           _T("SN_MODE")
#define SN_INI_MODE_DEF_STR       _T("0")
#define SN_INI_MODE_DEF_INT       0
#define SN_INI_IMEI2_KEY          _T("IMEI2_ENABLE")
#define SN_INI_IMEI2_DEF_STR      _T("1")
#define SN_INI_IMEI2_DEF_INT      1

#define SN_INI_IMEI15_KEY         _T("IMEI15_ENABLE")
#define SN_INI_IMEI15_DEF_STR     _T("0")
#define SN_INI_IMEI15_DEF_INT     0

#define SN_INI_RANDOMSPC_KEY      _T("RANDOMSPC_ENABLE")
#define SN_INI_RANDOMSPC_DEF_STR  _T("0")
#define SN_INI_RANDOMSPC_DEF_INT  0

// CSNWriterDlg 对话框
class CSNWriterDlg : public CDialog
{
// 构造
public:
	CSNWriterDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_SNWRITER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
    virtual void OnCancel();
    virtual void OnOK();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

protected:
    COMManager     *m_pCOMMgr;
    CDiagnostic     m_PhoneDiag;
    CString         m_szIMEI1;
    CString         m_szInput1;
    CString         m_szInput2;
    CStringArray    m_arMode;
    CBitmap         m_BmpPass;
    CBitmap         m_BmpFail;
    int             m_nPhoneStatus;
    int             m_nCOMPortIdx;
    int             m_bIMEI2Enable;
    int             m_bIMEI15Enable;
    int             m_bRandomSPCEnable;
    int             m_nIMEIMode;
    int             m_DefCOMID;
    BOOL            m_bWriteFromOKBtn;
    
protected:
    void            PhoneStateChange(int nPortID, int nNewState);
    BOOL            IsHexAvaliable(CString &szCode);
    BOOL            IsNumAvaliable(CString &szCode);
    byte            CharToHex(TCHAR ch);
    TCHAR           HexToChar(byte Hex);
    BOOL            WriteSN(void);
    void            ReadSN(void);

    // IMEI
    BOOL            IsIMEIAvaliable(CString &szCode);
    BOOL            WriteIMEI(void);
    BOOL            WriteIMEI1(void);
    BOOL            WriteIMEI2(void);
    CString         ReadIMEI(void);
    CString         ReadIMEI1(void);
    CString         ReadIMEI2(void);
    void            FormatIMEI(CString &szCode, nv_ue_imei_type *pIMEI);
    void            StringIMEI(CString &szCode, nv_ue_imei_type *pIMEI);
    byte            CalcIMEISP(CString &szCode);

    BOOL            IsMEIDAvaliable(CString &szCode);
    BOOL            WriteMEID(void);
    CString         ReadMEID(void);
    void            FormatMEID(CString &szCode, byte *pMEID);
    void            StringMEID(CString &szCode, byte *pMEID);
    byte            CalcMEIDSP(CString &szCode);

    BOOL            IsESNAvaliable(CString &szCode);
    BOOL            WriteESN(void);
    CString         ReadESN(void);
    void            FormatESN(CString &szCode, dword *pESN);
    void            StringESN(CString &szCode, dword *pESN);

    void            ShowMessageBox(int nResID);
    void            NewPhoneConnected(void);
    void            PhoneDisConnected(void);
    void            RecordHistory(CString &szCode);
    static void     PhoneStateCB(LPVOID pParam, int nPortID, int nNewState);

public:
    CButton         m_cOKButton;
    CComboBox       m_cCOMCombo;
    CHexEdit        m_cIMEI1Edit;
    CEdit           m_cIMEI2Edit;
    CButton         m_IMEI2Check;
    CButton         m_IMEI15Check;
    CButton         m_RandomSPCCheck;
    CStatic         m_cIMEI1Static;
    CStatic         m_cIMEI2Static;
    CStatic         m_cStatus;
    CStatic         m_cIMEI1ValStatic;
    CStatic         m_cIMEI2ValStatic;
    CStatic         m_cIMEI1StatusStatic;
    CStatic         m_cIMEI2StatusStatic;
    
    afx_msg void OnBnClickedButtonOk();
    afx_msg void OnDestroy();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnCbnDropdownComboComlist();
    afx_msg void OnCbnCloseupComboComlist();
    afx_msg void OnBnClickedImei2Check();
    afx_msg void OnBnClickedRadioImei();
    afx_msg void OnBnClickedRadioMeid();
    afx_msg void OnBnClickedRadioEsn();
    afx_msg void OnEnChangeEditImei1();
    afx_msg void OnEnChangeEditImei2();
    afx_msg void OnBnClickedImei15();
    afx_msg void OnBnClickedButtonClear();
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnBnClickedRandomspcCheck();
    afx_msg LRESULT OnMyUpdateState(WPARAM wParam, LPARAM lParam);
};
