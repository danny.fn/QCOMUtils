/*=================================================================================================

  FILE : DLoadPkt.H

  SERVICES: Declaration of some struct for efs operatoin in diag.

  GENERAL DESCRIPTION:
  
  PUBLIC CLASSES AND STATIC FUNCTIONS: 
    
  INITIALIZATION AND SEQUENCING REQUIREMENTS:
  
====================================================================================================*/


/*====================================================================================================

                                       ==MODIFY HISTOY FOR FILE==

  when         who     what, where, why
  ----------   ---     ----------------------------------------------------------
  5/28/2004   tyz      create
  
=====================================================================================================*/
#if !defined(_DLOADPKT_H__)
#define _DLOADPKT_H__

#include "comdef.h"
#include "diagdiag.h"

#pragma pack(1)  /* Notice: log packets are BYTE packed. */

//有的编译器需要在声明是增加PACKED,才使用#pragma pack(n)指定的长度封装该struct和union
//#ifndef PACKED
//#define PACKED
//#endif

/*===========================================================================

                  INCLUDE FILES FOR MODULE
  
===========================================================================*/


/*===========================================================================

                  DEFINITIONS AND CONSTANTS
  
===========================================================================*/
// ***********************************DLOAD Command code**********************************************
// DLOAD 采用Big Endian编码
#define  PKT_BUF_SIZE           (0x400)

typedef PACKED enum
{
   CMD_WRITE  = 0x01,   /* Write a block of data to memory (received)    */
   CMD_ACK    = 0x02,   /* Acknowledge receiving a packet  (transmitted) */
   CMD_NAK    = 0x03,   /* Acknowledge a bad packet        (transmitted) */
   CMD_ERASE  = 0x04,   /* Erase a block of memory         (received)    */
   CMD_GO     = 0x05,   /* Begin execution at an address   (received)    */
   CMD_NOP    = 0x06,   /* No operation, for debug         (received)    */
   CMD_PREQ   = 0x07,   /* Request implementation info     (received)    */
   CMD_PARAMS = 0x08,   /* Provide implementation info     (transmitted) */
   CMD_DUMP   = 0x09,   /* Debug: dump a block of memory   (received)    */
   CMD_RESET  = 0x0A,   /* Reset the phone                 (received)    */
   CMD_UNLOCK = 0x0B,   /* Unlock access to secured ops    (received)    */
   CMD_VERREQ = 0x0C,   /* Request software version info   (received)    */
   CMD_VERRSP = 0x0D,   /* Provide software version info   (transmitted) */
   CMD_PWROFF = 0x0E,   /* Turn phone power off            (received)    */
   CMD_WRITE_32BIT = 0x0F,  /* Write a block of data to 32-bit memory 
                               address (received)                        */
   CMD_MEM_DEBUG_QUERY = 0x10, /* Memory debug query       (received)    */
   CMD_MEM_DEBUG_INFO  = 0x11, /* Memory debug info        (transmitted) */
   CMD_MEM_READ_REQ    = 0x12, /* Memory read request      (received)    */
   CMD_MEM_READ_RESP   = 0x13, /* Memory read response     (transmitted) */
   CMD_DLOAD_SWITCH = 0x3A,
   CMD_DLOAD_RESETMODE = 0x88
} cmd_code_type;

#define RESET_RECOVERY_MODE   0x77665502
#define RESET_FASTBOOT_MODE   0x77665500
/*-------------------------------------------------------------------------*/

/* For the received packets, we need the minimum length of a valid packet
   of that type (except where the packet is only a command code). Note,
   size is in bytes */

#define  WRITE_SIZ   8  /* Minimum size of the Write packet */
#define  ERASE_SIZ   9  /* Total size of the Erase packet   */
#define  GO_SIZ      7  /* Total size of the Go packet      */
#define  UNLOCK_SIZ  9  /* Total size of the unlock packet  */
#define  WRITE_32BIT_SIZ   9  /* Minimum size of the Write_32bit packet */

/*-------------------------------------------------------------------------*/

typedef PACKED enum
  {
  DL_ACK=0x0,                    /* Success. Send an acknowledgement.           */
  DL_NAK_INVALID_FCS=0x1,        /* Failure: invalid frame check sequence.      */
  DL_NAK_INVALID_DEST=0x2,       /* Failure: destination address is invalid.    */
  DL_NAK_INVALID_LEN=0x3,        /* Failure: operation length is invalid.       */
  DL_NAK_EARLY_END=0x4,          /* Failure: packet was too short for this cmd. */
  DL_NAK_TOO_LARGE=0x5,          /* Failure: packet was too long for my buffer. */
  DL_NAK_INVALID_CMD=0x6,        /* Failure: packet command code was unknown.   */
  DL_NAK_FAILED=0x7,             /* Failure: operation did not succeed.         */
  DL_NAK_WRONG_IID=0x8,          /* Failure: intelligent ID code was wrong.     */
  DL_NAK_BAD_VPP=0x9,            /* Failure: programming voltage out of spec    */
  DL_NAK_OP_NOT_PERMITTED=0xA,   /* Failure: memory dump not permitted          */
  DL_NAK_INVALID_ADDR=0xB,       /* Failure: invalid address for a memory read  */
  DL_NAK_VERIFY_FAILED=0xC,      /* Failure: readback verify did not match      */
  DL_NAK_NO_SEC_CODE=0xD,        /* Failure: not permitted without unlock       */
  DL_NAK_BAD_SEC_CODE=0xE        /* Failure: invalid security code              */
  } dl_response_code_type;


typedef PACKED struct
{
    BYTE        cmd_code;
    BYTE        addr_1;
    BYTE        addr_2;
    BYTE        addr_3;
    BYTE        len_1;
    BYTE        len_2;
    BYTE        data[PKT_BUF_SIZE];
}diag_dload_write_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
    BYTE        addr_1;
    BYTE        addr_2;
    BYTE        addr_3;
    BYTE        len_1;
    BYTE        len_2;
    BYTE        len_3;
}diag_dload_erase_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
    BYTE        addr_1;
    BYTE        addr_2;
    BYTE        addr_3;
    BYTE        addr_4;
}diag_dload_go_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
}diag_dload_nop_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
}diag_dload_param_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
}diag_dload_ver_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
}diag_dload_reset_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
    BYTE        unlock[DIAG_PASSWORD_SIZE];
}diag_dload_unlock_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
}diag_dload_poweroff_req_type;

typedef PACKED struct
{
    BYTE        cmd_code;
    BYTE        addr_1;
    BYTE        addr_2;
    BYTE        addr_3;
    BYTE        addr_4;
    BYTE        len_1;
    BYTE        len_2;
    BYTE        data[PKT_BUF_SIZE];
}diag_dload_write32bit_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_dload_ack_type;


typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        reason_msb;
    BYTE        reason_lsb;
} diag_dload_nak_type;

typedef PACKED struct
{
    BYTE        cmd_code;
    BYTE        protocol_id;
    BYTE        min_protocol_id;
    WORD        max_write_size;
    BYTE        mob_model;
    BYTE        bb_ver;
    BYTE        dummy;
} diag_dload_param_rsp_type;

typedef PACKED struct
{
    BYTE        cmd_code;
    BYTE        len;
    BYTE        ver[0x100];
} diag_dload_ver_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        mode_1;
    BYTE        mode_2;
    BYTE        mode_3;
    BYTE        mode_4;
} diag_dload_reset_mode_req_type;

// ***********************************ARMPRG Command code**********************************************
#include "ap_packet.h"
#include "ap_armprg.h"
// ARM PRG 采用Little Endian编码
//--------------------------------------------------------------------------
// Defines
//--------------------------------------------------------------------------

#define DEVICE_UNKNOWN  0xFF

#define STREAM_DLOAD_MAX_VER            0x04
#define STREAM_DLOAD_MIN_VER            0x02
#define UNFRAMED_DLOAD_MIN_VER          0x04
#define FEATURE_UNCOMPRESSED_DLOAD      0x00000001
#define FEATURE_NAND_PRIMARY_IMAGE      0x00000002
#define FEATURE_NAND_BOOTLOADER_IMAGE   0x00000004
#define FEATURE_NAND_MULTI_IMAGE        0x00000008

#define SUPPORTED_FEATURES (FEATURE_UNCOMPRESSED_DLOAD | FEATURE_NAND_MULTI_IMAGE)

/* Command/Rsp codes, DUMMY_RSP should alwaus be the last one */
#define AP_HELLO_RSP                    0x02
#define AP_READ_RSP                     0x04
#define AP_WRITE_RSP                    0x06
#define AP_STREAM_WRITE_RSP             0x08
#define AP_NOP_RSP                      0x0A
#define AP_RESET_RSP                    0x0C
#define AP_ERROR_RSP                    0x0D
#define AP_CMD_LOG                      0x0E
#define AP_UNLOCK_RSP                   0x10
#define AP_PWRDOWN_RSP                  0x12
#define AP_OPEN_RSP                     0x14
#define AP_CLOSE_RSP                    0x16
#define AP_SECURITY_MODE_RSP            0x18
#define AP_PARTITION_TABLE_RSP          0x1A
#define AP_OPEN_MULTI_IMAGE_RSP         0x1C 

#define AP_HELLO_CMD                    0x01
#define AP_READ_CMD                     0x03
#define AP_WRITE_CMD                    0x05
#define AP_STREAM_WRITE_CMD             0x07
#define AP_NOP_CMD                      0x09
#define AP_RESET_CMD                    0x0B
#define AP_UNLOCK_CMD                   0x0F
#define AP_PWRDOWN_CMD                  0x11
#define AP_OPEN_CMD                     0x13
#define AP_CLOSE_CMD                    0x15
#define AP_SECURITY_MODE_CMD            0x17
#define AP_PARTITION_TABLE_CMD          0x19
#define AP_OPEN_MULTI_IMAGE_CMD         0x1B 
#define AP_UNFRAMED_STREAM_WRITE_CMD    0x30 /* Needed for command parsing */
#define AP_UNFRAMED_STREAM_WRITE_RSP    0x31

#define AP_HOST_HEADER "QCOM fast download protocol host";
#define AP_TARG_HEADER "QCOM fast download protocol targ";
#define AP_HELLO_HEADER_LEN 32
#define AP_FLASHNAME_LEN    128
#define MAX_BLOCKS_IN_REPLY 512

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        host_header[AP_HELLO_HEADER_LEN];
    BYTE        max_ver;
    BYTE        min_ver;
    BYTE        req_features;
} diag_ap_hello_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        target_header[AP_HELLO_HEADER_LEN];
    BYTE        max_ver;
    BYTE        min_ver;
    DWORD       preferred_block_size;
    DWORD       base_addr;
    BYTE        flashnamelen;
    BYTE        flashname[AP_FLASHNAME_LEN];
    WORD        windowsize;
    WORD        num_blocks;
    DWORD       blocks[MAX_BLOCKS_IN_REPLY];
    BYTE        support_features;
} diag_ap_hello_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    DWORD       dwAddr;
    WORD        wLen;
} diag_ap_read_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    DWORD       addr;
    BYTE        data[MAX_DATA_LENGTH];
} diag_ap_read_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    DWORD       dwAddr;
    BYTE        data[MAX_DATA_LENGTH];
} diag_ap_write_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    DWORD       addr;
} diag_ap_write_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    DWORD       dwAddr;
    BYTE        data[MAX_DATA_LENGTH];
} diag_ap_stream_write_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    DWORD       addr;
} diag_ap_stream_write_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        data[MAX_DATA_LENGTH];
} diag_ap_sync_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        data[MAX_DATA_LENGTH];
} diag_ap_sync_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_ap_reset_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_ap_reset_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_ap_poweroff_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_ap_poweroff_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        mode;
} diag_ap_open_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_ap_open_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_ap_close_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_ap_close_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        mode;
} diag_ap_sec_mode_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
} diag_ap_sec_mode_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        boverride;
    BYTE        table[512];
} diag_ap_parti_tbl_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        status;
} diag_ap_parti_tbl_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        mode;
    BYTE        data[MAX_DATA_LENGTH];
} diag_ap_open_multi_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    BYTE        status;
} diag_ap_open_multi_rsp_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    byte        dummy1;
    byte        dummy2;
    byte        dummy3;
    DWORD       dwAddr;
    DWORD       dwLen;
    BYTE        data[MAX_DATA_LENGTH];
} diag_ap_unframed_stream_write_req_type;

typedef PACKED struct 
{
    BYTE        cmd_code;                  /* Command code */
    DWORD       addr;
} diag_ap_unframed_stream_write_rsp_type;

#pragma pack()
/////////////////////////////////////////////////////////////////////////////
#endif // !defined(_DLOADPKT_H__)