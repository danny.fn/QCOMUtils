#pragma once
#include "afx.h"
#include <afxmt.h>
#include "HDLCFrame.h"

#define STREAMFRAME_TIMEOUT_MS  10000

class CHDLCFrameLink
{
public:
    CHDLCFrameLink(void)    {m_pNext = NULL; m_dwKey=0; m_dwTimeStamp=0; m_nFrameIndex=0;};
    CHDLCFrameLink     *m_pNext;
    DWORD               m_dwKey;
    DWORD               m_dwTimeStamp;
    int                 m_nFrameIndex;
    CHDLCFrame          m_Frame;
};

class CStreamFrames
{
public:
    CStreamFrames(void);
    ~CStreamFrames(void);
    
    BOOL                Create(int nWindowSize, DWORD dwTimeoutMS=STREAMFRAME_TIMEOUT_MS);
    BOOL                Release(void);
    CHDLCFrame         *InsertFrame(DWORD dwKey, void *pReqData=NULL, int nReqSize=0, BOOL bHeadEnc = TRUE, BOOL bHDLCEncode = TRUE);
    BOOL                RemoveFrame(DWORD dwKey);
    CHDLCFrame         *GetTimeoutFrame(void);
    BOOL                IsStreamComplete(void)  {return ((m_pWait==NULL)?TRUE:FALSE);};
    BOOL                IsFullWindows(void)     {return ((m_pFree==NULL)?TRUE:FALSE);};
    
private:
    void                InitParam(void);
    void                InitLink(void);
    CHDLCFrameLink     *GetLink(void);
    BOOL                FreeLink(CHDLCFrameLink* pLink);
    CHDLCFrameLink     *PeekTimeoutLink(void);

private:
    BOOL                m_bValid;
    CCriticalSection    m_CritiSect;
    CHDLCFrameLink     *m_pArray;
    CHDLCFrameLink     *m_pWait;
    CHDLCFrameLink     *m_pFree;
    int                 m_nWindowSize;
    int                 m_nFrameIndex;
    DWORD               m_dwTimeoutMS; // GetTickCount();
};
