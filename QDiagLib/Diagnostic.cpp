/*=================================================================================================

FILE : Diagnostic.C

SERVICES: Declaration of an MFC wrapper class for Diagnostic Class on DM

GENERAL DESCRIPTION:

PUBLIC CLASSES AND STATIC FUNCTIONS:
CDiagnostic;

INITIALIZATION AND SEQUENCING REQUIREMENTS:

====================================================================================================*/


/*====================================================================================================

==MODIFY HISTOY FOR FILE==

when         who     what, where, why
----------   ---     ----------------------------------------------------------
04/28/2004   tyz      (tyz040528)Add DLOAD mode process
12/28/2003   tyz      Create

=====================================================================================================*/
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "stdafx.h"
#include "Diagnostic.h"
#include "helper.h"
#include "COMManager.h"
#include "fs_fcntl.h"
#include "Log.h"

/*===========================================================================

DEFINITIONS AND CONSTANTS

===========================================================================*/
#define RX_SUCCESS_SYNC     (TRUE+1)
/*============================================================================
This is a call-back function of serial port event occur.
Diag response packets are proccessed in this  funtion.
Modify the switch-case sentence, add the actual proccess functions to proccess
the special response packet.

If proccess is successful the return value is 0,
else if the receive packet is not a right HDLC packet the return value is 256,
else the return value is the command code.

NOTICE: So all response proccessing functions are invoked by this should return
0 or cmd_code!
=============================================================================*/
void CDiagnostic::RspPacketCB(LPVOID pParam, DWORD dwFrameSize)
{
    CDiagnostic* pThis = (CDiagnostic*)pParam;
    if(pThis->m_bDiagStarted)
    {
        pThis->ReceiveFrame((WORD)dwFrameSize);
    }
}

void CDiagnostic::ReceiveFrame(WORD wFrameSize)
{
    BYTE *pData = m_rspFrame.GetHDLCFrame();
    
    if(wFrameSize < 4 || wFrameSize > DEFAULT_MAX_FRAME_SIZE)
    {
#if 0
        // 数据传输错误，立即启动重发机制
        m_COMPort.ReadFrame(NULL);
        m_bRXSuccess = FALSE;
        LOG_OUTPUT(_T("Frame TOO short!\n"));
        if(m_CommandMode != CMD_MODE_STREAM)
        {
            SetEvent(m_COMPort.GetTXEvent());
        }
#else
        // 数据传输错误，忽略，以防止是因为串口误发而导致的问题
        m_COMPort.ReadFrame(NULL);
#endif
        return;
    }

    BYTE  req_cmd_code = *m_reqFrame.GetOriginData();
    m_COMPort.ReadFrame(pData, wFrameSize);
    if(FALSE == m_rspFrame.Decode((char*) pData,wFrameSize))
    {
        if(m_CommandMode == CMD_MODE_DIAG && req_cmd_code ==  DIAG_STATUS_F)
        {
            m_bRXSuccess = TRUE; // 发现有些机器CRC校验码一定返回错误，因此，这里对于PROBE命令不检测解码CRC
        }
        else
        {
            m_bRXSuccess = FALSE;
        }
        if(m_CommandMode != CMD_MODE_STREAM)
        {
            SetEvent(m_COMPort.GetTXEvent());
        }
        return;
    }
    
    BYTE  rsp_cmd_code = *m_rspFrame.GetOriginData();

    if(m_CommandMode == CMD_MODE_DIAG)
    {
        m_nDiagErr = 0;
        if(rsp_cmd_code != req_cmd_code && req_cmd_code !=  DIAG_STATUS_F)
        {
            switch( rsp_cmd_code ) 
            {
            case DIAG_BAD_CMD_F:
                m_nDiagErr = rsp_cmd_code;
                break;
            case DIAG_BAD_PARM_F:
                m_nDiagErr = rsp_cmd_code;
                break;
            case DIAG_BAD_LEN_F:
                m_nDiagErr = rsp_cmd_code;
                break;
            case DIAG_BAD_MODE_F:
                m_nDiagErr = rsp_cmd_code;
                break;
            case DIAG_BAD_SPC_MODE_F:
                m_nDiagErr = rsp_cmd_code;
                break;
            case DIAG_BAD_SEC_MODE_F:
                m_nDiagErr = rsp_cmd_code;
                break;
            default:
                return;
            }
        }
    }
    else if(m_CommandMode == CMD_MODE_DLOAD)
    {
        if(FALSE == ProcessDLoadRsp())
        {
            return;
        }
    }
    else if(m_CommandMode == CMD_MODE_ARMPRG)
    {
        if(FALSE == ProcessAPRsp())
        {
            return;
        }
    }
    else if(m_CommandMode == CMD_MODE_STREAM)
    {
        switch(m_dwStreamMode){
        case CMD_MODE_ARMPRG:
            m_nAPErr = 0;
            switch(m_dwStreamCommand){
            case AP_STREAM_WRITE_CMD:
                if(rsp_cmd_code == AP_STREAM_WRITE_RSP)
                {
                    diag_ap_stream_write_rsp_type *pStreamRsp = (diag_ap_stream_write_rsp_type *)m_rspFrame.GetOriginData();
                    m_StreamFrames.RemoveFrame(pStreamRsp->addr);
                    SetEvent(m_hStreamEvent);
                }
                break;
            case AP_UNFRAMED_STREAM_WRITE_CMD:
                if(rsp_cmd_code == AP_UNFRAMED_STREAM_WRITE_RSP)
                {
                    diag_ap_unframed_stream_write_rsp_type *pStreamRsp = (diag_ap_unframed_stream_write_rsp_type *)m_rspFrame.GetOriginData();
                    m_StreamFrames.RemoveFrame(pStreamRsp->addr);
                    SetEvent(m_hStreamEvent);
                }
                break;
            default:
                return;
            }
            break;

        case CMD_MODE_DIAG:
            m_nDiagErr = 0;
            break;

        case CMD_MODE_DLOAD:
            m_nDLoadErr = 0;
            break;

        case CMD_MODE_STREAM:
        default:
            LOG_OUTPUT(_T("Unexcept Stream Mode %d COM%d!\n"),m_dwStreamMode, GetPortID()+1);
            return;
        }
    }
    else
    {
        LOG_OUTPUT(_T("Unexcept COMMAND Mode %d COM%d!\n"),m_CommandMode, GetPortID()+1);
    }
    
    m_bRXSuccess = TRUE;
    if(m_CommandMode != CMD_MODE_STREAM)
    {
        SetEvent(m_COMPort.GetTXEvent());
    }
}

UINT CDiagnostic::MonitorThread(LPVOID pParam)
{
    CDiagnostic* pThis = (CDiagnostic*)pParam;
    pThis->MonitorFunc();
    return 0;
}

void CDiagnostic::MonitorFunc(void)
{
    LOG_OUTPUT(_T("CDiagnostic::MonitorFunc Start COM%d\n"),GetPortID()+1);
    while(m_bMonitor)
    {
        if(FALSE == COMManager::GetCurrentPortUsed(GetPortID()))
        {
            ConnectPhone();
        }
        WaitForSingleObject(m_hMonitorEvent, DIAG_WAIT_TIME/2);
    };
    ResetMonitorThread();
    LOG_OUTPUT(_T("CDiagnostic::MonitorFunc END COM%d\n"),GetPortID()+1);
}

/*==========================================================================

MEMBER FUNCTION DEFINATION

==========================================================================*/
CDiagnostic::CDiagnostic()
{
    m_CommandMode   = CMD_MODE_DIAG;
    m_bDiagStarted  = FALSE;
    m_nDiagErr      = 0;
    m_nEFS2Err      = 0;
    m_nEFS2MaxPath  = 0;
    m_nEFS2MaxName  = 0;
    m_nDLoadErr     = 0;
    m_nAPErr        = 0;
    m_bFastBoot     = FALSE;
    m_hStreamEvent  = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_hMonitorEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_pThread       = NULL;
    TCHAR   tSPC[DIAGPKT_SEC_CODE_SIZE+1];
    DWORD   nRet = GetPrivateProfileString(DIAG_INI_APP_NAME,DIAG_INI_SPC_KEY,DIAG_INI_SPC_DEF_STR,tSPC,(DIAGPKT_SEC_CODE_SIZE+1)*sizeof(TCHAR),DIAG_INI_FILE);
    if(_tcscmp(tSPC,DIAG_INI_SPC_DEF_STR)== 0)
    {
        WritePrivateProfileString(DIAG_INI_APP_NAME,DIAG_INI_SPC_KEY,DIAG_INI_SPC_DEF_STR,DIAG_INI_FILE);
    }
}

CDiagnostic::~CDiagnostic()
{
    StopDiag();
    CloseHandle(m_hStreamEvent);
    CloseHandle(m_hMonitorEvent);
}

BOOL CDiagnostic::StartDiag(int nPortID, BOOL bMonitor)
{
    if(m_bDiagStarted)
    {
        if(nPortID != GetPortID())
        {
            StopDiag();
            LOG_OUTPUT(_T("CDiagnostic::StartDiag Restart %d COM%d\n"),m_bDiagStarted,nPortID+1);
        }
        else
        {
            DIAG_CRITISECT_LOCK();
            if(bMonitor && m_pThread == NULL)
            {
                // 启动串口状态监视线程
                LOG_OUTPUT(_T("CDiagnostic::StartDiag Thread %d COM%d\n"),m_bDiagStarted,nPortID+1);
                ResetEvent(m_hMonitorEvent);
                m_pThread = AfxBeginThread( MonitorThread, this, THREAD_PRIORITY_NORMAL);
                m_pThread->m_bAutoDelete = TRUE;
            }
            DIAG_CRITISECT_UNLOCK();
            return TRUE;
        }
    }

    DIAG_CRITISECT_LOCK();
    m_COMPort.SetPortID(nPortID);
    m_bDiagStarted = m_COMPort.Open(RspPacketCB, this);
    m_bMonitor = m_bDiagStarted&&bMonitor;
    if(m_bMonitor)
    {
        // 启动串口状态监视线程
        ResetEvent(m_hMonitorEvent);
        m_pThread = AfxBeginThread( MonitorThread, this, THREAD_PRIORITY_NORMAL);
        m_pThread->m_bAutoDelete = TRUE;
    }
    DIAG_CRITISECT_UNLOCK();
    return m_bDiagStarted;
}

BOOL  CDiagnostic::StopDiag(void)
{
    BOOL rValue = TRUE;
    DIAG_CRITISECT_LOCK();
    if(m_bDiagStarted)
    {
        LOG_OUTPUT(_T("CDiagnostic::StopDiag %d COM%d\n"),m_bDiagStarted,GetPortID()+1);
        if(m_bMonitor)
        {
            
            m_bMonitor = FALSE;
            m_bRXSuccess    = RX_SUCCESS_SYNC;
            SetEvent(m_COMPort.GetTXEvent());
            SetEvent(m_hMonitorEvent);
            while(m_pThread){Sleep(10);}
        }
        m_bDiagStarted = FALSE;
        m_StreamFrames.Release();
        rValue = m_COMPort.Close();
        m_nDiagErr      = 0;
        m_nEFS2Err      = 0;
        m_nDLoadErr     = 0;
        m_nAPErr        = 0;
        m_bRXSuccess    = RX_SUCCESS_SYNC;
        SetEvent(m_COMPort.GetTXEvent());
        SetEvent(m_hStreamEvent);
        LOG_OUTPUT(_T("CDiagnostic::StopDiag END %d COM%d\n"),m_bDiagStarted,GetPortID()+1);
    }
    DIAG_CRITISECT_UNLOCK();
    return rValue;
}

int CDiagnostic::ConnectPhone(void)
{
    diagpkt_hdr_type    reqStatus = {DIAG_STATUS_F};
    int                 nMode = DIAG_PHONEMODE_NONE;//COMManager::GetCurrentPhoneMode(GetPortID());
    BOOL                bRet;
    BYTE                cmd_code;
    COMPortInfo         myCOMPortInfo;

    DIAG_CRITISECT_LOCK();
    if(!m_bDiagStarted)
    { 
        if(m_bFastBoot)
        {
            nMode = DIAG_PHONEMODE_FASTBOOT;
        }
        else
        {
            nMode = DIAG_PHONEMODE_NOPORT;
        }
        LOG_OUTPUT(_T("CDiagnostic::ConnectPhone %d PORT %d\n"),m_bDiagStarted,m_COMPort.GetPortID());
    }
    else
    {
        m_rspFrame.Reset();
        bRet = SendDiagPacket(&reqStatus, sizeof(diagpkt_hdr_type),1500,3);
        cmd_code = *m_rspFrame.GetOriginData();
        
        if(m_rspFrame.GetOriginDataLen()>0)
        {
            if(m_rspFrame.IsHeadEnc())
            {
                if(NULL != strstr((char *)m_rspFrame.GetOriginData(),"PBL_Downloader"))
                {
                    nMode = DIAG_PHONEMODE_EDLOAD;
                }
                else
                {
                    nMode = DIAG_PHONEMODE_ARMPRG;
                }
                m_bFastBoot = FALSE;
            }
            else if(cmd_code == DIAG_BAD_CMD_F)
            {
                nMode = DIAG_PHONEMODE_WCDMA;
                m_bFastBoot = FALSE;
            }
            else if(cmd_code == DIAG_DLOAD_RSP)
            {
                nMode = DIAG_PHONEMODE_DLOAD;
                m_bFastBoot = FALSE;
            }
            else if(cmd_code == DIAG_STATUS_F)
            {
                nMode = DIAG_PHONEMODE_CDMA;
                m_bFastBoot = FALSE;
            }
            else if(m_bFastBoot)
            {
                nMode = DIAG_PHONEMODE_FASTBOOT;
            }

            LOG_OUTPUT(_T("CDiagnostic::ConnectPhone MODE %d COM%d\n"),nMode,GetPortID()+1);
        }
        else
        {
            if(m_bFastBoot)
            {
                nMode = DIAG_PHONEMODE_FASTBOOT;
            }
        }
    }
    DIAG_CRITISECT_UNLOCK();
    myCOMPortInfo.m_dwHubIndex = m_COMPort.GetHubIndex();
    myCOMPortInfo.m_dwPortIndex = m_COMPort.GetPortIndex();
    COMManager::UpdatePhoneMode(GetPortID(),nMode, myCOMPortInfo);
    return nMode;
}

BOOL CDiagnostic::SetStreamWindowSize(int nSize)
{
    if(FALSE == IsStreamComplete())
    {
        return FALSE;
    }
    return m_StreamFrames.Create(nSize);
}

BOOL CDiagnostic::SendTimeoutStreamFrames(void)
{
    CHDLCFrame *pTimeoutFrame;
    if(FALSE == m_StreamFrames.IsFullWindows())
    {
        pTimeoutFrame = m_StreamFrames.GetTimeoutFrame();
        if(pTimeoutFrame)
        {
            if(FALSE == SendStreamPacket(pTimeoutFrame->GetHDLCFrame(),pTimeoutFrame->GetHDLCFrameLen()))
            {
                return FALSE;
            }
        }
    }
    else
    {
        return FALSE;
    }
    return TRUE;
}

BOOL CDiagnostic::SwitchStreamMode(DWORD dwStreamMode, DWORD dwStreamCommand)
{
    if(m_dwStreamMode == dwStreamMode && m_dwStreamCommand == dwStreamCommand)
    {
        return TRUE;
    }

    if(FALSE == IsStreamComplete())
    {
        return FALSE;
    }
    m_dwStreamMode      = dwStreamMode;
    m_dwStreamCommand   = dwStreamCommand;
    return TRUE;
}

//返回值可以考虑使用三种:写失败；写成功，读失败；都成功。
BOOL CDiagnostic::SendReqPacket(void* pReqPkt, int Length, BOOL bHeadEnc, int nWaitMS, int nRetry)
{
    int tryTime;

    if(!m_COMPort.GetOpenFlag())
    {
        LOG_OUTPUT(_T("CDiagnostic::SendReqPacket Failed Open COM%d\n"),m_COMPort.GetPortID()+1);
        return FALSE;
    }

    DIAG_CRITISECT_LOCK();
    m_reqFrame.Encode((char*) pReqPkt, Length, bHeadEnc);

    ResetEvent(m_COMPort.GetTXEvent());
    for(tryTime = 0;tryTime < nRetry && m_COMPort.GetOpenFlag(); tryTime++)
    {
        if(TRUE == m_COMPort.Write((char*) m_reqFrame.GetHDLCFrame(),m_reqFrame.GetHDLCFrameLen()))
        {
            if(WAIT_OBJECT_0 == WaitForSingleObject(m_COMPort.GetTXEvent(), nWaitMS))
            {
                if(TRUE == m_bRXSuccess)
                {
                    BOOL bRet = TRUE;
                    if(m_CommandMode == CMD_MODE_DIAG)
                    {
                        bRet = bRet&&(GetLastDiagErr()==0);
                    }
                    else if(m_CommandMode == CMD_MODE_DLOAD)
                    {
                        bRet = bRet&&(GetLastDLoadErr()==0);
                    }
                    else if(m_CommandMode == CMD_MODE_ARMPRG)
                    {
                        bRet = bRet&&(GetLastAPErr()==0);
                    }
                    DIAG_CRITISECT_UNLOCK();
                    return bRet;
                }
                else if(RX_SUCCESS_SYNC == m_bRXSuccess)
                {
                    break;
                }
            }
        }
    }

    if(m_CommandMode == CMD_MODE_DIAG)
    {
        m_nDiagErr = REQUEST_SEND_FAILED;
    }
    else if(m_CommandMode == CMD_MODE_DLOAD)
    {
        m_nDLoadErr = REQUEST_SEND_FAILED;
    }
    else if(m_CommandMode == CMD_MODE_ARMPRG)
    {
        m_nAPErr = REQUEST_SEND_FAILED;
    }
    else
    {
        LOG_OUTPUT(_T("Unexcept COMMAND Mode %d COM%d!\n"),m_CommandMode, GetPortID()+1);
    }

    DIAG_CRITISECT_UNLOCK();
    return FALSE;
}

BOOL CDiagnostic::SendDiagPacket(void* pReqData, int Length, int nWaitMS, int nRetry)
{
    BOOL bRet;

    if(FALSE == IsStreamComplete())
    {
        LOG_OUTPUT(_T("CDiagnostic::SendDiagPacket Not StreamComplete %d %d\n"),m_CommandMode,m_StreamFrames.IsStreamComplete());
        return FALSE;
    }
    DIAG_CRITISECT_LOCK();
    m_CommandMode = CMD_MODE_DIAG;   
    bRet = SendReqPacket(pReqData, Length, FALSE, nWaitMS, nRetry);
    DIAG_CRITISECT_UNLOCK();
    return bRet;
}

BOOL CDiagnostic::SendDloadPacket(void* pReqData, int Length, int nWaitMS, int nRetry)
{
    BOOL bRet;
    
    if(FALSE == IsStreamComplete())
    {
        LOG_OUTPUT(_T("CDiagnostic::SendDloadPacket Not StreamComplete %d %d\n"),m_CommandMode,m_StreamFrames.IsStreamComplete());
        return FALSE;
    }
    DIAG_CRITISECT_LOCK();
    m_CommandMode = CMD_MODE_DLOAD;  
    bRet = SendReqPacket(pReqData, Length, TRUE, nWaitMS, nRetry);
    DIAG_CRITISECT_UNLOCK();
    return bRet;
}

BOOL CDiagnostic::SendAPPacket(void* pReqData, int Length, int nWaitMS, int nRetry)
{
    BOOL bRet;

    if(FALSE == IsStreamComplete())
    {
        LOG_OUTPUT(_T("CDiagnostic::SendAPPacket Not StreamComplete %d %d\n"),m_CommandMode,m_StreamFrames.IsStreamComplete());
        return FALSE;
    }

    DIAG_CRITISECT_LOCK();
    m_CommandMode = CMD_MODE_ARMPRG; 
    bRet = SendReqPacket(pReqData, Length, TRUE, nWaitMS, nRetry);
    DIAG_CRITISECT_UNLOCK();
    return bRet;
}

BOOL CDiagnostic::SendStreamPacket(void* pReqData, int Length)
{
    BOOL bRet;
    
    if(!m_COMPort.GetOpenFlag())
    {
        return FALSE;
    }
    
    DIAG_CRITISECT_LOCK();
    m_CommandMode = CMD_MODE_STREAM; 
    bRet = m_COMPort.Write((char*) pReqData, Length);
    DIAG_CRITISECT_UNLOCK();
    return bRet;
}

/*=======================================================================
These functions can perform mode-change command. 
(mode code: 0 = Offline Analog mode;1 = Offline Digital mode;2 = Reset) 
========================================================================*/
BOOL  CDiagnostic::SendModeChangeCmd(BYTE mode)
{
    cmdiag_mode_control_req_type  modechange;
    cmdiag_mode_control_rsp_type *pRsp;
    
    SYSTEM_PACKET_BUILT(modechange, DIAG_CONTROL_F);
    modechange.mode        = mode;
    
    if(!SendDiagPacket(&modechange, sizeof(cmdiag_mode_control_req_type), DIAG_WAIT_TIME*7, 3))
    {
        return FALSE;
    }
    
    pRsp = (cmdiag_mode_control_rsp_type*) m_rspFrame.GetOriginData();
    if(pRsp->mode != mode)
    {
        return FALSE;
    }
    return TRUE;
}

/*===========================================================================
These functions can switch phone to DLoad Mode
===========================================================================*/
BOOL  CDiagnostic::SendSwitchToDLoadReq(void)
{
    toolsdiag_dload_req_type dload;

    SYSTEM_PACKET_BUILT(dload, DIAG_DLOAD_F);
    return SendDiagPacket(&dload, sizeof(toolsdiag_dload_req_type));
}

/*===========================================================================
These functions send SPC to phone

Param: 
    SPC need by phone

Return: 
    1 – Code was correct and Service Programming (SP) is
    unlocked
    0 – Code was incorrect and SP is still locked
    If the code was incorrect, the phone will time out for 10 sec
    before responding to any more requests through the serial
    interface.
===========================================================================*/
BOOL CDiagnostic::SendSpcReq(diagpkt_sec_code_type *pSPC)
{
    DIAG_SPC_F_req_type  spc;
    DIAG_SPC_F_rsp_type* pRsp;
    
    SYSTEM_PACKET_BUILT(spc, DIAG_SPC_F);
    memcpy(&(spc.sec_code), pSPC, sizeof(diagpkt_sec_code_type));

    if(!SendDiagPacket(&spc, sizeof(DIAG_SPC_F_req_type)))
    {
        return FALSE;
    }
    
    pRsp = (DIAG_SPC_F_rsp_type*) m_rspFrame.GetOriginData();
    return (BOOL)pRsp->sec_code_ok;
}


BOOL CDiagnostic::EnterSPCMode(void)
{
    TCHAR   tSPC[DIAGPKT_SEC_CODE_SIZE+1];
    diagpkt_sec_code_type    SPC;

    GetPrivateProfileString(DIAG_INI_APP_NAME,DIAG_INI_SPC_KEY,DIAG_INI_SPC_DEF_STR,tSPC,(DIAGPKT_SEC_CODE_SIZE+1)*sizeof(TCHAR),DIAG_INI_FILE);

    for(int i=0;i<DIAGPKT_SEC_CODE_SIZE;i++)
    {
        SPC.digits[i] = (BYTE)tSPC[i];
    }

	if (!SendSpcReq(&SPC))
	{
        // Send Supper SPC
        SPC.digits[0] = '9';
        SPC.digits[1] = '5';
        SPC.digits[2] = '3';
        SPC.digits[3] = '3';
        SPC.digits[4] = '5';
        SPC.digits[5] = '9';
        if (!SendSpcReq(&SPC))
        {
		    return FALSE;
        }
	}
    return TRUE;
}

// NV Operater Functions
BOOL CDiagnostic::SendWriteNVItem(nv_items_enum_type nItem, void *pBuf, UINT nSize)
{
    DIAG_NV_WRITE_F_req_type    nv;
    DIAG_NV_WRITE_F_rsp_type*   pRsp;
    
    if(nSize > DIAG_NV_ITEM_SIZE)
    {
        return FALSE;
    }

    //将命令放到请求数据包的首部。
    SYSTEM_PACKET_BUILT(nv, DIAG_NV_WRITE_F);
    nv.item     = nItem;
    nv.nv_stat  = 0;
    memset(nv.item_data,0,DIAG_NV_ITEM_SIZE);
    memcpy(nv.item_data, pBuf, nSize);
    
    if(!SendDiagPacket(&nv, sizeof(DIAG_NV_WRITE_F_req_type)))
    {
        LOG_OUTPUT(_T("***NV %d Send packet failed\n"),nItem);
        return FALSE;
    }

    pRsp = (DIAG_NV_WRITE_F_rsp_type*) m_rspFrame.GetOriginData();

    if(pRsp->nv_stat != 0)
    {
        LOG_OUTPUT(_T("NV %d BAD State %d\n"),nItem, pRsp->nv_stat);
    }
    return TRUE;
}

BOOL CDiagnostic::SendReadNVItem(nv_items_enum_type nItem, void *pBuf, UINT nSize)
{
    DIAG_NV_READ_F_req_type    nv;
    DIAG_NV_READ_F_rsp_type*   pRsp;
    
    if(nSize > DIAG_NV_ITEM_SIZE)
    {
        return FALSE;
    }

    //将命令放到请求数据包的首部。
    SYSTEM_PACKET_BUILT(nv, DIAG_NV_READ_F);
    nv.item     = nItem;
    nv.nv_stat  = 0;
    memset(nv.item_data,0,DIAG_NV_ITEM_SIZE);
    
    if(!SendDiagPacket(&nv, sizeof(DIAG_NV_READ_F_req_type)))
    {
        LOG_OUTPUT(_T("***NV %d Send packet failed\n"),nItem);
        return FALSE;
    }

    pRsp = (DIAG_NV_READ_F_rsp_type*) m_rspFrame.GetOriginData();

    if(pRsp->nv_stat != 0)
    {
        LOG_OUTPUT(_T("NV %d BAD State %d\n"),nItem, pRsp->nv_stat);
        return FALSE;
    }
    
    memcpy(pBuf, pRsp->item_data, nSize);
    return TRUE;
}

BOOL CDiagnostic::SendWriteNVItemExt(nv_items_enum_type nItem, void *pBuf, UINT nSize, UINT context)
{
    DIAG_NV_WRITE_EXT_F_req_type    nv;
    DIAG_NV_WRITE_EXT_F_rsp_type*   pRsp;
    
    if(nSize > DIAG_NV_ITEM_SIZE)
    {
        return FALSE;
    }

    //将命令放到请求数据包的首部。
    NVSUBSYS_PACKET_BUILT(nv, DIAG_SUBSYS_NV_WRITE_EXT_F);
    nv.item     = nItem;
    nv.context  = context;
    nv.nv_stat  = 0;
    memset(nv.item_data,0,DIAG_NV_ITEM_SIZE);
    memcpy(nv.item_data, pBuf, nSize);
    
    if(!SendDiagPacket(&nv, sizeof(DIAG_NV_WRITE_EXT_F_req_type)))
    {
        LOG_OUTPUT(_T("***NV %d Send packet failed\n"),nItem);
        return FALSE;
    }

    pRsp = (DIAG_NV_WRITE_EXT_F_rsp_type*) m_rspFrame.GetOriginData();

    if(pRsp->nv_stat != 0)
    {
        LOG_OUTPUT(_T("NV %d BAD State %d\n"),nItem, pRsp->nv_stat);
    }
    return TRUE;
}

BOOL CDiagnostic::SendReadNVItemExt(nv_items_enum_type nItem, void *pBuf, UINT nSize, UINT context)
{
    DIAG_NV_READ_EXT_F_req_type    nv;
    DIAG_NV_READ_EXT_F_rsp_type*   pRsp;
    
    if(nSize > DIAG_NV_ITEM_SIZE)
    {
        return FALSE;
    }

    //将命令放到请求数据包的首部。
    NVSUBSYS_PACKET_BUILT(nv, DIAG_SUBSYS_NV_READ_EXT_F);
    nv.item     = nItem;
    nv.context  = context;
    nv.nv_stat  = 0;
    memset(nv.item_data,0,DIAG_NV_ITEM_SIZE);
    
    if(!SendDiagPacket(&nv, sizeof(DIAG_NV_READ_EXT_F_req_type)))
    {
        LOG_OUTPUT(_T("***NV %d Send packet failed\n"),nItem);
        return FALSE;
    }

    pRsp = (DIAG_NV_READ_EXT_F_rsp_type*) m_rspFrame.GetOriginData();

    if(pRsp->nv_stat != 0)
    {
        LOG_OUTPUT(_T("NV %d BAD State %d\n"),nItem, pRsp->nv_stat);
        return FALSE;
    }
    
    memcpy(pBuf, pRsp->item_data, nSize);
    return TRUE;
}

//DLoad Mode
BOOL CDiagnostic::SendDLoadWriteReq(DWORD dwAddr, const BYTE *pData, WORD wLen)
{
    BOOL bRet;
    diag_dload_write_req_type     write;

    write.cmd_code      = CMD_WRITE;
    //write.addr_1        = (BYTE)((dwAddr >> 24) & 0xFF);
	write.addr_1        = (BYTE)((dwAddr >> 16) & 0xFF);
	write.addr_2        = (BYTE)((dwAddr >> 8) & 0xFF);
	write.addr_3        = (BYTE)((dwAddr) & 0xFF);
    write.len_1         = (BYTE)((wLen>>8)&0xFF);
    write.len_2         = (BYTE)((wLen)&0xFF);

    memcpy(write.data,pData,wLen);

    bRet = SendDloadPacket(&write, WRITE_SIZ+wLen-2);
    return bRet;
}

BOOL CDiagnostic::SendDLoadEreaseReq(DWORD dwAddr, DWORD dwLen)
{
    BOOL bRet;
    diag_dload_erase_req_type     erase;

    erase.cmd_code      = CMD_ERASE;
    erase.addr_1        = (BYTE)((dwAddr >> 16) & 0xFF);
	erase.addr_2        = (BYTE)((dwAddr >> 8) & 0xFF);
	erase.addr_3        = (BYTE)((dwAddr) & 0xFF);
    erase.len_1         = (BYTE)((dwLen>>16)&0xFF);
    erase.len_2         = (BYTE)((dwLen>>8)&0xFF);
    erase.len_3         = (BYTE)((dwLen)&0xFF);

    bRet = SendDloadPacket(&erase, sizeof(erase));
    return bRet;
}

BOOL CDiagnostic::SendDLoadGoReq(DWORD dwAddr)
{
    BOOL bRet;
    diag_dload_go_req_type     go;

    go.cmd_code     = CMD_GO;
    go.addr_1       = (BYTE)((dwAddr >> 24) & 0xFF);
	go.addr_2       = (BYTE)((dwAddr >> 16) & 0xFF);
	go.addr_3       = (BYTE)((dwAddr >> 8) & 0xFF);
	go.addr_4       = (BYTE)((dwAddr) & 0xFF);
    
    bRet = SendDloadPacket(&go, sizeof(go));
    return bRet;
}

BOOL CDiagnostic::SendDLoadNOPReq(void)
{
    BOOL bRet;
    diag_dload_nop_req_type     nop;

    nop.cmd_code = CMD_NOP;

    bRet = SendDloadPacket(&nop, sizeof(nop));
    return bRet;
}

BOOL CDiagnostic::SendDLoadParamReq(diag_dload_param_rsp_type *pRspParam)
{
    BOOL bRet;
    diag_dload_param_req_type     param;

    param.cmd_code = CMD_PREQ;

    bRet = SendDloadPacket(&param, sizeof(param));
    if(bRet && pRspParam)
    {
        BYTE *pRsp  = m_rspFrame.GetOriginData();
        pRspParam->cmd_code          = pRsp[0];
        pRspParam->protocol_id       = pRsp[1];
        pRspParam->min_protocol_id   = pRsp[2];
        pRspParam->max_write_size    = (pRsp[3]<<8)|pRsp[4];
        pRspParam->mob_model         = pRsp[5];
        pRspParam->bb_ver            = pRsp[6];
    }
    return bRet;
}

BOOL CDiagnostic::SendDLoadVerReq(diag_dload_ver_rsp_type *pRspVer)
{
    BOOL bRet;
    diag_dload_ver_req_type     ver;

    ver.cmd_code = CMD_VERREQ;

    bRet = SendDloadPacket(&ver, sizeof(ver));
    if(bRet && pRspVer)
    {
        BYTE *pRsp  = m_rspFrame.GetOriginData();
        pRspVer->cmd_code            = pRsp[0];
        pRspVer->len                 = pRsp[1];
        for(int i=0;i<pRspVer->len;i++)
        {
            pRspVer->ver[i] = pRsp[2+i];
        }
    }
    return bRet;
}

BOOL CDiagnostic::SendDLoadResetReq(void)
{
    BOOL bRet;
    diag_dload_reset_req_type     reset;

    reset.cmd_code = CMD_RESET;

    bRet = SendDloadPacket(&reset, sizeof(reset));
    return bRet;
}

BOOL CDiagnostic::SendDLoadUnlockReq(const BYTE *pCode, WORD wLen)
{
    BOOL bRet;
    diag_dload_unlock_req_type     unlock;

    unlock.cmd_code = CMD_UNLOCK;
    memset(unlock.unlock,0,sizeof(unlock.unlock));
    memcpy(unlock.unlock,pCode,min(wLen,DIAG_PASSWORD_SIZE));

    bRet = SendDloadPacket(&unlock, sizeof(unlock));
    return bRet;
}

BOOL CDiagnostic::SendDLoadPoweroffReq(void)
{
    BOOL bRet;
    diag_dload_poweroff_req_type     poweroff;

    poweroff.cmd_code = CMD_PWROFF;

    bRet = SendDloadPacket(&poweroff, sizeof(poweroff));
    return bRet;
}

BOOL CDiagnostic::SendDLoadWrite32Req(DWORD dwAddr, const BYTE *pData, WORD wLen)
{
    BOOL bRet;
    diag_dload_write32bit_req_type     write32;

    write32.cmd_code    = CMD_WRITE_32BIT;
    write32.addr_1      = (BYTE)((dwAddr >> 24) & 0xFF);
	write32.addr_2      = (BYTE)((dwAddr >> 16) & 0xFF);
	write32.addr_3      = (BYTE)((dwAddr >> 8) & 0xFF);
	write32.addr_4      = (BYTE)((dwAddr) & 0xFF);
    write32.len_1       = (BYTE)((wLen>>8)&0xFF);
    write32.len_2       = (BYTE)((wLen)&0xFF);

    memcpy(write32.data,pData,wLen);
    
    bRet = SendDloadPacket(&write32, WRITE_32BIT_SIZ+wLen-2);
    return bRet;
}

BOOL CDiagnostic::SendDLoadResetModeReq(DWORD dwMode)
{
    BOOL bRet;
    diag_dload_reset_mode_req_type     resetmode;

    resetmode.cmd_code    = CMD_DLOAD_RESETMODE;
    resetmode.mode_1      = (BYTE)((dwMode >> 24) & 0xFF);
	resetmode.mode_2      = (BYTE)((dwMode >> 16) & 0xFF);
	resetmode.mode_3      = (BYTE)((dwMode >> 8) & 0xFF);
	resetmode.mode_4      = (BYTE)((dwMode) & 0xFF);
    
    bRet = SendDloadPacket(&resetmode, sizeof(resetmode));
    return bRet;
}

BOOL CDiagnostic::ProcessDLoadRsp(void)
{
    BYTE *pReq = m_reqFrame.GetOriginData();
    BYTE *pRsp = m_rspFrame.GetOriginData();

    m_nDLoadErr = 0;

    switch(pRsp[0]){
    case CMD_ACK:
        break;

    case CMD_NAK:
        m_nDLoadErr = (pRsp[1]<<8)| pRsp[2];
        break;

    case CMD_PARAMS:
        if(pReq[0] != CMD_PREQ)
        {
            return FALSE;
        }
        break;

    case CMD_VERRSP:
        if(pReq[0] != CMD_VERREQ)
        {
            return FALSE;
        }
        break;

    case CMD_MEM_DEBUG_INFO:
        if(pReq[0] != CMD_MEM_DEBUG_QUERY)
        {
            return FALSE;
        }
        break;

    case CMD_MEM_READ_RESP:
        if(pReq[0] != CMD_MEM_READ_REQ)
        {
            return FALSE;
        }
        break;

    default:
        return FALSE;
    }
    return TRUE;
}

// ARMPRG Mode
BOOL CDiagnostic::SendAPHelloReq(diag_ap_hello_rsp_type *pHelloRsp, BYTE req_features)
{
    BOOL bRet;
    diag_ap_hello_req_type     hello;
    static const char host_header[] = AP_HOST_HEADER;
    static const char targ_header[] = AP_TARG_HEADER;

    hello.cmd_code      = AP_HELLO_CMD;
    memcpy(hello.host_header, host_header, AP_HELLO_HEADER_LEN);
    hello.max_ver       = STREAM_DLOAD_MAX_VER;
    hello.min_ver       = STREAM_DLOAD_MIN_VER;
    hello.req_features  = req_features;
    
    bRet = SendAPPacket(&hello, sizeof(hello));
    if(bRet && pHelloRsp)
    {
        BYTE *pRsp  = m_rspFrame.GetOriginData();
        int   idx, i;

        pHelloRsp->cmd_code             = pRsp[0];
        memcpy(pHelloRsp->target_header, &pRsp[1], AP_HELLO_HEADER_LEN);
        if(memcmp(pHelloRsp->target_header,targ_header, AP_HELLO_HEADER_LEN) != 0)
        {
            return FALSE;
        }
        idx = AP_HELLO_HEADER_LEN+1;
        pHelloRsp->max_ver              = pRsp[idx++];
        pHelloRsp->min_ver              = pRsp[idx++];
        pHelloRsp->preferred_block_size = (pRsp[idx])|(pRsp[idx+1]<<8)|(pRsp[idx+2]<<16)|(pRsp[idx+3]<<24);
        idx += 4;
        pHelloRsp->base_addr            = (pRsp[idx])|(pRsp[idx+1]<<8)|(pRsp[idx+2]<<16)|(pRsp[idx+3]<<24);
        idx += 4;
        pHelloRsp->flashnamelen         = pRsp[idx++];
        memcpy(pHelloRsp->flashname,&pRsp[idx],pHelloRsp->flashnamelen);
        idx += pHelloRsp->flashnamelen;
        pHelloRsp->windowsize           = (pRsp[idx])|(pRsp[idx+1]<<8);
        idx += 2;
        pHelloRsp->num_blocks           = (pRsp[idx])|(pRsp[idx+1]<<8);
        idx += 2;
        for(i=0;i<pHelloRsp->num_blocks;i++)
        {
            pHelloRsp->blocks[i]        = (pRsp[idx])|(pRsp[idx+1]<<8)|(pRsp[idx+2]<<16)|(pRsp[idx+3]<<24);
            idx += 4;
        }
        pHelloRsp->support_features     = pRsp[idx];
    }
    return bRet;
}

BOOL CDiagnostic::SendAPReadReq(diag_ap_read_rsp_type *pReadRsp, DWORD dwAddr, WORD wLen)
{
    BOOL bRet;
    diag_ap_read_req_type     read;

    read.cmd_code   = AP_READ_CMD;
    read.dwAddr     = dwAddr;
    read.wLen       = wLen;

    bRet = SendAPPacket(&read, sizeof(read), DIAG_WAIT_TIME*2);
    if(bRet && pReadRsp)
    {
        if(m_rspFrame.GetOriginDataLen() != wLen+5)
        {
            return FALSE;
        }
        memcpy(pReadRsp, m_rspFrame.GetOriginData(), m_rspFrame.GetOriginDataLen());
    }
    return bRet;
}

BOOL CDiagnostic::SendAPWriteReq(DWORD dwAddr, const BYTE *pData, WORD wLen)
{
    BOOL bRet;
    diag_ap_write_req_type     write;

    write.cmd_code  = AP_WRITE_CMD;
    write.dwAddr    = dwAddr;
    memcpy(write.data, pData, wLen);

    bRet = SendAPPacket(&write, wLen+5);
    return bRet;
}

BOOL CDiagnostic::SendAPStreamWriteReq(DWORD dwAddr, const BYTE *pData, WORD wLen)
{
    BOOL bRet = FALSE;
    diag_ap_stream_write_req_type     write;
    
    if(FALSE == SwitchStreamMode(CMD_MODE_ARMPRG,AP_STREAM_WRITE_CMD))
    {
        return FALSE;
    }

    write.cmd_code  = AP_STREAM_WRITE_CMD;
    write.dwAddr    = dwAddr;
    memcpy(write.data, pData, wLen);
    
    CHDLCFrame *pReqFrame = m_StreamFrames.InsertFrame(dwAddr, &write, wLen+5, TRUE);
    if(NULL == pReqFrame)
    {
        ResetEvent(m_hStreamEvent);
        if(WAIT_OBJECT_0 != WaitForSingleObject(m_hStreamEvent, STREAMFRAME_TIMEOUT_MS))
        {
            m_StreamFrames.RemoveFrame(dwAddr);
            return FALSE;
        }
        pReqFrame = m_StreamFrames.InsertFrame(dwAddr, &write, wLen+5, TRUE);
    }
    
    if(pReqFrame)
    {
        bRet = SendStreamPacket(pReqFrame->GetHDLCFrame(), pReqFrame->GetHDLCFrameLen());
        if(bRet == TRUE)
        {
            SendTimeoutStreamFrames();
        }
        else
        {
            m_StreamFrames.RemoveFrame(dwAddr);
        }
    }
    return bRet;
}

BOOL CDiagnostic::SendAPSyncReq(const BYTE *pData, WORD wLen)
{
    BOOL bRet;
    diag_ap_sync_req_type     sync;

    sync.cmd_code = AP_NOP_CMD;
    memcpy(sync.data, pData, wLen);
    
    bRet = SendAPPacket(&sync, wLen+1);
    if(bRet)
    {
        if(m_rspFrame.GetOriginDataLen() != wLen+1)
        {
            return FALSE;
        }

        if(memcmp(pData, m_rspFrame.GetOriginData()+1, wLen) != 0)
        {
            return FALSE;
        }
    }
    return bRet;
}

BOOL CDiagnostic::SendAPResetReq(void)
{
    BOOL bRet;
    diag_ap_reset_req_type     reset;

    reset.cmd_code = AP_RESET_CMD;

    bRet = SendAPPacket(&reset, sizeof(reset));
    return bRet;
}

BOOL CDiagnostic::SendAPPoweroffReq(void)
{
    BOOL bRet;
    diag_ap_poweroff_req_type     poweroff;

    poweroff.cmd_code = AP_PWRDOWN_CMD;

    bRet = SendAPPacket(&poweroff, sizeof(poweroff));
    return bRet;
}

BOOL CDiagnostic::SendAPOpenReq(open_mode_type mode)
{
    BOOL bRet;
    diag_ap_open_req_type     open;

    open.cmd_code   = AP_OPEN_CMD;
    open.mode       = mode;

    bRet = SendAPPacket(&open, sizeof(open));
    return bRet;
}

BOOL CDiagnostic::SendAPCloseReq(void)
{
    BOOL bRet;
    diag_ap_close_req_type     close;

    close.cmd_code = AP_CLOSE_CMD;

    bRet = SendAPPacket(&close, sizeof(close));
    return bRet;
}

BOOL CDiagnostic::SendAPSecModeReq(BYTE mode)
{
    BOOL bRet;
    diag_ap_sec_mode_req_type     secmode;

    secmode.cmd_code   = AP_SECURITY_MODE_CMD;
    secmode.mode       = mode;
    
    bRet = SendAPPacket(&secmode, sizeof(secmode));
    return bRet;
}

BOOL CDiagnostic::SendAPPartiTblReq(BYTE bOverride, const BYTE *pTbl, WORD wLen)
{
    BOOL bRet;
    diag_ap_parti_tbl_req_type     parti;

    parti.cmd_code  = AP_PARTITION_TABLE_CMD;
    parti.boverride = bOverride;
    memcpy(parti.table, pTbl, wLen);

    bRet = SendAPPacket(&parti, wLen+2, 5000);
    if(bRet)
    {
        diag_ap_parti_tbl_rsp_type *pRsp = (diag_ap_parti_tbl_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(diag_ap_parti_tbl_rsp_type))
        {
            return FALSE;
        }

        m_nAPErr = pRsp->status;
    }
    return (bRet&&(GetLastAPErr() == 0));
}

BOOL CDiagnostic::SendAPOpenMultiReq(open_multi_mode_type mode, const BYTE *pData, WORD wLen)
{
    BOOL bRet;
    diag_ap_open_multi_req_type     multi;

    multi.cmd_code  = AP_OPEN_MULTI_IMAGE_CMD;
    multi.mode      = mode;
    if(pData && wLen)
    {
        memcpy(multi.data,pData, wLen);
    }
    
    bRet = SendAPPacket(&multi, wLen+2);
    if(bRet)
    {
        diag_ap_open_multi_rsp_type *pRsp = (diag_ap_open_multi_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(diag_ap_open_multi_rsp_type))
        {
            return FALSE;
        }
        
        m_nAPErr = pRsp->status;
    }
    return bRet;
}

BOOL CDiagnostic::SendAPUnframedStreamWriteReq(DWORD dwAddr, const BYTE *pData, WORD wLen)
{
    BOOL bRet = FALSE;
    diag_ap_unframed_stream_write_req_type     write;
    
    if(FALSE == SwitchStreamMode(CMD_MODE_ARMPRG,AP_UNFRAMED_STREAM_WRITE_CMD))
    {
        return FALSE;
    }

    write.cmd_code  = AP_UNFRAMED_STREAM_WRITE_CMD;
    write.dummy1    = 0;
    write.dummy2    = 0;
    write.dummy3    = 0;
    write.dwAddr    = dwAddr;
    write.dwLen     = wLen;
    memcpy(write.data, pData, wLen);
    
    CHDLCFrame *pReqFrame = m_StreamFrames.InsertFrame(dwAddr, &write, wLen+12, TRUE, FALSE);
    if(NULL == pReqFrame)
    {
        ResetEvent(m_hStreamEvent);
        if(WAIT_OBJECT_0 != WaitForSingleObject(m_hStreamEvent, STREAMFRAME_TIMEOUT_MS))
        {
            m_StreamFrames.RemoveFrame(dwAddr);
            return FALSE;
        }
        pReqFrame = m_StreamFrames.InsertFrame(dwAddr, &write, wLen+12, TRUE, FALSE);
    }
    
    if(pReqFrame)
    {
        bRet = SendStreamPacket(pReqFrame->GetHDLCFrame(), pReqFrame->GetHDLCFrameLen());
        if(bRet == TRUE)
        {
            SendTimeoutStreamFrames();
        }
        else
        {
            m_StreamFrames.RemoveFrame(dwAddr);
        }
    }
    return bRet;
}

BOOL CDiagnostic::ProcessAPRsp(void)
{
    BYTE *pReq = m_reqFrame.GetOriginData();
    BYTE *pRsp = m_rspFrame.GetOriginData();

    m_nAPErr = 0;
    switch(pRsp[0]){
    case AP_HELLO_RSP:
        if(pReq[0] != AP_HELLO_CMD)
        {
            return FALSE;
        }
        break;

    case AP_READ_RSP:
        if(pReq[0] != AP_READ_CMD)
        {
            return FALSE;
        }
        break;

    case AP_WRITE_RSP:
        if(pReq[0] != AP_WRITE_CMD)
        {
            return FALSE;
        }
        break;

    case AP_STREAM_WRITE_RSP:
        if(pReq[0] != AP_STREAM_WRITE_CMD)
        {
            return FALSE;
        }
        break;

    case AP_NOP_RSP:
        if(pReq[0] != AP_NOP_CMD)
        {
            return FALSE;
        }
        break;

    case AP_RESET_RSP:
        if(pReq[0] != AP_RESET_CMD)
        {
            return FALSE;
        }
        break;

    case AP_ERROR_RSP:
        m_nAPErr = pRsp[1]|(pRsp[2]<<8)|(pRsp[3]<<16)|(pRsp[4]<<24);
        if(m_nAPErr == NAK_INVALID_CMD)
        {
            return FALSE; // 对于此错误我们将忽略，因为有可能是Probe命令引起的。
        }
        break;

    case AP_UNLOCK_RSP:
        if(pReq[0] != AP_UNLOCK_CMD)
        {
            return FALSE;
        }
        break;

    case AP_PWRDOWN_RSP:
        if(pReq[0] != AP_PWRDOWN_CMD)
        {
            return FALSE;
        }
        break;

    case AP_OPEN_RSP:
        if(pReq[0] != AP_OPEN_CMD)
        {
            return FALSE;
        }
        break;

    case AP_CLOSE_RSP:
        if(pReq[0] != AP_CLOSE_CMD)
        {
            return FALSE;
        }
        break;

    case AP_SECURITY_MODE_RSP:
        if(pReq[0] != AP_SECURITY_MODE_CMD)
        {
            return FALSE;
        }
        break;

    case AP_PARTITION_TABLE_RSP:
        if(pReq[0] != AP_PARTITION_TABLE_CMD)
        {
            return FALSE;
        }
        break;

    case AP_OPEN_MULTI_IMAGE_RSP:
        if(pReq[0] != AP_OPEN_MULTI_IMAGE_CMD)
        {
            return FALSE;
        }
        break;
        
    case AP_UNFRAMED_STREAM_WRITE_RSP:
        if(pReq[0] != AP_UNFRAMED_STREAM_WRITE_CMD)
        {
            return FALSE;
        }
        break;
        
    default:
        return FALSE;
    }
    return TRUE;
}

//EFS2 Opration
// Parameter negotiation packet
BOOL CDiagnostic::SendEFS2HelloReq(void)
{
    BOOL bRet = TRUE;
    efs2_diag_hello_req_type hello;

    memset(&hello,0,sizeof(hello));
    FSSUBSYS_PACKET_BUILT(hello, EFS2_DIAG_HELLO);
    bRet = SendDiagPacket(&hello, sizeof(hello));
    if(bRet)
    {
        efs2_diag_hello_rsp_type *pRsp = (efs2_diag_hello_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_hello_rsp_type))
        {
            return FALSE;
        }
    }
    return bRet;
}

// Send information about EFS2 params
BOOL CDiagnostic::SendEFS2QueryReq(void)
{
    BOOL bRet = TRUE;
    efs2_diag_query_req_type query;

    FSSUBSYS_PACKET_BUILT(query, EFS2_DIAG_QUERY);
    
    bRet = SendDiagPacket(&query, sizeof(query));
    if(bRet)
    {
        efs2_diag_query_rsp_type *pRsp = (efs2_diag_query_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_query_rsp_type))
        {
            return FALSE;
        }
        m_nEFS2MaxPath  = pRsp->max_path;
        m_nEFS2MaxName  = pRsp->max_name;
    }
    return bRet;
}

// Open a file
BOOL CDiagnostic::SendEFS2OpenReq(char *pName, int32 oFlag, int32 *pFd)
{
    BOOL bRet = TRUE;

    if(NULL == pFd || NULL == pName )
    {
        return FALSE;
    }
    
    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        *pFd = -1;
        return FALSE;
    }

    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen+sizeof(efs2_diag_open_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_open_req_type *pReq = (efs2_diag_open_req_type *)bData; 

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_OPEN);
    pReq->mode   = 0;
    pReq->oflag  = oFlag;
    strcpy(pReq->path, pName);
    
    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_open_rsp_type *pRsp = (efs2_diag_open_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_open_rsp_type))
        {
            *pFd = -1;
            return FALSE;
        }

        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0)
        {
            *pFd = -1;
            return FALSE;
        }
        *pFd        = pRsp->fd;
    }
    return bRet;
}

// Close a file
BOOL CDiagnostic::SendEFS2CloseReq(int32 nFd)
{
    BOOL bRet = TRUE;
    
    if(nFd < 0)
    {
        return FALSE;
    }
    efs2_diag_close_req_type close;

    FSSUBSYS_PACKET_BUILT(close, EFS2_DIAG_CLOSE);
    close.fd    = nFd;
    
    bRet = SendDiagPacket(&close, sizeof(close));
    if(bRet)
    {
        efs2_diag_close_rsp_type *pRsp = (efs2_diag_close_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_close_rsp_type))
        {
            return FALSE;
        }
        m_nEFS2Err  = pRsp->diag_errno;
    }
    return bRet&&(GetLastEFS2Err()==0);
}

// Read a file
BOOL CDiagnostic::SendEFS2ReadReq(int32 nFd, byte *pBuf, uint32 dwLen, uint32 dwOffset, int32 *pReaded)
{
    BOOL bRet = TRUE;

    if(nFd < 0 || NULL == pBuf || dwLen == 0)
    {
        return FALSE;
    }

    efs2_diag_read_req_type read;

    FSSUBSYS_PACKET_BUILT(read, EFS2_DIAG_READ);
    read.fd     = nFd;
    read.nbyte  = dwLen;
    read.offset = dwOffset;
    
    bRet = SendDiagPacket(&read, sizeof(read));
    if(bRet)
    {
        efs2_diag_read_rsp_type *pRsp = (efs2_diag_read_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() < sizeof(efs2_diag_read_rsp_type))
        {
            return FALSE;
        }

        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0 || (int32)dwLen <= pRsp->bytes_read || dwOffset != pRsp->offset)
        {
            return FALSE;
        }
        if(pReaded) *pReaded = pRsp->bytes_read;
        memcpy(pBuf, pRsp->data, pRsp->bytes_read);
    }
    return bRet;
}

// Write a file
BOOL CDiagnostic::SendEFS2WriteReq(int32 nFd, byte *pBuf, uint32 dwLen, uint32 dwOffset, int32 *pWritten)
{
    BOOL bRet = TRUE;
    
    if(nFd < 0 || NULL == pBuf || dwLen == 0)
    {
        return FALSE;
    }
    
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    efs2_diag_write_req_type *pReq = (efs2_diag_write_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_WRITE);
    pReq->fd    = nFd;
    pReq->offset    = dwOffset;
    memcpy(pReq->data, pBuf, dwLen);

    bRet = SendDiagPacket(pReq, dwLen+sizeof(efs2_diag_write_req_type)-1);
    if(bRet)
    {
        efs2_diag_write_rsp_type *pRsp = (efs2_diag_write_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_write_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0 || (int32)dwLen < pRsp->bytes_written || dwOffset != pRsp->offset)
        {
            return FALSE;
        }
        if(pWritten) *pWritten = pRsp->bytes_written;
    }
    return bRet;
}

// Remove a symbolic link or file
BOOL CDiagnostic::SendEFS2UnlinkReq(char *pName)
{
    BOOL bRet = TRUE;
    
    if(NULL == pName)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }
    
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_unlink_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_unlink_req_type *pReq = (efs2_diag_unlink_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_UNLINK);
    strcpy(pReq->path, pName);

    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_unlink_rsp_type *pRsp = (efs2_diag_unlink_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_unlink_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
    }
    return bRet&&(GetLastEFS2Err()==0);
}

// Create a directory
BOOL CDiagnostic::SendEFS2MkDirReq(char *pName)
{
    BOOL bRet = TRUE;
    
    if(NULL == pName)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }
    
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_mkdir_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_mkdir_req_type *pReq = (efs2_diag_mkdir_req_type *)bData;
    if(pReq == NULL)
    {
        return FALSE;
    }

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_MKDIR);
    pReq->mode  = 0;
    strcpy(pReq->path, pName);

    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_mkdir_rsp_type *pRsp = (efs2_diag_mkdir_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_mkdir_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
    }
    return bRet&&(GetLastEFS2Err()==0);
}

// Remove a directory
BOOL CDiagnostic::SendEFS2RmDirReq(char *pName)
{
    BOOL bRet = TRUE;
    
    if(NULL == pName)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }
    
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_rmdir_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_rmdir_req_type *pReq = (efs2_diag_rmdir_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_RMDIR);
    strcpy(pReq->path, pName);

    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_rmdir_rsp_type *pRsp = (efs2_diag_rmdir_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_rmdir_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
    }
    return bRet&&(GetLastEFS2Err()==0);
}

// Open a directory for reading
BOOL CDiagnostic::SendEFS2OpenDirReq(char *pName, uint32 *pdwDirp)
{
    BOOL bRet = TRUE;
    
    if(NULL == pName || NULL == pdwDirp)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        *pdwDirp = 0;
        return FALSE;
    }
    
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_opendir_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_opendir_req_type *pReq = (efs2_diag_opendir_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_OPENDIR);
    strcpy(pReq->path, pName);

    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_opendir_rsp_type *pRsp = (efs2_diag_opendir_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_opendir_rsp_type))
        {
            *pdwDirp = 0;
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0)
        {
            *pdwDirp = 0;
            return FALSE;
        }
        *pdwDirp    = pRsp->dirp;
    }
    return bRet;
}

// Read a directory
BOOL CDiagnostic::SendEFS2ReadDirReq(uint32 dwDirp, int32 nSeq, char *pName, uint32 dwLen, BOOL *pbFile, int32 *pSize)
{
    BOOL bRet = TRUE;
    
    if(0 == dwDirp || NULL == pName || 0 == dwLen)
    {
        return FALSE;
    }
    
    efs2_diag_readdir_req_type readdir;

    FSSUBSYS_PACKET_BUILT(readdir, EFS2_DIAG_READDIR);
    readdir.dirp    = dwDirp;
    readdir.seqno   = nSeq;
    
    bRet = SendDiagPacket(&readdir, sizeof(readdir));
    if(bRet)
    {
        efs2_diag_readdir_rsp_type *pRsp = (efs2_diag_readdir_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() < sizeof(efs2_diag_readdir_rsp_type))
        {
            return FALSE;
        }
        m_nEFS2Err  = pRsp->diag_errno;
        if(0 != m_nEFS2Err || pRsp->seqno != nSeq)
        {
            return FALSE;
        }
        memcpy(pName, pRsp->entry_name, MIN(dwLen,(m_rspFrame.GetOriginDataLen()-sizeof(efs2_diag_readdir_rsp_type)+1)));
        if(pbFile)  *pbFile = (pRsp->entry_type == 0)?TRUE:FALSE;
        if(pSize)   *pSize  = pRsp->size;
    }
    return bRet;
}

// Close an open directory
BOOL CDiagnostic::SendEFS2CloseDirReq(uint32 dwDirp)
{
    BOOL bRet = TRUE;

    if(0 == dwDirp)
    {
        return FALSE;
    }

    efs2_diag_closedir_req_type closedir;

    FSSUBSYS_PACKET_BUILT(closedir, EFS2_DIAG_CLOSEDIR);
    closedir.dirp   = dwDirp;
    
    bRet = SendDiagPacket(&closedir, sizeof(closedir));
    if(bRet)
    {
        efs2_diag_closedir_rsp_type *pRsp = (efs2_diag_closedir_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_closedir_rsp_type))
        {
            return FALSE;
        }
        m_nEFS2Err  = pRsp->diag_errno;
    }
    return bRet&&(GetLastEFS2Err()==0);
}

// Rename a file or directory
BOOL CDiagnostic::SendEFS2RenameReq(char *pOldName, char *pNewName)
{
    BOOL bRet = TRUE;

    if(NULL == pOldName || NULL == pNewName)
    {
        return FALSE;
    }
    
    int nOldLen = strlen(pOldName);
    int nNewLen = strlen(pNewName);
    if(m_nEFS2MaxPath != 0 && (nOldLen > m_nEFS2MaxPath || nNewLen > m_nEFS2MaxPath))
    {
        return FALSE;
    }
    
    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    efs2_diag_rename_req_type *pReq = (efs2_diag_rename_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_RENAME);
    memcpy(pReq->oldpath,pOldName,nOldLen+1);
    memcpy(pReq->oldpath+nOldLen+1,pNewName,nNewLen+1);
    
    bRet = SendDiagPacket(pReq, nOldLen+nNewLen+sizeof(efs2_diag_rename_req_type));
    if(bRet)
    {
        efs2_diag_rename_rsp_type *pRsp = (efs2_diag_rename_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_rename_rsp_type))
        {
            return FALSE;
        }
        m_nEFS2Err  = pRsp->diag_errno;
    }
    return bRet&&(GetLastEFS2Err()==0);
}

// Obtain information about a named file
BOOL CDiagnostic::SendEFS2StatReq(char *pName, int32 *pSize)
{
    BOOL bRet = TRUE;

    if(NULL == pName)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }

    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_stat_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_stat_req_type *pReq = (efs2_diag_stat_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_STAT);
    strcpy(pReq->path, pName);
    
    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_stat_rsp_type *pRsp = (efs2_diag_stat_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_stat_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0)
        {
            return FALSE;
        }
        if(pSize) *pSize = pRsp->size;
    }
    return bRet;
}

// Obtain file system information
BOOL CDiagnostic::SendEFS2StatFsReq(char *pName, efs2_diag_statfs_rsp_type *pStatFs)
{
    BOOL bRet = TRUE;

    if(NULL == pName)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }

    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_statfs_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_statfs_req_type *pReq = (efs2_diag_statfs_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_STATFS);
    strcpy(pReq->path, pName);
    
    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_statfs_rsp_type *pRsp = (efs2_diag_statfs_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_statfs_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0)
        {
            return FALSE;
        }
        if(pStatFs) memcpy(pStatFs, pRsp, sizeof(efs2_diag_statfs_rsp_type));
    }
    return bRet;
}

// Get flash device info
BOOL CDiagnostic::SendEFS2DevInfoReq(efs2_diag_dev_info_rsp_type *pDevInfo)
{
    BOOL bRet = TRUE;

    if(NULL == pDevInfo)
    {
        return FALSE;
    }

    efs2_diag_dev_info_req_type devinfo;
    FSSUBSYS_PACKET_BUILT(devinfo, EFS2_DIAG_DEV_INFO);
    
    bRet = SendDiagPacket(&devinfo, sizeof(devinfo));
    if(bRet)
    {
        efs2_diag_dev_info_rsp_type *pRsp = (efs2_diag_dev_info_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_dev_info_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0)
        {
            return FALSE;
        }
        memcpy(pDevInfo, pRsp, sizeof(efs2_diag_dev_info_rsp_type));
    }
    return bRet;
}

// Truncate a file by the name
BOOL CDiagnostic::SendEFS2TruncateReq(char *pName, int32 nTruncLen)
{
    BOOL bRet = TRUE;

    if(NULL == pName)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }

    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_truncate_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_truncate_req_type *pReq = (efs2_diag_truncate_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_TRUNCATE);
    pReq->sequence_number   = 0;
    pReq->length            = nTruncLen;
    strcpy(pReq->path, pName);
    
    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_truncate_rsp_type *pRsp = (efs2_diag_truncate_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_truncate_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0 || pRsp->sequence_number != 0)
        {
            return FALSE;
        }
    }
    return bRet;
}

// Obtains extensive file system info
BOOL CDiagnostic::SendEFS2StatVFSReq(char *pName, efs2_diag_statvfs_v2_rsp_type *pStatVFS)
{
    BOOL bRet = TRUE;

    if(NULL == pName)
    {
        return FALSE;
    }
    
    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }

    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_statvfs_v2_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_statvfs_v2_req_type *pReq = (efs2_diag_statvfs_v2_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_STATVFS_V2);
    pReq->sequence_number   = 0;
    strcpy(pReq->path, pName);
    
    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_statvfs_v2_rsp_type *pRsp = (efs2_diag_statvfs_v2_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_statvfs_v2_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0)
        {
            return FALSE;
        }
        if(pStatVFS) memcpy(pStatVFS, pRsp, sizeof(efs2_diag_statvfs_v2_rsp_type));
    }
    return bRet;
}

// Format a Connected device
BOOL CDiagnostic::SendEFS2HotplugFormatReq(char *pName)
{
    BOOL bRet = TRUE;

    if(NULL == pName)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }

    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_hotplug_format_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_hotplug_format_req_type *pReq = (efs2_diag_hotplug_format_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_HOTPLUG_FORMAT);
    pReq->sequence_number   = 0;
    strcpy(pReq->path, pName);
    
    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_hotplug_format_rsp_type *pRsp = (efs2_diag_hotplug_format_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_hotplug_format_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0 || pRsp->sequence_number != 0)
        {
            return FALSE;
        }
    }
    return bRet;
}

// get the hotplug device info
BOOL CDiagnostic::SendEFS2HotplugDevInfoReq(char *pName, efs2_diag_hotplug_device_info_rsp_type *pHotplugInfo)
{
    BOOL bRet = TRUE;

    if(NULL == pName)
    {
        return FALSE;
    }

    int nLen = strlen(pName);
    if(m_nEFS2MaxPath != 0 && nLen > m_nEFS2MaxPath )
    {
        return FALSE;
    }

    BYTE bData[DEFAULT_MAX_FRAME_SIZE];
    nLen = nLen + sizeof(efs2_diag_hotplug_device_info_req_type);// path[1]可以作为NULL的长度。
    efs2_diag_hotplug_device_info_req_type *pReq = (efs2_diag_hotplug_device_info_req_type *)bData;

    FSSUBSYS_PACKET_BUILT(*pReq, EFS2_DIAG_HOTPLUG_DEVICE_INFO);
    pReq->sequence_number   = 0;
    strcpy(pReq->path, pName);
    
    bRet = SendDiagPacket(pReq, nLen);
    if(bRet)
    {
        efs2_diag_hotplug_device_info_rsp_type *pRsp = (efs2_diag_hotplug_device_info_rsp_type *)m_rspFrame.GetOriginData();
        if(m_rspFrame.GetOriginDataLen() != sizeof(efs2_diag_hotplug_device_info_rsp_type))
        {
            return FALSE;
        }
        
        m_nEFS2Err  = pRsp->diag_errno;
        if(m_nEFS2Err != 0 || pRsp->sequence_number != 0)
        {
            return FALSE;
        }
        if(pHotplugInfo) memcpy(pHotplugInfo, pRsp, sizeof(efs2_diag_hotplug_device_info_rsp_type));
    }
    return bRet;
}

BOOL CDiagnostic::EFS2Connect(void)
{
    if(FALSE == SendEFS2HelloReq())
    {
        return FALSE;
    }
    
    if(FALSE == SendEFS2QueryReq())
    {
        return FALSE;
    }
    return TRUE;
}

int32 CDiagnostic::EFS2OpenFile(CString &szName)
{
    char szFileName[FS_DIAG_MAX_READ_REQ];
    int32 nFd;
    szName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szFileName, szName, FS_DIAG_MAX_READ_REQ);
    if(FALSE == SendEFS2OpenReq(szFileName, O_RDWR, &nFd))
    {
        return -1;
    }
    return nFd;
}

int32 CDiagnostic::EFS2CreateFile(CString &szName)
{
    char szFileName[FS_DIAG_MAX_READ_REQ];
    int32 nFd;
    szName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szFileName, szName, FS_DIAG_MAX_READ_REQ);
    if(FALSE == SendEFS2OpenReq(szFileName, O_CREAT| O_RDWR, &nFd))
    {
        return -1;
    }
    return nFd;
}
int32 CDiagnostic::EFS2ReadFile(int32 nFd, byte *pBuf, uint32 dwLen, uint32 dwOffset)
{
    int32 nReadLen = (int32)dwLen;
    int32 nReaded;

    while(nReadLen > 0)
    {
        if(FALSE == SendEFS2ReadReq(nFd, pBuf, MIN(nReadLen,EFS2_DATAPACKET_LEN), dwOffset, &nReaded))
        {
            break;
        }
        pBuf        += nReaded;
        dwOffset    += nReaded;
        nReadLen    -= nReaded;
    }
    return (dwLen-nReadLen);
}

int32 CDiagnostic::EFS2WriteFile(int32 nFd, byte *pBuf, uint32 dwLen, uint32 dwOffset)
{
    int32 nWriteLen = (int32)dwLen;
    int32 nWritten;

    while(nWriteLen > 0)
    {
        if(FALSE == SendEFS2WriteReq(nFd, pBuf, MIN(nWriteLen,EFS2_DATAPACKET_LEN), dwOffset, &nWritten))
        {
            break;
        }
        pBuf        += nWritten;
        dwOffset    += nWritten;
        nWriteLen   -= nWritten;
    }
    return (dwLen-nWriteLen);
}

BOOL CDiagnostic::EFS2CloseFile(int32 nFd)
{
    return SendEFS2CloseReq(nFd);
}

BOOL CDiagnostic::EFS2RemoveFile(CString &szName)
{
    char szFileName[FS_DIAG_MAX_READ_REQ];
    szName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szFileName, szName, FS_DIAG_MAX_READ_REQ);
    return SendEFS2UnlinkReq(szFileName);
}

BOOL CDiagnostic::EFS2RenameFile(CString &szOldName, CString &szNewName)
{
    char szOldFileName[FS_DIAG_MAX_READ_REQ];
    char szNewFileName[FS_DIAG_MAX_READ_REQ];
    szOldName.Replace(_T('\\'),_T('/'));
    szNewName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szOldFileName, szOldName, FS_DIAG_MAX_READ_REQ);
    TCHARTOUTF8(szNewFileName, szNewName, FS_DIAG_MAX_READ_REQ);
    return SendEFS2RenameReq(szOldFileName, szNewFileName);
}

BOOL CDiagnostic::EFS2MkDir(CString &szName)
{
    char szFileName[FS_DIAG_MAX_READ_REQ];
    szName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szFileName, szName, FS_DIAG_MAX_READ_REQ);
    return SendEFS2MkDirReq(szFileName);
}

BOOL CDiagnostic::EFS2RmDir(CString &szName)
{
    char szFileName[FS_DIAG_MAX_READ_REQ];
    szName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szFileName, szName, FS_DIAG_MAX_READ_REQ);
    return SendEFS2RmDirReq(szFileName);
}

BOOL CDiagnostic::EFS2TruncateFile(CString &szName, int32 nTruncLen)
{
    char szFileName[FS_DIAG_MAX_READ_REQ];
    szName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szFileName, szName, FS_DIAG_MAX_READ_REQ);
    return SendEFS2TruncateReq(szFileName, nTruncLen);
}

int32 CDiagnostic::EFS2GetFileSize(CString &szName)
{
    char szFileName[FS_DIAG_MAX_READ_REQ];
    int32 nSize = 0;
    szName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szFileName, szName, FS_DIAG_MAX_READ_REQ);
    if(FALSE == SendEFS2StatReq(szFileName, &nSize))
    {
        return 0;
    }
    return nSize;
}

BOOL CDiagnostic::EFS2IsFileExist(CString &szName)
{
    char szFileName[FS_DIAG_MAX_READ_REQ];
    szName.Replace(_T('\\'),_T('/'));
    TCHARTOUTF8(szFileName, szName, FS_DIAG_MAX_READ_REQ);
    return SendEFS2StatReq(szFileName, NULL);
}
