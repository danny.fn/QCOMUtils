#include "StdAfx.h"
#include "HexFile.h"

CHexFile::CHexFile(void)
{
    InitParam();
}

CHexFile::CHexFile(LPCTSTR lpszFileName)
{
    InitParam();
    Open(lpszFileName);
}

CHexFile::~CHexFile(void)
{
}

void CHexFile::InitParam(void)
{
    m_dwStartAddr       = 0;
    m_dwSegmentSize     = 0;
    m_bSegmentEnumInit  = FALSE;
    m_bValid            = FALSE;
}

BOOL CHexFile::Open(LPCTSTR lpszFileName)
{
    CFileStatus myStatus;
    
    if(FALSE == GetStatus(lpszFileName, myStatus))
    {
        m_bValid = FALSE;
        return FALSE;
    }
    
    m_bValid = CStdioFile::Open(lpszFileName, CFile::typeText | CFile :: modeRead | CFile::shareDenyWrite);
    if(m_bValid)
    {
        SeekToBegin();
    }
    return m_bValid;
}

BOOL CHexFile::EnumSegmentInit(void)
{
    CString lineStr;
    HexRecord myRecord;
    
    if(!m_bValid)
    {
        return FALSE;
    }

    SeekToBegin();

    m_bSegmentEnumInit = FALSE;

    if(ReadString(lineStr))
    {
        // ReadString UNIX/MAC format have a bug
        Seek(lineStr.GetLength()+1,begin);
        if(DecodeRecord(&myRecord, lineStr.GetBuffer(), lineStr.GetLength()))
        {
            if(myRecord.bType == HEX_TYPE_EXT_SADDR || myRecord.bType == HEX_TYPE_EXT_LADDR)
            {
                memcpy(&m_HexExtSegInfo,&myRecord,sizeof(m_HexExtSegInfo));
                m_bSegmentEnumInit = TRUE;
            }
            else if(myRecord.bType == HEX_TYPE_START_SADDR || myRecord.bType == HEX_TYPE_START_LADDR)
            {
                m_dwStartAddr = myRecord.d.wStartLAddr;
                m_dwSegmentStartAddr = HEX_SEG_SIZE_MAX;
            }
        }
    }

    return m_bSegmentEnumInit;
}

BOOL CHexFile::EnumNextSegment(void)
{
    CString lineStr;
    HexRecord myRecord;

    if(!m_bValid || m_bSegmentEnumInit == FALSE)
    {
        return FALSE;
    }
    
    m_dwSegmentSize = 0;
    m_dwSegmentStartAddr = HEX_SEG_SIZE_MAX;
    m_dwExtAddr     = m_HexExtSegInfo.d.wExtLAddr;
    m_dwExtType     = m_HexExtSegInfo.bType;
    
    while(TRUE == ReadString(lineStr))
    {
        if(TRUE == DecodeRecord(&myRecord, lineStr.GetBuffer(), lineStr.GetLength()))
        {
            if(myRecord.bType == HEX_TYPE_EOF) 
            {
                m_bSegmentEnumInit = FALSE;
                break;
            }
            else if(myRecord.bType == HEX_TYPE_DATA)
            {
                m_dwSegmentStartAddr = min(m_dwSegmentStartAddr,myRecord.wAddr);
                memcpy(&m_pBuf[myRecord.wAddr], myRecord.d.bData, myRecord.bLen);
                m_dwSegmentSize += myRecord.bLen;
            }
            else if(myRecord.bType == HEX_TYPE_EXT_SADDR || myRecord.bType == HEX_TYPE_EXT_LADDR)
            {
                memcpy(&m_HexExtSegInfo,&myRecord,sizeof(m_HexExtSegInfo));
                break;
            }
            else if(myRecord.bType == HEX_TYPE_START_SADDR || myRecord.bType == HEX_TYPE_START_LADDR)
            {
                m_dwStartAddr = myRecord.d.wStartLAddr;
            }
        }
    }
    return (m_dwSegmentSize > 0 ? TRUE:FALSE);
}

BOOL CHexFile::DecodeRecord(HexRecord *pRecord, TCHAR *pData, WORD wLen)
{
    BYTE bChkSum = 0;
    BYTE bData;
    TCHAR H,L;
    int  i;
    
    if(wLen<HEX_RECORD_MIN_LEN || wLen>HEX_RECORD_MAX_LEN)
    {
        return FALSE;
    }
    
    if(*pData++ != HEX_RECORD_HEADER)
    {
        return FALSE;
    }
    
    // Len
    H = *pData++;
    L = *pData++;
    bData = CharsToHex(H, L);
    bChkSum += bData;
    pRecord->bLen = bData;
    
    // Addr
    H = *pData++;
    L = *pData++;
    bData = CharsToHex(H, L);
    bChkSum += bData;
    pRecord->wAddr = bData<<8;
    H = *pData++;
    L = *pData++;
    bData = CharsToHex(H, L);
    bChkSum += bData;
    pRecord->wAddr |= bData;
    
    // Type
    H = *pData++;
    L = *pData++;
    bData = CharsToHex(H, L);
    bChkSum += bData;
    pRecord->bType = bData;
    
    // Data
    switch(pRecord->bType){
    case HEX_TYPE_DATA:
        for(i=0;i<pRecord->bLen;i++)
        {
            H = *pData++;
            L = *pData++;
            bData = CharsToHex(H, L);
            bChkSum += bData;
            pRecord->d.bData[i] = bData;
        }
        break;
        
    case HEX_TYPE_EOF:
        break;
        
    case HEX_TYPE_EXT_SADDR:
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wExtSAddr = bData<<8;
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wExtSAddr |= bData;
        break;
        
    case HEX_TYPE_START_SADDR:
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wStartSAddr = bData<<24;
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wStartSAddr |= bData<<16;
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wStartSAddr |= bData<<8;
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wStartSAddr |= bData;
        break;
        
    case HEX_TYPE_EXT_LADDR:
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wExtLAddr = bData<<8;
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wExtLAddr |= bData;
        break;
        
    case HEX_TYPE_START_LADDR:
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wStartLAddr = bData<<24;
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wStartLAddr |= bData<<16;
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wStartLAddr |= bData<<8;
        H = *pData++;
        L = *pData++;
        bData = CharsToHex(H, L);
        bChkSum += bData;
        pRecord->d.wStartLAddr |= bData;
        break;
        
    default:
        return FALSE;
    }
    
    // CheckSum
    H = *pData++;
    L = *pData++;
    bData = CharsToHex(H, L);
    if(bData != ((0x100-bChkSum)&0xFF))
    {
        return FALSE;
    }
    return TRUE;
}

BYTE CHexFile::CharsToHex(TCHAR H, TCHAR L)
{
    if(H >= '0' && H <= '9')
    {
        H = H - '0';
    }
    else if(H >= 'a' && H <= 'f')
    {
        H = 0x0A + H - 'a';
    }
    else if(H >= 'A' && H <= 'F')
    {
        H = 0x0A + H - 'A';
    }
    else
    {
        H = 0;
    }
    
    if(L >= '0' && L <= '9')
    {
        L = L - '0';
    }
    else if(L >= 'a' && L <= 'f')
    {
        L = 0x0A + L - 'a';
    }
    else if(L >= 'A' && L <= 'F')
    {
        L = 0x0A + L - 'A';
    }
    else
    {
        L = 0;
    }

    return (BYTE)((H<<4)|L);
}