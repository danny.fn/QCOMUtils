/*=================================================================================================

  FILE : HDLCFRAME.H

  SERVICES: Declaration of an MFC wrapper class for Async-HDLC frame's encapsulation and getting
  origin data from a Async-HDLC frame

  GENERAL DESCRIPTION:
  
  PUBLIC CLASSES AND STATIC FUNCTIONS:
  CHDLCFrame;
    
  INITIALIZATION AND SEQUENCING REQUIREMENTS:
  
====================================================================================================*/


/*====================================================================================================

                                       ==MODIFY HISTOY FOR FILE==

  when         who     what, where, why
  ----------   ---     ----------------------------------------------------------
  12/23/2003   tyz      create
  
=====================================================================================================*/
#if !defined(_HDLCFRAME_H__)
#define _HDLCFRAME_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*===========================================================================

                  INCLUDE FILES FOR MODULE
  
===========================================================================*/
//#include ""


/*===========================================================================

                  DEFINITIONS AND CONSTANTS
  
===========================================================================*/
//define the default char for frame encapsulation

#define	DEFAULT_ESC_CHAR			0x7d	/* Escapes special chars (0x7d, 0x7e)*/
#define	DEFAULT_FLAG_CHAR 		    0x7e	/* Ends char for frame */
#define	DEFAULT_ESC_COMPL			0x20	/* XORed with special chars in data */

#define  DEFAULT_MAX_FRAME_SIZE     8192

/*===========================================================================

                     GLOBAL DATA

===========================================================================*/



/*===========================================================================

                     LOCAL/STATIC DATA

===========================================================================*/



/*===========================================================================

                     TYPE DECLARATIONS
  
===========================================================================*/



/*===========================================================================

                      CLASS DEFINITIONS

===========================================================================*/

class CHDLCFrame  : virtual public CObject
{
private:
   //data member
   BYTE         m_bHDLCFrame[DEFAULT_MAX_FRAME_SIZE];     //a buffer of HDLC-frame data 
   BYTE         m_bOriginData[DEFAULT_MAX_FRAME_SIZE];    //a buffer of origin data
   DWORD        m_dwHDLCFrameLen;
   DWORD        m_dwOriginDatalen;
   BYTE         m_EscChar;                                //the char to escape special chars  
   BYTE         m_FlagChar;                               //the char to be the flag of a HDLC-frame
   BYTE         m_EscComplement;                          //the chat to XORed with special chars in data
   BOOL         m_bHeadEnc;
 
   //Implementation
   int          CRCEncode(BYTE* pData, WORD length);
   BOOL         CRCCheck(BYTE* pData, WORD length);
public:
   // Construction and Destruction
   CHDLCFrame(char escChar = DEFAULT_ESC_CHAR, 
              char flagChar = DEFAULT_FLAG_CHAR, 
              char escCompl = DEFAULT_ESC_COMPL);
   
   virtual ~CHDLCFrame();

   //property   
   BYTE*        GetHDLCFrame(void)      {return m_bHDLCFrame;}
   BYTE*        GetOriginData(void)     {return m_bOriginData;}
   DWORD        GetHDLCFrameLen(void)   {return m_dwHDLCFrameLen;}
   DWORD        GetOriginDataLen(void)  {return m_dwOriginDatalen;}
   void         Reset(void)             {m_dwOriginDatalen=0;m_dwHDLCFrameLen=0;};

   //Implementation
   DWORD        Encode(char* pOriginData, WORD length, BOOL isHeadEnc = FALSE, BOOL bHDLCEncode = TRUE);
   BOOL         Decode(char* pHDLCFrame,  WORD length);
   BOOL         IsHeadEnc(void)  {return this->m_bHeadEnc;};
};
#endif // !defined(_HDLCFRAME_H__)