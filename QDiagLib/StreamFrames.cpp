#include "StdAfx.h"
#include "StreamFrames.h"

CStreamFrames::CStreamFrames(void)
{
    InitParam();
}

CStreamFrames::~CStreamFrames(void)
{
    Release();
}


BOOL CStreamFrames::Create(int nWindowSize, DWORD dwTimeoutMS)
{
    if(TRUE == m_bValid)
    {
        Release();
    }
    m_dwTimeoutMS = dwTimeoutMS;
    m_nWindowSize = nWindowSize;
    m_pArray = new CHDLCFrameLink[m_nWindowSize];
    if(m_pArray != NULL)
    {
        m_bValid = TRUE;
        InitLink();
    }
    return m_bValid;
}

BOOL CStreamFrames::Release(void)
{
    if(FALSE == m_bValid)
    {
        return TRUE;
    }

    if(m_pArray)
    {
        delete []m_pArray;
    }
    InitParam();
    return TRUE;
}

CHDLCFrame* CStreamFrames::InsertFrame(DWORD dwKey, void *pReqData, int nReqSize, BOOL bHeadEnc, BOOL bHDLCEncode)
{
    CHDLCFrameLink *pCurrent = GetLink();
    if(FALSE == m_bValid)
    {
        return NULL;
    }

    if(!pCurrent)
    {
        return NULL;
    }
    
    pCurrent->m_dwKey       = dwKey;
    if(pReqData)
    {
        pCurrent->m_Frame.Encode((char *)pReqData, nReqSize, bHeadEnc, bHDLCEncode);
    }
    return &pCurrent->m_Frame;
}

BOOL CStreamFrames::RemoveFrame(DWORD dwKey)
{
    m_CritiSect.Lock();
    CHDLCFrameLink *pCurrent = m_pWait;
    CHDLCFrameLink *pPrev    = NULL;
    while(pCurrent)
    {
        if(pCurrent->m_dwKey == dwKey)
        {
            if(pPrev)
            {
                pPrev->m_pNext = pCurrent->m_pNext;
            }
            else
            {
                m_pWait = m_pWait->m_pNext;
            }
            pCurrent->m_pNext = m_pFree;
            m_pFree = pCurrent;
            pCurrent->m_Frame.Reset();
            pCurrent->m_dwTimeStamp = 0; // 标示此Frame已经释放
            m_CritiSect.Unlock();
            return TRUE;
        }
        pPrev    = pCurrent;
        pCurrent = pCurrent->m_pNext;
    }
    m_CritiSect.Unlock();
    return FALSE;
}

CHDLCFrame *CStreamFrames::GetTimeoutFrame(void)
{
    m_CritiSect.Lock();
    CHDLCFrameLink *pTimeout = PeekTimeoutLink();
    if(pTimeout)
    {
        pTimeout->m_dwTimeStamp = GetTickCount();
        pTimeout->m_nFrameIndex = m_nFrameIndex++;
        m_CritiSect.Unlock();
        return &(pTimeout->m_Frame);
    }
    m_CritiSect.Unlock();
    return NULL;
}

void CStreamFrames::InitParam(void)
{
    m_bValid            = FALSE;
    m_pArray            = NULL;
    m_pWait             = NULL;
    m_pFree             = NULL;
    m_nWindowSize       = 0;
    m_nFrameIndex       = 0;
    m_dwTimeoutMS       = 0; // GetTickCount();
}

void CStreamFrames::InitLink(void)
{
    m_CritiSect.Lock();
    m_pFree = m_pArray;
    for(int i=0; i< m_nWindowSize-1; i++)
    {
        m_pArray[i].m_pNext = &m_pArray[i+1];
    }
    m_pArray[m_nWindowSize-1].m_pNext = NULL;
    m_CritiSect.Unlock();
}

CHDLCFrameLink* CStreamFrames::GetLink(void)
{
    CHDLCFrameLink *pCurrent;
    m_CritiSect.Lock();
    pCurrent = m_pFree;
    if(pCurrent)
    {
        pCurrent->m_dwTimeStamp = GetTickCount();
        pCurrent->m_nFrameIndex = m_nFrameIndex++;
        m_pFree = m_pFree->m_pNext;
        pCurrent->m_pNext = m_pWait;
        m_pWait = pCurrent;
    }
    m_CritiSect.Unlock();
    return pCurrent;
}

CHDLCFrameLink *CStreamFrames::PeekTimeoutLink(void)
{
    m_CritiSect.Lock();
    CHDLCFrameLink *pCurrent = m_pWait;
    while(pCurrent)
    {
        if(pCurrent->m_dwTimeStamp > 0)
        {
            if((m_nFrameIndex-pCurrent->m_nFrameIndex)>(2*m_nWindowSize) || (GetTickCount()-pCurrent->m_dwTimeStamp)>=m_dwTimeoutMS)
            {
                break;
            }
        }
        pCurrent = pCurrent->m_pNext;
    }
    m_CritiSect.Unlock();
    return pCurrent;
}