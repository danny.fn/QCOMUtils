// SNGenDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "SNGen.h"
#include "SNGenDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CSNGenDlg 对话框




CSNGenDlg::CSNGenDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSNGenDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSNGenDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_EDIT_VALUE, m_cEditValue);
    DDX_Control(pDX, IDC_EDIT_NUMBER, m_cEditNumber);
    DDX_Control(pDX, IDOK, m_cButtonGen);
}

BEGIN_MESSAGE_MAP(CSNGenDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
    ON_BN_CLICKED(IDOK, &CSNGenDlg::OnBnClickedOk)
    ON_EN_CHANGE(IDC_EDIT_VALUE, &CSNGenDlg::OnEnChangeEditValue)
    ON_BN_CLICKED(IDC_RADIO_MEID, &CSNGenDlg::OnBnClickedRadioMeid)
    ON_BN_CLICKED(IDC_RADIO_ESN, &CSNGenDlg::OnBnClickedRadioEsn)
    ON_BN_CLICKED(IDC_RADIO_IMEI, &CSNGenDlg::OnBnClickedRadioImei)
END_MESSAGE_MAP()


// CSNGenDlg 消息处理程序

BOOL CSNGenDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
    ((CButton *)GetDlgItem(IDC_RADIO_MEID))->SetCheck(TRUE);//选上
    nMode = MODE_MEID;
    
    m_cEditNumber.SetLimitText(8);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CSNGenDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CSNGenDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CSNGenDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CSNGenDlg::OnBnClickedOk()
{
    // TODO: 在此添加控件通知处理程序代码
    CString szValue;
    CString szNumber;

    m_cEditValue.GetWindowText(szValue);
    m_cEditNumber.GetWindowText(szNumber);
    
    if(szValue.IsEmpty()||szNumber.IsEmpty())
	{
        ShowMessageBox(IDS_ERROR_INPUT);
		return;
	}
    
    if(nMode == MODE_MEID)
    {
        if(szValue.GetLength()!=14)
	    {
		    ShowMessageBox(IDS_ERROR_INPUT);
		    return;
	    }
    }
    else if(nMode == MODE_ESN)
    {
        if(szValue.GetLength()!=8)
	    {
		    ShowMessageBox(IDS_ERROR_INPUT);
		    return;
	    }
    }
    else
    {
        if(szValue.GetLength()!=14)
	    {
		    ShowMessageBox(IDS_ERROR_INPUT);
		    return;
	    }
    }
    
    m_cButtonGen.EnableWindow(false);
    RecordSN(szValue,_tcstol(szNumber,NULL,10));
    m_cButtonGen.EnableWindow(true);
    //OnOK();
}

void CSNGenDlg::OnEnChangeEditValue()
{
    // TODO:  如果该控件是 RICHEDIT 控件，则它将不会
    // 发送该通知，除非重写 CDialog::OnInitDialog()
    // 函数并调用 CRichEditCtrl().SetEventMask()，
    // 同时将 ENM_CHANGE 标志“或”运算到掩码中。

    CString szCode;
	
	m_cEditValue.GetWindowText(szCode);
    if(!IsCodeAvaliable(szCode))
    {
		if(szCode.GetLength()>0)
		{
			szCode.Delete(szCode.GetLength()-1);
			m_cEditValue.SetWindowText(szCode);
			m_cEditValue.SetSel(szCode.GetLength(),szCode.GetLength());
		}
	}
}

bool CSNGenDlg::IsCodeAvaliable(CString &szCode)
{
	if(szCode.IsEmpty())
	{
		return false;
	}
    
    if(nMode == MODE_MEID)
    {
        if(szCode.GetLength()>14)
	    {
		    return false;
	    }
    	
	    for(int i=0; i<szCode.GetLength(); i++)
	    {
		    TCHAR c = szCode.GetAt(i);
		    if((c>_T('9') || c<_T('0'))&&(c>_T('F') || c<_T('A')))
		    {
			    return false;
		    }
	    }
    }
    else if(nMode == MODE_ESN)
    {
        if(szCode.GetLength()>8)
	    {
		    return false;
	    }
    	
	    for(int i=0; i<szCode.GetLength(); i++)
	    {
		    TCHAR c = szCode.GetAt(i);
		    if((c>_T('9') || c<_T('0'))&&(c>_T('F') || c<_T('A')))
		    {
			    return false;
		    }
	    }
    }
    else
    {
        if(szCode.GetLength()>14)
	    {
		    return false;
	    }
    	
	    for(int i=0; i<szCode.GetLength(); i++)
	    {
		    TCHAR c = szCode.GetAt(i);
		    if(c>_T('9') || c<_T('0'))
		    {
			    return false;
		    }
	    }
    }

	return true;
}

void CSNGenDlg::OnBnClickedRadioMeid()
{
    nMode = MODE_MEID;
}

void CSNGenDlg::OnBnClickedRadioEsn()
{
    nMode = MODE_ESN;
}

void CSNGenDlg::OnBnClickedRadioImei()
{
    nMode = MODE_IMEI;
}

void CSNGenDlg::RecordSN(CString &szValue, int nNum)
{
    CStdioFile  myHistFile;
    CString     szFileName;
    byte SP;
    CString     szCurrCode,szCalc;
    
    szCalc = szCurrCode = szValue;
    switch(nMode){
    case MODE_MEID:
        szFileName.Format(_T(".\\MEID-%s-%08d.txt"),szValue,nNum);
        myHistFile.Open(szFileName,CStdioFile::modeReadWrite|CStdioFile::modeCreate|CStdioFile::modeNoTruncate);
        //myHistFile.SeekToEnd();
        for(int i=0;i<nNum;i++)
        {
            //SP = CalcIMEISP(szCurrCode);
            //szCurrCode.AppendChar(HexToChar(SP));
            szCurrCode += _T("\n");
            myHistFile.WriteString(szCurrCode);
            szCalc = szCurrCode = AddHEXString(szCalc);
        }
        break;
    case MODE_ESN:
        szFileName.Format(_T(".\\ESN-%s-%08d.txt"),szValue,nNum);
        myHistFile.Open(szFileName,CStdioFile::modeReadWrite|CStdioFile::modeCreate|CStdioFile::modeNoTruncate);
        //myHistFile.SeekToEnd();
        for(int i=0;i<nNum;i++)
        {
            szCurrCode += _T("\n");
            myHistFile.WriteString(szCurrCode);
            szCalc = szCurrCode = AddHEXString(szCalc);
        }
        break;
    default:
    case MODE_IMEI:
        szFileName.Format(_T(".\\IMEI-%s-%08d.txt"),szValue,nNum);
        myHistFile.Open(szFileName,CStdioFile::modeReadWrite|CStdioFile::modeCreate|CStdioFile::modeNoTruncate);
        //myHistFile.SeekToEnd();
        for(int i=0;i<nNum;i++)
        {
            SP = CalcIMEISP(szCalc);
            szCurrCode.AppendChar(SP+'0');
            szCurrCode += _T("\n");
            myHistFile.WriteString(szCurrCode);
            szCalc = szCurrCode = AddDECString(szCalc);
        }
        break;
    }
    myHistFile.Close();
}


byte CSNGenDlg::CalcIMEISP(CString &szCode)
{
    // 计算校验位
    // IMEI校验码算法：
    // (1).将偶数位数字分别乘以2，分别计算个位数和十位数之和
    // (2).将奇数位数字相加，再加上上一步算得的值
    // (3).如果得出的数个位是0则校验位为0，否则为10减去个位数
    // 如：35 89 01 80 69 72 41 偶数位乘以2得到5*2=10 9*2=18 1*2=02 0*2=00 9*2=18 2*2=04 1*2=02,
    //    计算奇数位数字之和和偶数位个位十位之和，得到 3+(1+0)+8+(1+8)+0+(0+2)+8+(0+0)+6+(1+8)+7+(0+4)+4+(0+2)=63 
    // => 校验位 10-3 = 7
    byte SP;
    byte oH = 0, oL = 0;

    SP  = szCode.GetAt(1) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(3) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(5) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(7) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(9) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(11) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = szCode.GetAt(13) - '0';
    SP  = 2*SP;
    oH += SP/10;
    oL += SP%10;

    SP  = 0;
    SP += szCode.GetAt(0) - '0';
    SP += szCode.GetAt(2) - '0';
    SP += szCode.GetAt(4) - '0';
    SP += szCode.GetAt(6) - '0';
    SP += szCode.GetAt(8) - '0';
    SP += szCode.GetAt(10) - '0';
    SP += szCode.GetAt(12) - '0';
    SP += oH + oL;

    SP = SP%10;
    if(SP > 0)
    {
        SP = 10 - SP;
    }
    return SP;
}


byte CSNGenDlg::CalcMEIDSP(CString &szCode)
{
    // 计算校验位
    // MEID校验码算法：
    // (1).将偶数位数字分别乘以2，分别计算个位数和十位数之和，注意是16进制数
    // (2).将奇数位数字相加，再加上上一步算得的值
    // (3).如果得出的数个位是0则校验位为0，否则为10(这里的10是16进制)减去个位数
    // 如：AF 01 23 45 0A BC DE 偶数位乘以2得到F*2=1E 1*2=02 3*2=06 5*2=0A A*2=14 C*2=1C E*2=1C,
    // 计算奇数位数字之和和偶数位个位十位之和，得到 A+(1+E)+0+2+2+6+4+A+0+(1+4)+B+(1+8)+D+(1+C)=64 
    // => 校验位 10-4 = C

    byte SP;
    byte oH = 0, oL = 0;

    SP  = CharToHex(szCode.GetAt(1));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(3));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(5));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(7));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(9));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(11));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = CharToHex(szCode.GetAt(13));
    SP  = 2*SP;
    oH += SP/0x10;
    oL += SP%0x10;

    SP  = 0;
    SP += CharToHex(szCode.GetAt(0));
    SP += CharToHex(szCode.GetAt(2));
    SP += CharToHex(szCode.GetAt(4));
    SP += CharToHex(szCode.GetAt(6));
    SP += CharToHex(szCode.GetAt(8));
    SP += CharToHex(szCode.GetAt(10));
    SP += CharToHex(szCode.GetAt(12));
    SP += oH + oL;

    SP = SP%0x10;
    if(SP > 0)
    {
        SP = 0x10 - SP;
    }
    return SP;
}

void CSNGenDlg::ShowMessageBox(int nResID)
{
    CString szTitle;
    CString szText;

    szTitle.LoadString(IDS_ERROR);
    szText.LoadString(nResID);

    MessageBox(szText, szTitle, MB_OK);
}

byte CSNGenDlg::CharToHex(TCHAR ch)
{
    byte Hex = 0;

    if(ch >= '0' && ch <= '9')
    {
        Hex = ch - '0';
    }
    else if(ch >= 'A' && ch <= 'F')
    {
        Hex = 0xA+(ch - 'A');
    }
    else if(ch >= 'a' && ch <= 'f')
    {
        Hex = 0xA + (ch - 'a');
    }
    return Hex;
}

TCHAR CSNGenDlg::HexToChar(byte Hex)
{
    TCHAR ch = '0';

    if(Hex >= 0 && Hex <= 9)
    {
        ch = Hex + '0';
    }
    else if(Hex >= 0xA && Hex <= 0xF)
    {
        ch = (Hex-0xA) + 'A';
    }
    return ch;
}

CString CSNGenDlg::AddHEXString(CString &szCode)
{
    CString szRet = szCode;
    TCHAR   cCurr;
    int     nCurr, nNext;
    
    nNext = 1;
    for(int i=szCode.GetLength()-1;i>=0;i--)
    {
        cCurr = szCode.GetAt(i);
        if(cCurr>=_T('0') && cCurr<=_T('9'))
        {
            nCurr = cCurr-_T('0');
        }
        else if(cCurr>=_T('a') && cCurr<=_T('f'))
        {
            nCurr = cCurr - _T('a') + 0xA;
        }
        else if(cCurr>=_T('A') && cCurr<=_T('F'))
        {
            nCurr = cCurr - _T('A') + 0xA;
        }
        else
        {
            nCurr = 0;
        }
        
        nCurr = nCurr + nNext;
        nNext = nCurr/16;
        nCurr = nCurr%16;
        
        if(nCurr>=0xA)
        {
            cCurr = nCurr+_T('A') - 0xA;
        }
        else
        {
            cCurr = nCurr+_T('0');
        }

        szRet.SetAt(i,cCurr);
        if(nNext == 0)
        {
            break;
        }
    }

    if(nNext)
    {
        szRet = _T('1') + szRet;
    }

    return szRet;
}

CString CSNGenDlg::AddDECString(CString &szCode)
{
    CString szRet = szCode;
    TCHAR   cCurr;
    int     nCurr, nNext;
    
    nNext = 1;
    for(int i=szCode.GetLength()-1;i>=0;i--)
    {
        cCurr = szCode.GetAt(i);
        if(cCurr>=_T('0') && cCurr<=_T('9'))
        {
            nCurr = cCurr-_T('0');
        }
        else
        {
            nCurr = 0;
        }
        
        nCurr = nCurr + nNext;
        nNext = nCurr/10;
        nCurr = nCurr%10;
        cCurr = nCurr+_T('0');
        szRet.SetAt(i,cCurr);
        if(nNext == 0)
        {
            break;
        }
    }

    if(nNext)
    {
        szRet = _T('1') + szRet;
    }

    return szRet;
}