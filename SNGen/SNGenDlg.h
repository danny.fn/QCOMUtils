// SNGenDlg.h : 头文件
//

#pragma once
#include "afxwin.h"

#define MODE_IMEI  0
#define MODE_MEID  1
#define MODE_ESN   2

// CSNGenDlg 对话框
class CSNGenDlg : public CDialog
{
// 构造
public:
	CSNGenDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_SNGEN_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

    bool IsCodeAvaliable(CString &szCode);
    void RecordSN(CString &szCurrCode, int nNum);
    byte CalcIMEISP(CString &szCode);
    byte CalcMEIDSP(CString &szCode);
    void ShowMessageBox(int nResID);
    byte CharToHex(TCHAR ch);
    TCHAR HexToChar(byte Hex);
    CString AddHEXString(CString &szCode);
    CString AddDECString(CString &szCode);

public:
    int nMode;

    CEdit m_cEditValue;
    CEdit m_cEditNumber;
    afx_msg void OnBnClickedOk();
    afx_msg void OnEnChangeEditValue();
    afx_msg void OnBnClickedRadioMeid();
    afx_msg void OnBnClickedRadioEsn();
    afx_msg void OnBnClickedRadioImei();
    CButton m_cButtonGen;
};
