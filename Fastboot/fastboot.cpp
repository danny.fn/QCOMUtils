/*
 * Copyright (C) 2008 The Android Open Source Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the 
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
#include "stdafx.h"
#include <usb100.h>
#include "fastboot.h"
#include "DLoadThread.h"
#include "usb_fastboot.h"
#include "adb_api.h"
#include <winioctl.h>

void get_my_path(char exe[MAX_PATH]);
void bootimg_set_cmdline(boot_img_hdr *h, const char *cmdline);
boot_img_hdr *mkbootimg(void *kernel, unsigned kernel_size,
                        void *ramdisk, unsigned ramdisk_size,
                        void *second, unsigned second_size,
                        unsigned page_size, unsigned base,
                        unsigned *bootimg_size);
void *load_file(const char *fn, unsigned *_sz);

static const GUID adb_usb_class_id = ANDROID_USB_CLASS_ID;

CFastboot::CFastboot(void)
{
    usb = 0;
    serial = 0;
    product = 0;
    cmdline = 0;
    wipe_data = 0;
    vendor_id = 0;

    base_addr = 0x10000000;
    g_pThread = NULL;
    open_usb = 0;
    action_list = 0;
    action_last = 0;
    memcpy(&usb_class_id,&adb_usb_class_id,sizeof(GUID));

    ParentHubIndex = 0;
    ParentPortIndex = 0;
    bLimitSize = FALSE;
}

CFastboot::~CFastboot(void)
{
}

char *CFastboot::find_item(const char *item, const char *product)
{
    char *dir;
    char *fn;
    char path[MAX_PATH + 128];

    if(!strcmp(item,"boot")) {
        fn = "boot.img";
    } else if(!strcmp(item,"recovery")) {
        fn = "recovery.img";
    } else if(!strcmp(item,"system")) {
        fn = "system.img";
    } else if(!strcmp(item,"userdata")) {
        fn = "userdata.img";
    } else if(!strcmp(item,"info")) {
        fn = "android-info.txt";
    } else {
        fprintf(stderr,"unknown partition '%s'\n", item);
        return 0;
    }

    if(product) {
        get_my_path(path);
        sprintf(path + strlen(path),
                "../../../target/product/%s/%s", product, fn);
        return strdup(path);
    }
        
    dir = getenv("ANDROID_PRODUCT_OUT");
    if((dir == 0) || (dir[0] == 0)) {
        return 0;
    }
    
    sprintf(path, "%s/%s", dir, fn);
    return strdup(path);
}

int CFastboot::match_fastboot(usb_ifc_info *info, void *puser, TCHAR *devname)
{
    CFastboot *pThis = (CFastboot *)puser;
    if(!(pThis->vendor_id && (info->dev_vendor == pThis->vendor_id)) &&
       (info->dev_vendor != 0x18d1) &&  // Google
       (info->dev_vendor != 0x0451) &&
       (info->dev_vendor != 0x0502) &&
       (info->dev_vendor != 0x0fce) &&  // Sony Ericsson
       (info->dev_vendor != 0x05c6) &&  // Qualcomm
       (info->dev_vendor != 0x22b8) &&  // Motorola
       (info->dev_vendor != 0x0955) &&  // Nvidia
       (info->dev_vendor != 0x413c) &&  // DELL
       (info->dev_vendor != 0x0bb4))    // HTC
            return -1;
    if(info->ifc_class != 0xff) return -1;
    if(info->ifc_subclass != 0x42) return -1;
    if(info->ifc_protocol != 0x03) return -1;
    // require matching serial number if a serial number is specified
    // at the command line with the -s option.
    if (pThis->serial && strcmp(pThis->serial, info->serial_number) != 0) return -1;
    if(!pThis->myUSBPorts.FindUsbDeviceByDevPath(devname, &pThis->usb_class_id)) return -1;
    if(pThis->ParentHubIndex !=0 && pThis->ParentPortIndex!=0)
    {
        if(!pThis->myUSBPorts.IsSamePort(pThis->ParentHubIndex,pThis->ParentPortIndex)) return -1;
    }
    return 0;
}

usb_handle *CFastboot::open_device(void)
{
    if(open_usb) return open_usb;
    for(;IsRunning();) {
        open_usb = usb_open(match_fastboot);
        if(open_usb) return open_usb;
        Sleep(1);
    }
    return open_usb;
}

void *CFastboot::load_bootable_image(unsigned page_size, const char *kernel, const char *ramdisk,
                          unsigned *sz, const char *cmdline)
{
    void *kdata = 0, *rdata = 0;
    unsigned ksize = 0, rsize = 0;
    void *bdata;
    unsigned bsize;

    if(kernel == 0) {
        fprintf(stderr, "no image specified\n");
        return 0;
    }

    kdata = load_file(kernel, &ksize);
    if(kdata == 0) {
        fprintf(stderr, "cannot load '%s'\n", kernel);
        return 0;
    }
    
        /* is this actually a boot image? */
    if(!memcmp(kdata, BOOT_MAGIC, BOOT_MAGIC_SIZE)) {
        if(cmdline) bootimg_set_cmdline((boot_img_hdr*) kdata, cmdline);
        
        if(ramdisk) {
            fprintf(stderr, "cannot boot a boot.img *and* ramdisk\n");
            return 0;
        }
        
        *sz = ksize;
        return kdata;
    }

    if(ramdisk) {
        rdata = load_file(ramdisk, &rsize);
        if(rdata == 0) {
#ifdef _WINDOWS
            free(kdata);
#endif
            fprintf(stderr,"cannot load '%s'\n", ramdisk);
            return  0;
        }
    }

    fprintf(stderr,"creating boot image...\n");
    bdata = mkbootimg(kdata, ksize, rdata, rsize, 0, 0, page_size, base_addr, &bsize);
#ifdef _WINDOWS
    free(kdata);
#endif
    if(bdata == 0) {
        fprintf(stderr,"failed to create boot.img\n");
        return 0;
    }
    if(cmdline) bootimg_set_cmdline((boot_img_hdr*) bdata, cmdline);
    fprintf(stderr,"creating boot image - %d bytes\n", bsize);
    *sz = bsize;
    
    return bdata;
}

void *CFastboot::unzip_file(zipfile_t zip, const char *name, unsigned *sz)
{
    void *data;
    zipentry_t entry;
    unsigned datasz;
    
    entry = lookup_zipentry(zip, name);
    if (entry == NULL) {
        fprintf(stderr, "archive does not contain '%s'\n", name);
        return 0;
    }

    *sz = get_zipentry_size(entry);

    datasz = (unsigned)(*sz * 1.001);
    data = malloc(datasz);

    if(data == 0) {
        fprintf(stderr, "failed to allocate %d bytes\n", *sz);
        return 0;
    }

    if (decompress_zipentry(entry, data, datasz)) {
        fprintf(stderr, "failed to unzip '%s' from archive\n", name);
        free(data);
        return 0;
    }

    return data;
}

char *CFastboot::strip(char *s)
{
    int n;
    while(*s && isspace(*s)) s++;
    n = strlen(s);
    while(n-- > 0) {
        if(!isspace(s[n])) break;
        s[n] = 0;
    }
    return s;
}

void CFastboot::do_update_signature(zipfile_t zip, char *fn)
{
    void *data;
    unsigned sz;
    data = unzip_file(zip, fn, &sz);
    if (data == 0) return;
    fb_queue_download("signature", data, sz);
    fb_queue_command("signature", "installing signature");
}

void CFastboot::do_update(char *fn)
{
    void *zdata;
    unsigned zsize;
    void *data;
    unsigned sz;
    zipfile_t zip;
#ifndef _WINDOWS
    queue_info_dump();
#endif
    zdata = load_file(fn, &zsize);
#ifdef _WINDOWS
    if (zdata == 0) return;
#else
    if (zdata == 0) die("failed to load '%s'", fn);
#endif
    zip = init_zipfile(zdata, zsize);
#ifdef _WINDOWS
    if(zip == 0){
        free(zdata);
        return;
    }
#else
    if(zip == 0) die("failed to access zipdata in '%s'");
#endif
#ifndef _WINDOWS
    data = unzip_file(zip, "android-info.txt", &sz);
    if (data == 0) {
        char *tmp;
            /* fallback for older zipfiles */
        data = unzip_file(zip, "android-product.txt", &sz);
        if ((data == 0) || (sz < 1)) {
            die("update package has no android-info.txt or android-product.txt");
        }
        tmp = (char *)malloc(sz + 128);
        if (tmp == 0) die("out of memory");
        sprintf(tmp,"board=%sversion-baseband=0.66.04.19\n",(char*)data);
        data = tmp;
        sz = strlen(tmp);
    }

    setup_requirements((char *)data, sz);
#endif
    data = unzip_file(zip, "boot.img", &sz);
#ifdef _WINDOWS
    if (data == 0){
        free(zdata);
        free(zip);
        return;
    }
#else
    if (data == 0) die("update package missing boot.img");
#endif
    do_update_signature(zip, "boot.sig");
    fb_queue_flash("boot", data, sz);

    data = unzip_file(zip, "recovery.img", &sz);
    if (data != 0) {
        do_update_signature(zip, "recovery.sig");
        fb_queue_flash("recovery", data, sz);
    }

    data = unzip_file(zip, "system.img", &sz);
#ifdef _WINDOWS
    if (data == 0){
        free(zdata);
        free(zip);
        return;
    }
#else
    if (data == 0) die("update package missing system.img");
#endif
    do_update_signature(zip, "system.sig");
    fb_queue_flash("system", data, sz);
#ifdef _WINDOWS
    free(zdata);
    free(zip);
#endif
}

void CFastboot::do_send_signature(char *fn)
{
    void *data;
    unsigned sz;
    char *xtn;
	
    xtn = strrchr(fn, '.');
    if (!xtn) return;
    if (strcmp(xtn, ".img")) return;
	
    strcpy(xtn,".sig");
    data = load_file(fn, &sz);
    strcpy(xtn,".img");
    if (data == 0) return;
    fb_queue_download("signature", data, sz);
    fb_queue_command("signature", "installing signature");
}

void CFastboot::do_flashall(void)
{
    char *fname;
    void *data;
    unsigned sz;
#ifndef _WINDOWS
    queue_info_dump();
    fname = find_item("info", product);
    if (fname == 0) die("cannot find android-info.txt");
    data = load_file(fname, &sz);
    if (data == 0) die("could not load android-info.txt");
    setup_requirements((char *)data, sz);
#endif
    fname = find_item("boot", product);
    data = load_file(fname, &sz);
#ifdef _WINDOWS
    if (data == 0){
        
        return;
    }
#else
    if (data == 0) die("could not load boot.img");
#endif
    do_send_signature(fname);
#ifdef _WINDOWS
    free(fname);
#endif
    fb_queue_flash("boot", data, sz);

    fname = find_item("recovery", product);
    data = load_file(fname, &sz);
    if (data != 0) {
        do_send_signature(fname);
        fb_queue_flash("recovery", data, sz);
    }
#ifdef _WINDOWS
    if(fname) free(fname);
#endif
    fname = find_item("system", product);
    data = load_file(fname, &sz);
#ifdef _WINDOWS
    if (data == 0){
        free(fname);
        return;
    }
#else
    if (data == 0) die("could not load system.img");
#endif
    do_send_signature(fname);
    fb_queue_flash("system", data, sz);   
#ifdef _WINDOWS
    if(fname) free(fname);
#endif
}

#define skip(n) do { argc -= (n); argv += (n); } while (0)
#ifndef _WINDOWS
#define require(n) do { if (argc < (n)) usage(); } while (0)
#else
#define require(n) do {} while (0)
#endif
int CFastboot::do_oem_command(int argc, char **argv)
{
    char command[256];
    if (argc <= 1) return 0;
    
    command[0] = 0;
    while(1) {
        strcat(command,*argv);
        skip(1);
        if(argc == 0) break;
        strcat(command," ");
    }

    fb_queue_command(command,"");    
    return 0;
}

int CFastboot::fastboot_main(int argc, char **argv)
{
    int wants_wipe = 0;
    int wants_reboot = 0;
    int wants_reboot_bootloader = 0;
    void *data;
    unsigned sz;
    unsigned page_size = 2048;
#ifndef _WINDOWS
    skip(1);
#endif
    if (argc == 0) {
#ifndef _WINDOWS
        usage();
#endif
        return 0;
    }

    serial = getenv("ANDROID_SERIAL");

    while (argc > 0) {
        if(!strcmp(*argv, "-w")) {
            wants_wipe = 1;
            skip(1);
        } else if(!strcmp(*argv, "-b")) {
            require(2);
            base_addr = strtoul(argv[1], 0, 16);
            skip(2);
        } else if(!strcmp(*argv, "-n")) {
            require(2);
            page_size = (unsigned)strtoul(argv[1], NULL, 0);
#ifdef _WINDOWS
            if (!page_size) return -1;
#else
            if (!page_size) die("invalid page size");
#endif
            skip(2);
        } else if(!strcmp(*argv, "-s")) {
            require(2);
            serial = argv[1];
            skip(2);
        } else if(!strcmp(*argv, "-p")) {
            require(2);
            product = argv[1];
            skip(2);
        } else if(!strcmp(*argv, "-c")) {
            require(2);
            cmdline = argv[1];
            skip(2);
        } else if(!strcmp(*argv, "-i")) {
            char *endptr = NULL;
            unsigned long val;

            require(2);
            val = strtoul(argv[1], &endptr, 0);
            if (!endptr || *endptr != '\0' || (val & ~0xffff))
#ifdef _WINDOWS
                return -1;
#else
                die("invalid vendor id '%s'", argv[1]);
#endif
            vendor_id = (unsigned short)val;
            skip(2);
        } else if(!strcmp(*argv, "getvar")) {
            require(2);
#ifndef _WINDOWS
            fb_queue_display(argv[1], argv[1]);
#endif
            skip(2);
        } else if(!strcmp(*argv, "erase")) {
            require(2);
            fb_queue_erase(argv[1]);
            skip(2);
        } else if(!strcmp(*argv, "signature")) {
            require(2);
            data = load_file(argv[1], &sz);
#ifdef _WINDOWS
            if (data == 0) return -1;
#else
            if (data == 0) die("could not load '%s'", argv[1]);
#endif
#ifdef _WINDOWS
            if (sz != 256) return -1;
#else
            if (sz != 256) die("signature must be 256 bytes");
#endif
            fb_queue_download("signature", data, sz);
            fb_queue_command("signature", "installing signature");
            skip(2);
        } else if(!strcmp(*argv, "reboot")) {
            wants_reboot = 1;
            skip(1);
        } else if(!strcmp(*argv, "reboot-bootloader")) {
            wants_reboot_bootloader = 1;
            skip(1);
        } else if (!strcmp(*argv, "continue")) {
            fb_queue_command("continue", "resuming boot");
            skip(1);
        } else if(!strcmp(*argv, "boot")) {
            char *kname = 0;
            char *rname = 0;
            skip(1);
            if (argc > 0) {
                kname = argv[0];
                skip(1);
            }
            if (argc > 0) {
                rname = argv[0];
                skip(1);
            }
            data = load_bootable_image(page_size, kname, rname, &sz, cmdline);
            if (data == 0) return 1;
            fb_queue_download("boot.img", data, sz);
            fb_queue_command("boot", "booting");
        } else if(!strcmp(*argv, "flash")) {
            char *pname = argv[1];
            char *fname = 0;
            require(2);
            if (argc > 2) {
                fname = argv[2];
                skip(3);
            } else {
                fname = find_item(pname, product);
                skip(2);
            }
#ifdef _WINDOWS
            if (fname == 0) return -1;
#else
            if (fname == 0) die("cannot determine image filename for '%s'", pname);
#endif
            data = load_file(fname, &sz);
#ifdef _WINDOWS
            if (data == 0) return -1;
#else
            if (data == 0) die("cannot load '%s'\n", fname);
#endif
            fb_queue_flash(pname, data, sz);
        } else if(!strcmp(*argv, "flash:raw")) {
            char *pname = argv[1];
            char *kname = argv[2];
            char *rname = 0;
            require(3);
            if(argc > 3) {
                rname = argv[3];
                skip(4);
            } else {
                skip(3);
            }
            data = load_bootable_image(page_size, kname, rname, &sz, cmdline);
#ifdef _WINDOWS
            if (data == 0) return -1;
#else
            if (data == 0) die("cannot load bootable image");
#endif
            fb_queue_flash(pname, data, sz);
        } else if(!strcmp(*argv, "flashall")) {
            skip(1);
            do_flashall();
            wants_reboot = 1;
        } else if(!strcmp(*argv, "update")) {
            if (argc > 1) {
                do_update(argv[1]);
                skip(2);
            } else {
                do_update("update.zip");
                skip(1);
            }
            wants_reboot = 1;
        } else if(!strcmp(*argv, "oem")) {
            argc = do_oem_command(argc, argv);
        } else {
#ifndef _WINDOWS
            usage();
#endif
        }
    }

    if (wants_wipe) {
        fb_queue_erase("userdata");
        fb_queue_erase("cache");
    }
    if (wants_reboot) {
        fb_queue_reboot();
    } else if (wants_reboot_bootloader) {
        fb_queue_command("reboot-bootloader", "rebooting into bootloader");
    }

    usb = open_device();

    fb_execute_queue(usb);
#ifdef _WINDOWS
    fb_clean_queue();
    usb_close(usb);
#endif
    return 0;
}

void CFastboot::SetTotal(DWORD dwTotal)
{
    if(g_pThread)
    {
        ((DLoadThread *)g_pThread)->SetTotal(dwTotal);
    }
}

void CFastboot::SetCurr(DWORD dwCurr)
{
    if(g_pThread)
    {
        ((DLoadThread *)g_pThread)->SetCurr(dwCurr);
    }
}

BOOL CFastboot::IsRunning(void)
{
    if(g_pThread)
    {
        return ((DLoadThread *)g_pThread)->IsBusy();
    }
    return TRUE;
}

void CFastboot::SleepMS(UINT nMS)
{
    if(g_pThread)
    {
        return ((DLoadThread *)g_pThread)->WaitMS(nMS);
    }
    else
    {
        Sleep(nMS);
    }
}

int CFastboot::Exec(char *pExecStr, void *pThread)
{
    int   nRet;
    int   argc = 0,k = 0;
    char  argarr[CMD_MAX_NUM][CMD_MAX_LEN+1];
    char *argv[CMD_MAX_NUM];
    bool  combchar;
    
    usb = 0;
    serial = 0;
    product = 0;
    cmdline = 0;
    wipe_data = 0;
    vendor_id = 0;
    base_addr = 0x10000000;
    open_usb = 0;
    memcpy(&usb_class_id,&adb_usb_class_id,sizeof(GUID));

    if(pExecStr == NULL)
    {
        usb = usb_open(match_fastboot);
        if(usb)
        {
            usb_close(usb);
            return 0;
        }
        return -1;
    }
    
    combchar = false;
    for(int i=0;i<(int)strlen(pExecStr);i++)
    {
        if(pExecStr[i] == '\"')
        {
            combchar = !combchar;
            continue;
        }

        if(pExecStr[i] == ' ' && combchar == false)
        {
            if(k == 0)
            {
                continue;
            }
            else
            {
                argarr[argc][k] = 0;
                argv[argc] = &argarr[argc][0];
                argc++;
                k = 0;
            }
        }
        else
        {
            argarr[argc][k] = pExecStr[i];
            k++;
        }
    }
    argarr[argc][k] = 0;
    argv[argc] = &argarr[argc][0];
    if(k > 0)
    {
        argc++;
    }

    g_pThread = pThread;
    nRet = fastboot_main(argc,argv);
    g_pThread = NULL;
    return nRet;
}
