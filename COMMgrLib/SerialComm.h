/*=================================================================================================

FILE : SERIALPORT.H

SERVICES: Declaration of an MFC wrapper class for serial ports communicate

GENERAL DESCRIPTION:

PUBLIC CLASSES AND STATIC FUNCTIONS:
CSerialComm;

INITIALIZATION AND SEQUENCING REQUIREMENTS:

====================================================================================================*/


/*====================================================================================================

==MODIFY HISTOY FOR FILE==

when         who     what, where, why
----------   ---     ----------------------------------------------------------
12/12/2003   tyz      create

=====================================================================================================*/
#if !defined(_SERIALCOMM_H__)
#define _SERIALCOMM_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "QCOMConfig.h"


/*===========================================================================

DEFINITIONS AND CONSTANTS

===========================================================================*/
// RX QUEUE SIZE
#define RXQUEUESIZE	    10240	// Receive Queue Size
#define TXQUEUESIZE	    10240	// Transmit Queue Size
#define RXBUFSIZE	    (RXQUEUESIZE*10)	// Receive Queue Size

//Define default port parameter
#define DEFAULT_BAUDRATE    CBR_115200
#define DEFAULT_DATABITS    8
#define DEFAULT_PARITY      NOPARITY
#define DEFAULT_PORT        COM1
#define DEFAULT_RTSCTS      FALSE
#define DEFAULT_STOPBITS    ONESTOPBIT
#define DEFAULT_XONXOFF     FALSE
#define DEFAULT_XONCHAR     0x11
#define DEFAULT_XOFFCHAR    0x13
#define DEFAULT_DTRDSR      FALSE
#define DEFAULT_DTRCTRL     DTR_CONTROL_ENABLE
#define DEFAULT_RTSCTRL     RTS_CONTROL_ENABLE
#define DEFAULT_EVTCHAR     0x7E
#define DEFAULT_COMMASK     EV_RXFLAG | EV_DSR// | EV_RXCHAR          //串口异步处理中需响应的事件的掩码

#define DEFAULT_RDINTERVAL_TOUT  MAXDWORD
#define DEFAULT_RDTOTAL_TOUT     5000
#define DEFAULT_WTTOTAL_TOUT     5000

/*
//Enum for ERR information
typedef enum {
PORT_NO_ERROR, PORT_OPEN_ERROR, PORT_HANSHAKE_LINE_IN_USE
} COMM_ERROR;
*/

//Enum for port number
enum {
      COM1, COM2, COM3, COM4, COM5
    , COM6, COM7, COM8, COM9, COM10
    , COM11, COM12, COM13, COM14, COM15
    , COM16, COM17, COM18, COM19, COM20
    , COM21, COM22, COM23, COM24, COM25
    , COM26, COM27, COM28, COM29, COM30
    , COM31, COM32, COM33, COM34, COM35
    , COM36, COM37, COM38, COM39, COM40
    , COM41, COM42, COM43, COM44, COM45
    , COM46, COM47, COM48, COM49, COM50
    , COM51, COM52, COM53, COM54, COM55
    , COM56, COM57, COM58, COM59, COM60
    , COM61, COM62, COM63, COM64, COM65
    , COM66, COM67, COM68, COM69, COM70
    , COM71, COM72, COM73, COM74, COM75
    , COM76, COM77, COM78, COM79, COM80
    , COM81, COM82, COM83, COM84, COM85
    , COM86, COM87, COM88, COM89, COM90
    , COM91, COM92, COM93, COM94, COM95
    , COM96, COM97, COM98, COM99, COM100
    , COM101, COM102, COM103, COM104, COM105
    , COM106, COM107, COM108, COM109, COM110
    , COM111, COM112, COM113, COM114, COM115
    , COM116, COM117, COM118, COM119, COM120
    , COM121, COM122, COM123, COM124, COM125
    , COM126, COM127, COM128, COM129, COM130
    , COM131, COM132, COM133, COM134, COM135
    , COM136, COM137, COM138, COM139, COM140
    , COM141, COM142, COM143, COM144, COM145
    , COM146, COM147, COM148, COM149, COM150
    , COM151, COM152, COM153, COM154, COM155
    , COM156, COM157, COM158, COM159, COM160
    , COM161, COM162, COM163, COM164, COM165
    , COM166, COM167, COM168, COM169, COM170
    , COM171, COM172, COM173, COM174, COM175
    , COM176, COM177, COM178, COM179, COM180
    , COM181, COM182, COM183, COM184, COM185
    , COM186, COM187, COM188, COM189, COM190
    , COM191, COM192, COM193, COM194, COM195
    , COM196, COM197, COM198, COM199, COM200
    , COM201, COM202, COM203, COM204, COM205
    , COM206, COM207, COM208, COM209, COM210
    , COM211, COM212, COM213, COM214, COM215
    , COM216, COM217, COM218, COM219, COM220
    , COM221, COM222, COM223, COM224, COM225
    , COM226, COM227, COM228, COM229, COM230
    , COM231, COM232, COM233, COM234, COM235
    , COM236, COM237, COM238, COM239, COM240
    , COM241, COM242, COM243, COM244, COM245
    , COM246, COM247, COM248, COM249, COM250
    , COM251, COM252, COM253, COM254, COM255
    , COM256
    , COM_MAX
};

/*===========================================================================

LOCAL/STATIC DATA

===========================================================================*/



/*===========================================================================

TYPE DECLARATIONS

===========================================================================*/

typedef struct _PortPara   //Some member in this struct is subset(but not all) of DCB for user easly initialize port
{
    DWORD          dwBaudRate;
    BYTE	       bDataBits;      //4-8
    BYTE		   bStopBits;
    BYTE	       bParity;
    BOOL	       fRTSCTS;
    BOOL    	   fXONXOFF;
    BOOL	       fDTRDSR;
    BYTE           fDtrControl;
    BYTE           fRtsControl;
    BYTE           XonChar;
    BYTE           XoffChar;
    char           evtChar;
    DWORD          dwCommMask;
    int            port;
    COMMTIMEOUTS   CommTimeOuts;   //struct to specifies the communicate time-out
}  PortPara;

typedef  void (*PFNFRAMECB)(LPVOID pParam, DWORD dwFrameSize);
/*===========================================================================

CLASS DEFINITIONS

===========================================================================*/
class CSerialComm : virtual public CObject
{
private:
    OVERLAPPED      m_osRead, m_osWrite;
    BOOL            m_bPortOpen;                //Indicate if the serial port has already opened
    BOOL            m_bInterOpen;
    HANDLE          m_hSerialPort;              //HANDLE to a opened serial port
    HANDLE          m_hTXEvent;                 //A event handle we need for TX synchronisation
    HANDLE          m_hWaitEvent; 
    DCB			    m_dcb;                      //Data control block
    PortPara        m_Parameter;                //a struct contain port's parameter,such as baud rate
    int      	    m_nPortID;                  //The opened port's ID, Can't Modify. Set by Constructer from value of m_parameter.port
    LPVOID          m_pUser;                    //pointer to the Parent
    CWinThread*     m_pThread; 
    PFNFRAMECB      m_pfnFrameCB;
    BYTE            m_bRXBuf[RXBUFSIZE];
    DWORD           m_dwBufReadIdx;
    DWORD           m_dwBufWriteIdx;
    DWORD           m_dwBufScanIdx;
    DWORD           m_dwHubIndex;
    DWORD           m_dwPortIndex;

public:
    // Construction and Destruction
    CSerialComm();
    CSerialComm(const char* fileName);     //this Construction is for read para from a Ini file
    CSerialComm(PortPara portPara);        //this Construction user a PortPara struct Ini port
    void CommonInit();
    virtual ~CSerialComm();
    
    //property
    void            SetPortID(int nPortID)      { m_nPortID = nPortID; }
    int             GetPortID(void)             { return m_nPortID; }
    const TCHAR*    GetPortName(int portName);
    BOOL		    GetOpenFlag(void)           { return m_bPortOpen; }
    HANDLE		    GetPortHandle(void)         { return m_hSerialPort; }
    HANDLE		    GetTXEvent(void)            { return m_hTXEvent; }
    void            ResetMonitorThread(void)    { m_pThread = NULL; }
    DWORD           GetHubIndex(void)           { return m_dwHubIndex;};
    DWORD           GetPortIndex(void)          { return m_dwPortIndex;};
    
    //Implementation
    BOOL            Open(PFNFRAMECB pfnFrameCB, LPVOID pUser);
    BOOL            Close(void);
    BOOL            Read(char* pData, DWORD dwlength, DWORD *pdwReaded=NULL);
    BOOL            Write(char* pData, DWORD dwlength, DWORD *pdwWritten=NULL);   
    void            RXFrame(void);
    void            ReadToBuf(void);
    void            ReadFrame(BYTE* pData, int nSize=-1);
    BOOL            WaitMS(int nMS);
    BOOL            InterOpen(void);
    BOOL            InterClose(void);
    
    void            PortMonitor(void);
    //Static Implementation
    static UINT     PortMonitorThread(LPVOID pParam);
};

#endif // !defined(_SERIALCOMM_H__)
