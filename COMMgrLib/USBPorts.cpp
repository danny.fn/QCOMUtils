/*=================================================================================================

FILE : USBPorts.cpp

SERVICES: Implementation of an MFC wrapper class for USB Pors

GENERAL DESCRIPTION:

PUBLIC CLASSES AND STATIC FUNCTIONS:
CUSBPorts;

INITIALIZATION AND SEQUENCING REQUIREMENTS:

====================================================================================================*/


/*====================================================================================================

==MODIFY HISTOY FOR FILE==

when         who     what, where, why
----------   ---     ----------------------------------------------------------
22/08/2013   JYH      create

=====================================================================================================*/
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Stdafx.h"

#include "USBPorts.h"
#include "Log.h"
#include "Usbiodef.h"
#include <cfgmgr32.h>

#define ALLOC(dwBytes) GlobalAlloc(GPTR,(dwBytes))

#define REALLOC(hMem, dwBytes) GlobalReAlloc((hMem), (dwBytes), (GMEM_MOVEABLE|GMEM_ZEROINIT))

#define FREE(hMem)  GlobalFree((hMem))

static const GUID hostcontroller_usb_class_id = {0x3abf6f2d, 0x71c4, 0x462a, {0x8a, 0x92, 0x1e, 0x68, 0x61, 0xe6, 0xaf, 0x27}};
static const GUID usbdevice_usb_class_id      = {0xA5DCBF10, 0x6530, 0x11D2, {0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED}};
static const GUID qcser_usb_class_id = GUID_CLASS_COMPORT;//GUID_SERENUM_BUS_ENUMERATOR;

/*==========================================================================

MEMBER FUNCTION DEFINATION

==========================================================================*/

CUSBPorts::CUSBPorts(void)
{
    m_bInited = false;
    m_pEnumDevInfo = INVALID_HANDLE_VALUE;
    m_CompareLevel = 2;
}

CUSBPorts::~CUSBPorts(void)
{
}

bool CUSBPorts::FindUsbDeviceByPort(const int nPortID, ULONG nCmpLevel)
{
    CString reqPortName;
    
    reqPortName.Format(_T("COM%d"), nPortID);
    m_bInited       = false;
    m_CompareLevel  = nCmpLevel;

    // Open a handle to the plug and play dev node.
    // SetupDiGetClassDevs() returns a device information set that
    // contains info on all installed devices of a specified class.
    HDEVINFO hardware_dev_info = SetupDiGetClassDevs(&qcser_usb_class_id, NULL, NULL, DIGCF_DEVICEINTERFACE|DIGCF_PRESENT);

    if (INVALID_HANDLE_VALUE != hardware_dev_info) 
    {
        SP_DEVINFO_DATA             DeviceInfoData;

        // Enumerate interfaces on this device
        for (ULONG index = 0; ; index++) 
        {
            DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

            if(SetupDiEnumDeviceInfo(hardware_dev_info, index, &DeviceInfoData))
            {
                CString szPortName;
                if(GetUsbDevicePortName(hardware_dev_info,&DeviceInfoData,&szPortName))
                {
                    if(szPortName != reqPortName)
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }

                if(GetUsbDriverName(hardware_dev_info,&DeviceInfoData, &m_UsbDriverName))
                {
                    if(GetParentDriverName(&m_UsbDriverName, &m_UsbParentDriverName))
                    {
                        m_bInited = true;
                        break;
                    }
                }
            }
            else
            {
                // Enum compelete
                break;
            }
        }

        SetupDiDestroyDeviceInfoList(hardware_dev_info);
    }

    if(m_bInited)
    {
        SyncUsbDevicePositon();
    }
    return m_bInited;
}

bool CUSBPorts::FindUsbDeviceByDevPath(TCHAR *pDevPath, const GUID *pUSBClass, ULONG nCmpLevel)
{
    m_bInited       = false;
    m_CompareLevel  = nCmpLevel;
    
    // Open a handle to the plug and play dev node.
    // SetupDiGetClassDevs() returns a device information set that
    // contains info on all installed devices of a specified class.
    HDEVINFO hardware_dev_info = SetupDiGetClassDevs(pUSBClass, NULL, NULL, DIGCF_DEVICEINTERFACE|DIGCF_PRESENT);

    if (INVALID_HANDLE_VALUE != hardware_dev_info) 
    {
        SP_DEVINFO_DATA             DeviceInfoData;
        SP_DEVICE_INTERFACE_DATA    interface_data;
        // Enumerate interfaces on this device
        for (ULONG index = 0; ; index++) 
        {
            DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
            interface_data.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
            if(SetupDiEnumDeviceInfo(hardware_dev_info, index, &DeviceInfoData))
            {
                // SetupDiEnumDeviceInterfaces() returns information about device
                // interfaces exposed by one or more devices defined by our interface
                // class. Each call returns information about one interface. The routine
                // can be called repeatedly to get information about several interfaces
                // exposed by one or more devices.
                if (SetupDiEnumDeviceInterfaces(hardware_dev_info,
                                                &DeviceInfoData, 
                                                pUSBClass,
                                                0,
                                                &interface_data)) 
                {
                    // Satisfy "exclude removed" and "active only" filters.
                    //if ((0 == (interface_data.Flags & SPINT_REMOVED)) && (interface_data.Flags & SPINT_ACTIVE)) 
                    {
                        if (GetUsbDeviceName(hardware_dev_info, &interface_data, &m_UsbName)) 
                        {
                            if(_tcscmp(m_UsbName, pDevPath) != 0)
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                        
                        if(GetUsbDriverName(hardware_dev_info,&DeviceInfoData, &m_UsbDriverName))
                        {
                            if(GetParentDriverName(&m_UsbDriverName, &m_UsbParentDriverName))
                            {
                                m_bInited = true;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                // Enum compelete
                break;
            }
        }

        SetupDiDestroyDeviceInfoList(hardware_dev_info);
    }

    if(m_bInited)
    {
        SyncUsbDevicePositon();
    }
    return m_bInited;
}

bool CUSBPorts::EnumUsbDeviceByIndexInit(const GUID *pUSBClass, ULONG nHubIndex, ULONG nPortIndex, ULONG nCmpLevel)
{
    m_CompareLevel  = nCmpLevel;
    m_EnumIndex     = 0;
    m_EnumHubIndex  = nHubIndex;
    m_EnumPortIndex = nPortIndex;
    m_pEnumDevInfo  = SetupDiGetClassDevs(pUSBClass, NULL, NULL, DIGCF_DEVICEINTERFACE|DIGCF_PRESENT);
    if (INVALID_HANDLE_VALUE == m_pEnumDevInfo) 
    {
        return false;
    }
    return true;
}

bool CUSBPorts::EnumUsbDeviceByIndexNext(const GUID *pUSBClass)
{
    m_bInited       = false;
    
    SP_DEVINFO_DATA             DeviceInfoData;
    SP_DEVICE_INTERFACE_DATA    interface_data;
    // Enumerate interfaces on this device
    for (;;) 
    {
        DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
        interface_data.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
        if(SetupDiEnumDeviceInfo(m_pEnumDevInfo, m_EnumIndex, &DeviceInfoData))
        {
            // SetupDiEnumDeviceInterfaces() returns information about device
            // interfaces exposed by one or more devices defined by our interface
            // class. Each call returns information about one interface. The routine
            // can be called repeatedly to get information about several interfaces
            // exposed by one or more devices.
            if (SetupDiEnumDeviceInterfaces(m_pEnumDevInfo,
                                            &DeviceInfoData, 
                                            pUSBClass,
                                            0,
                                            &interface_data)) 
            {
                // Satisfy "exclude removed" and "active only" filters.
                //if ((0 == (interface_data.Flags & SPINT_REMOVED)) && (interface_data.Flags & SPINT_ACTIVE)) 
                {
                    if(GetUsbDriverName(m_pEnumDevInfo,&DeviceInfoData, &m_UsbDriverName))
                    {
                        m_UsbParentDriverName.Empty();
                        GetParentDriverName(&m_UsbDriverName, &m_UsbParentDriverName);
                        m_UsbRootDriverName.Empty();
                        GetParentDriverName(&m_UsbParentDriverName, &m_UsbRootDriverName);
                        
                        SyncUsbDevicePositon();
                        if(m_EnumHubIndex == m_HubIndex && m_EnumPortIndex == m_PortIndex)
                        {
                            GetUsbDeviceName(m_pEnumDevInfo, &interface_data, &m_UsbName);
                            m_bInited = true;
                            m_EnumIndex++;
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            // Enum compelete
            break;
        }
        m_EnumIndex++;
    }

    return m_bInited;
}

void CUSBPorts::EnumUsbDeviceByIndexEnd(void)
{
    if (INVALID_HANDLE_VALUE != m_pEnumDevInfo)
    {
        SetupDiDestroyDeviceInfoList(m_pEnumDevInfo);
    }
}

bool CUSBPorts::GetUsbDeviceDetails(HDEVINFO hardware_dev_info,
                                    PSP_DEVICE_INTERFACE_DATA dev_info_data,
                                    PSP_DEVICE_INTERFACE_DETAIL_DATA* dev_info_detail_data) 
{
    ULONG required_len = 0;

    // First query for the structure size. At this point we expect this call
    // to fail with ERROR_INSUFFICIENT_BUFFER error code.
    if (SetupDiGetDeviceInterfaceDetail(hardware_dev_info,
                                        dev_info_data,
                                        NULL,
                                        0,
                                        &required_len,
                                        NULL)) 
    {
        return false;
    }

    if (ERROR_INSUFFICIENT_BUFFER != GetLastError())
    {
        return false;
    }

    // Allocate buffer for the structure
    PSP_DEVICE_INTERFACE_DETAIL_DATA buffer = reinterpret_cast<PSP_DEVICE_INTERFACE_DETAIL_DATA>(malloc(required_len));

    if (NULL == buffer) 
    {
        SetLastError(ERROR_OUTOFMEMORY);
        return false;
    }

    buffer->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

    // Retrieve the information from Plug and Play.
    if (SetupDiGetDeviceInterfaceDetail(hardware_dev_info,
                                        dev_info_data,
                                        buffer,
                                        required_len,
                                        &required_len,
                                        NULL)) 
    {
        *dev_info_detail_data = buffer;
        return true;
    } 
    else 
    {
        // Free the buffer if this call failed
        free(buffer);

        return false;
    }
}

bool CUSBPorts::GetUsbDeviceName(HDEVINFO hardware_dev_info,
                                 PSP_DEVICE_INTERFACE_DATA dev_info_data,
                                 CString* name) 
{
    PSP_DEVICE_INTERFACE_DETAIL_DATA func_class_dev_data = NULL;
    if (!GetUsbDeviceDetails(hardware_dev_info,
                             dev_info_data,
                             &func_class_dev_data)) 
    {
        return false;
    }

    try 
    {
        *name = func_class_dev_data->DevicePath;
    } 
    catch (...) 
    {
        SetLastError(ERROR_OUTOFMEMORY);
    }

    free(func_class_dev_data);

    return !name->IsEmpty();
}

bool CUSBPorts::GetUsbDriverName(HDEVINFO hardware_dev_info,
                                 PSP_DEVINFO_DATA dev_info_data,
                                 CString* name) 
{
    TCHAR szBuf[MAX_USBNAME_LEN] = {0};
    if (!SetupDiGetDeviceRegistryProperty(hardware_dev_info, dev_info_data, SPDRP_DRIVER, NULL, (PBYTE)szBuf, MAX_USBNAME_LEN, NULL))
    {
        return false;
    }

    try 
    {
        *name = szBuf;
    } 
    catch (...) 
    {
        SetLastError(ERROR_OUTOFMEMORY);
    }
    return !name->IsEmpty();
}

bool CUSBPorts::GetUsbDevicePortName(HDEVINFO hardware_dev_info, PSP_DEVINFO_DATA dev_info_data, CString* pszPortName)
{
    bool bRet = false;
    HKEY hKey = SetupDiOpenDevRegKey(hardware_dev_info, 
                                     dev_info_data, 
                                     DICS_FLAG_GLOBAL, 
                                     0, 
                                     DIREG_DEV, 
                                     KEY_READ);

    if (hKey == INVALID_HANDLE_VALUE)
    {
        return false;
    }

    TCHAR chPortName[MAX_PATH] = {0};
    DWORD lType = 0;
    DWORD cbData = MAX_PATH;

    if (RegQueryValueEx(hKey, _T("PortName"), NULL, &lType, (PBYTE)chPortName, &cbData) == ERROR_SUCCESS)
    {
         *pszPortName = chPortName ;
         bRet = true;
    }

    RegCloseKey(hKey);
    return bRet;
}

PTSTR CUSBPorts::WideStrToMultiStr(LPCWSTR WideStr)
{
    // Is there a better way to do this?
#if defined(_UNICODE) //  If this is built for UNICODE, just clone the input
    ULONG nChars;
    PTSTR RetStr;

    nChars = wcslen(WideStr) + 1;
    RetStr = (PTSTR)ALLOC(nChars * sizeof(TCHAR));
    if (RetStr == NULL)
    {
        return NULL;
    }
    _tcscpy_s(RetStr, nChars, WideStr);
    return RetStr;
#else //  convert
    ULONG nBytes;
    PTSTR MultiStr;
    
    // Get the length of the converted string
    //
    nBytes = WideCharToMultiByte(
                 CP_ACP,
                 0,
                 WideStr,
                 -1,
                 NULL,
                 0,
                 NULL,
                 NULL);

    if (nBytes == 0)
    {
        return NULL;
    }

    // Allocate space to hold the converted string
    //
    MultiStr = ALLOC(nBytes);

    if (MultiStr == NULL)
    {
        return NULL;
    }

    // Convert the string
    //
    nBytes = WideCharToMultiByte(
                 CP_ACP,
                 0,
                 WideStr,
                 -1,
                 MultiStr,
                 nBytes,
                 NULL,
                 NULL);

    if (nBytes == 0)
    {
        FREE(MultiStr);
        return NULL;
    }
    return MultiStr;
#endif
}

bool CUSBPorts::SyncUsbDevicePositon(void)
{
    bool        bRet = false;
    TCHAR       HCName[16];
    int         HCNum;
    HANDLE      hHCDev;

    HDEVINFO                         deviceInfo;
    SP_DEVICE_INTERFACE_DATA         deviceInfoData;
    PSP_DEVICE_INTERFACE_DETAIL_DATA deviceDetailData;
    ULONG                            index;
    ULONG                            requiredLength;
    
    m_HubIndex  = 0;
    m_PortIndex = 0;
    
    // Iterate over some Host Controller names and try to open them.
    //
    for (HCNum = 0; HCNum < NUM_HCS_TO_CHECK; HCNum++)
    {
        _stprintf_s(HCName, sizeof(HCName)/sizeof(HCName[0]), _T("\\\\.\\HCD%d"), HCNum);

        hHCDev = CreateFile(HCName,
                            GENERIC_WRITE,
                            FILE_SHARE_WRITE,
                            NULL,
                            OPEN_EXISTING,
                            0,
                            NULL);

        // If the handle is valid, then we've successfully opened a Host
        // Controller.  Display some info about the Host Controller itself,
        // then enumerate the Root Hub attached to the Host Controller.
        //
        if (hHCDev != INVALID_HANDLE_VALUE)
        {

            bRet = EnumerateHostController(hHCDev);

            CloseHandle(hHCDev);
            if(bRet)
            {
                return true;
            }
        }
    }

    // Now iterate over host controllers using the new GUID based interface
    //
    deviceInfo = SetupDiGetClassDevs((LPGUID)&hostcontroller_usb_class_id,
                                     NULL,
                                     NULL,
                                     (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

    if (deviceInfo != INVALID_HANDLE_VALUE)
    {
        deviceInfoData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

        for (index=0;
             SetupDiEnumDeviceInterfaces(deviceInfo,
                                         0,
                                         (LPGUID)&hostcontroller_usb_class_id,
                                         index,
                                         &deviceInfoData);
             index++)
        {
            SetupDiGetDeviceInterfaceDetail(deviceInfo,
                                            &deviceInfoData,
                                            NULL,
                                            0,
                                            &requiredLength,
                                            NULL);

            deviceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)GlobalAlloc(GPTR, requiredLength);

            deviceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

            SetupDiGetDeviceInterfaceDetail(deviceInfo,
                                            &deviceInfoData,
                                            deviceDetailData,
                                            requiredLength,
                                            &requiredLength,
                                            NULL);

            hHCDev = CreateFile(deviceDetailData->DevicePath,
                                GENERIC_WRITE,
                                FILE_SHARE_WRITE,
                                NULL,
                                OPEN_EXISTING,
                                0,
                                NULL);

            // If the handle is valid, then we've successfully opened a Host
            // Controller.  Display some info about the Host Controller itself,
            // then enumerate the Root Hub attached to the Host Controller.
            //
            if (hHCDev != INVALID_HANDLE_VALUE)
            {

                bRet = EnumerateHostController(hHCDev);

                CloseHandle(hHCDev);
            }

            GlobalFree(deviceDetailData);
            if(bRet)
            {
                break;
            }
        }

        SetupDiDestroyDeviceInfoList(deviceInfo);
    }

    if(m_PortIndex == 0)
    {
        m_HubIndex = 0;
    }
    return bRet;
}

bool CUSBPorts::EnumerateHostController(HANDLE hHCDev)
{
    bool        bRet = false;
    PTSTR       rootHubName;

    // Get the name of the root hub for this host
    // controller and then enumerate the root hub.
    //
    rootHubName = GetRootHubName(hHCDev);

    if (rootHubName != NULL)
    {
        if (EnumerateHub(rootHubName))
        {
            bRet = true;
        }
        FREE(rootHubName);
    }
    return bRet;
}

BOOL CUSBPorts::EnumerateHub (PTSTR HubName)
{
    BOOL                    bRet = FALSE;
    PUSB_NODE_INFORMATION   hubInfo;
    HANDLE                  hHubDevice;
    PTSTR                   deviceName;
    size_t                  deviceNameSize;
    BOOL                    success;
    ULONG                   nBytes;

    // Initialize locals to not allocated state so the error cleanup routine
    // only tries to cleanup things that were successfully allocated.
    //
    hubInfo     = NULL;
    hHubDevice  = INVALID_HANDLE_VALUE;

    // Allocate some space for a USB_NODE_INFORMATION structure for this Hub,
    //
    hubInfo = (PUSB_NODE_INFORMATION)ALLOC(sizeof(USB_NODE_INFORMATION));

    if (hubInfo == NULL)
    {
        goto EnumerateHubError;
    }

    // Allocate a temp buffer for the full hub device name.
    //
    deviceNameSize = _tcslen(HubName) + _tcslen(_T("\\\\.\\")) + 1;
    deviceName = (PTSTR)ALLOC(deviceNameSize * sizeof(TCHAR));

    if (deviceName == NULL)
    {
        goto EnumerateHubError;
    }

    // Create the full hub device name
    //
    _tcscpy_s(deviceName, deviceNameSize, _T("\\\\.\\"));
    _tcscat_s(deviceName, deviceNameSize, HubName);

    // Try to hub the open device
    //
    hHubDevice = CreateFile(deviceName,
                            GENERIC_WRITE,
                            FILE_SHARE_WRITE,
                            NULL,
                            OPEN_EXISTING,
                            0,
                            NULL);

    // Done with temp buffer for full hub device name
    //
    FREE(deviceName);

    if (hHubDevice == INVALID_HANDLE_VALUE)
    {
        goto EnumerateHubError;
    }
    
    m_HubIndex++;
    m_PortIndex = 0;

    //
    // Now query USBHUB for the USB_NODE_INFORMATION structure for this hub.
    // This will tell us the number of downstream ports to enumerate, among
    // other things.
    //
    success = DeviceIoControl(hHubDevice,
                              IOCTL_USB_GET_NODE_INFORMATION,
                              hubInfo,
                              sizeof(USB_NODE_INFORMATION),
                              hubInfo,
                              sizeof(USB_NODE_INFORMATION),
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto EnumerateHubError;
    }

    // Now recursively enumrate the ports of this hub.
    //
    bRet = EnumerateHubPorts(hHubDevice, hubInfo->u.HubInformation.HubDescriptor.bNumberOfPorts);

EnumerateHubError:
    //
    // Clean up any stuff that got allocated
    //
    if (hHubDevice != INVALID_HANDLE_VALUE)
    {
        CloseHandle(hHubDevice);
        hHubDevice = INVALID_HANDLE_VALUE;
    }
    
    if (hubInfo)
    {
        FREE(hubInfo);
    }

    return bRet;
}


BOOL CUSBPorts::EnumerateHubPorts (HANDLE hHubDevice, ULONG NumPorts)
{
    BOOL bRet = FALSE;
    ULONG       index;
    BOOL        success;

    PUSB_NODE_CONNECTION_INFORMATION_EX connectionInfoEx;
    
    PTSTR driverKeyName = NULL;

    // Port indices are 1 based, not 0 based.
    //
    for (index=1; index <= NumPorts; index++)
    {
        ULONG nBytesEx;

        // Allocate space to hold the connection info for this port.
        // For now, allocate it big enough to hold info for 30 pipes.
        //
        // Endpoint numbers are 0-15.  Endpoint number 0 is the standard
        // control endpoint which is not explicitly listed in the Configuration
        // Descriptor.  There can be an IN endpoint and an OUT endpoint at
        // endpoint numbers 1-15 so there can be a maximum of 30 endpoints
        // per device configuration.
        //
        // Should probably size this dynamically at some point.
        //
        nBytesEx = sizeof(USB_NODE_CONNECTION_INFORMATION_EX) +
                   sizeof(USB_PIPE_INFO) * 30;

        connectionInfoEx = (PUSB_NODE_CONNECTION_INFORMATION_EX)ALLOC(nBytesEx);

        if (connectionInfoEx == NULL)
        {
            break;
        }

        //
        // Now query USBHUB for the USB_NODE_CONNECTION_INFORMATION_EX structure
        // for this port.  This will tell us if a device is attached to this
        // port, among other things.
        //
        connectionInfoEx->ConnectionIndex = index;

        success = DeviceIoControl(hHubDevice,
                                  IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX,
                                  connectionInfoEx,
                                  nBytesEx,
                                  connectionInfoEx,
                                  nBytesEx,
                                  &nBytesEx,
                                  NULL);

        if (!success)
        {
            PUSB_NODE_CONNECTION_INFORMATION    connectionInfo;
            ULONG                               nBytes;

            // Try using IOCTL_USB_GET_NODE_CONNECTION_INFORMATION
            // instead of IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX
            //
            nBytes = sizeof(USB_NODE_CONNECTION_INFORMATION) +
                     sizeof(USB_PIPE_INFO) * 30;

            connectionInfo = (PUSB_NODE_CONNECTION_INFORMATION)ALLOC(nBytes);

            connectionInfo->ConnectionIndex = index;

            success = DeviceIoControl(hHubDevice,
                                      IOCTL_USB_GET_NODE_CONNECTION_INFORMATION,
                                      connectionInfo,
                                      nBytes,
                                      connectionInfo,
                                      nBytes,
                                      &nBytes,
                                      NULL);

            if (!success)
            {
                FREE(connectionInfo);
                FREE(connectionInfoEx);
                continue;
            }

            // Copy IOCTL_USB_GET_NODE_CONNECTION_INFORMATION into
            // IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX structure.
            //
            connectionInfoEx->ConnectionIndex =
                connectionInfo->ConnectionIndex;

            connectionInfoEx->DeviceDescriptor =
                connectionInfo->DeviceDescriptor;

            connectionInfoEx->CurrentConfigurationValue =
                connectionInfo->CurrentConfigurationValue;

            connectionInfoEx->Speed =
                connectionInfo->LowSpeed ? UsbLowSpeed : UsbFullSpeed;

            connectionInfoEx->DeviceIsHub =
                connectionInfo->DeviceIsHub;

            connectionInfoEx->DeviceAddress =
                connectionInfo->DeviceAddress;

            connectionInfoEx->NumberOfOpenPipes =
                connectionInfo->NumberOfOpenPipes;

            connectionInfoEx->ConnectionStatus =
                connectionInfo->ConnectionStatus;

            memcpy(&connectionInfoEx->PipeList[0],
                   &connectionInfo->PipeList[0],
                   sizeof(USB_PIPE_INFO) * 30);

            FREE(connectionInfo);
        }

        // If the device connected to the port is an external hub, get the
        // name of the external hub and recursively enumerate it.
        //
        if (connectionInfoEx->DeviceIsHub)
        {
            PTSTR extHubName;

            extHubName = GetExternalHubName(hHubDevice,index);

            if (extHubName != NULL)
            {
                if (EnumerateHub(extHubName)) 
                {
                    bRet = TRUE;
                    FREE(extHubName);
                    break;
                    
                }
                FREE(extHubName);
            }
        }
        else
        {
            // If there is a device connected, get the Device Description
            //
            if (connectionInfoEx->ConnectionStatus != NoDeviceConnected)
            {
                driverKeyName = GetDriverKeyName(hHubDevice, index);

                if (driverKeyName)
                {
                    if((m_CompareLevel>=3&&_tcscmp(driverKeyName,m_UsbRootDriverName.GetBuffer())==0)
                        ||(m_CompareLevel>=2&&_tcscmp(driverKeyName,m_UsbParentDriverName.GetBuffer())==0)
                        ||(m_CompareLevel>=1&&_tcscmp(driverKeyName,m_UsbDriverName.GetBuffer())==0))
                    {
                        m_PortIndex = index;
                        bRet = TRUE;
                    }
                    FREE(driverKeyName);
                    if(bRet)
                    {
                        break;
                    }
                }
            }
        }

        if(connectionInfoEx)
        {
            FREE(connectionInfoEx);
            connectionInfoEx = NULL;
        }
    }

    if(connectionInfoEx)
    {
        FREE(connectionInfoEx);
        connectionInfoEx = NULL;
    }
    return bRet;
}

PTSTR CUSBPorts::GetRootHubName(HANDLE HostController)
{
    BOOL                success;
    ULONG               nBytes;
    USB_ROOT_HUB_NAME   rootHubName;
    PUSB_ROOT_HUB_NAME  rootHubNameW;
    PTSTR               rootHubNameA;

    rootHubNameW = NULL;
    rootHubNameA = NULL;

    // Get the length of the name of the Root Hub attached to the
    // Host Controller
    //
    success = DeviceIoControl(HostController,
                              IOCTL_USB_GET_ROOT_HUB_NAME,
                              0,
                              0,
                              &rootHubName,
                              sizeof(rootHubName),
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto GetRootHubNameError;
    }

    // Allocate space to hold the Root Hub name
    //
    nBytes = rootHubName.ActualLength;

    rootHubNameW = (PUSB_ROOT_HUB_NAME)ALLOC(nBytes);

    if (rootHubNameW == NULL)
    {
        goto GetRootHubNameError;
    }

    // Get the name of the Root Hub attached to the Host Controller
    //
    success = DeviceIoControl(HostController,
                              IOCTL_USB_GET_ROOT_HUB_NAME,
                              NULL,
                              0,
                              rootHubNameW,
                              nBytes,
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto GetRootHubNameError;
    }

    // Convert the Root Hub name
    //
    rootHubNameA = WideStrToMultiStr(rootHubNameW->RootHubName);

    // All done, free the uncoverted Root Hub name and return the
    // converted Root Hub name
    //
    FREE(rootHubNameW);

    return rootHubNameA;


GetRootHubNameError:
    // There was an error, free anything that was allocated
    //
    if (rootHubNameW != NULL)
    {
        FREE(rootHubNameW);
        rootHubNameW = NULL;
    }

    return NULL;
}

PTSTR CUSBPorts::GetHCDDriverKeyName (HANDLE HCD)
{
    BOOL                    success;
    ULONG                   nBytes;
    USB_HCD_DRIVERKEY_NAME  driverKeyName;
    PUSB_HCD_DRIVERKEY_NAME driverKeyNameW;
    PTSTR                   driverKeyNameA;
    
    driverKeyNameW = NULL;
    driverKeyNameA = NULL;

    // Get the length of the name of the driver key of the HCD
    //
    success = DeviceIoControl(HCD,
                              IOCTL_GET_HCD_DRIVERKEY_NAME,
                              &driverKeyName,
                              sizeof(driverKeyName),
                              &driverKeyName,
                              sizeof(driverKeyName),
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto GetHCDDriverKeyNameError;
    }

    // Allocate space to hold the driver key name
    //
    nBytes = driverKeyName.ActualLength;

    if (nBytes <= sizeof(driverKeyName))
    {
        goto GetHCDDriverKeyNameError;
    }

    driverKeyNameW = (PUSB_HCD_DRIVERKEY_NAME)ALLOC(nBytes);

    if (driverKeyNameW == NULL)
    {
        goto GetHCDDriverKeyNameError;
    }

    // Get the name of the driver key of the device attached to
    // the specified port.
    //
    success = DeviceIoControl(HCD,
                              IOCTL_GET_HCD_DRIVERKEY_NAME,
                              driverKeyNameW,
                              nBytes,
                              driverKeyNameW,
                              nBytes,
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto GetHCDDriverKeyNameError;
    }

    // Convert the driver key name
    //
    driverKeyNameA = WideStrToMultiStr(driverKeyNameW->DriverKeyName);

    // All done, free the uncoverted driver key name and return the
    // converted driver key name
    //
    FREE(driverKeyNameW);

    return driverKeyNameA;


GetHCDDriverKeyNameError:
    // There was an error, free anything that was allocated
    //
    if (driverKeyNameW != NULL)
    {
        FREE(driverKeyNameW);
        driverKeyNameW = NULL;
    }

    return NULL;
}


PUSB_DESCRIPTOR_REQUEST CUSBPorts::GetConfigDescriptor(HANDLE hHubDevice, ULONG ConnectionIndex, UCHAR DescriptorIndex)
{
    BOOL    success;
    ULONG   nBytes;
    ULONG   nBytesReturned;
    
    UCHAR   configDescReqBuf[sizeof(USB_DESCRIPTOR_REQUEST) +
                             sizeof(USB_CONFIGURATION_DESCRIPTOR)];

    PUSB_DESCRIPTOR_REQUEST         configDescReq;
    PUSB_CONFIGURATION_DESCRIPTOR   configDesc;


    // Request the Configuration Descriptor the first time using our
    // local buffer, which is just big enough for the Cofiguration
    // Descriptor itself.
    //
    nBytes = sizeof(configDescReqBuf);

    configDescReq = (PUSB_DESCRIPTOR_REQUEST)configDescReqBuf;
    configDesc = (PUSB_CONFIGURATION_DESCRIPTOR)(configDescReq+1);

    // Zero fill the entire request structure
    //
    memset(configDescReq, 0, nBytes);

    // Indicate the port from which the descriptor will be requested
    //
    configDescReq->ConnectionIndex = ConnectionIndex;

    //
    // USBHUB uses URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE to process this
    // IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION request.
    //
    // USBD will automatically initialize these fields:
    //     bmRequest = 0x80
    //     bRequest  = 0x06
    //
    // We must inititialize these fields:
    //     wValue    = Descriptor Type (high) and Descriptor Index (low byte)
    //     wIndex    = Zero (or Language ID for String Descriptors)
    //     wLength   = Length of descriptor buffer
    //
    configDescReq->SetupPacket.wValue = (USB_CONFIGURATION_DESCRIPTOR_TYPE << 8)
                                        | DescriptorIndex;

    configDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

    // Now issue the get descriptor request.
    //
    success = DeviceIoControl(hHubDevice,
                              IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION,
                              configDescReq,
                              nBytes,
                              configDescReq,
                              nBytes,
                              &nBytesReturned,
                              NULL);

    if (!success)
    {
        return NULL;
    }

    if (nBytes != nBytesReturned)
    {
        return NULL;
    }

    if (configDesc->wTotalLength < sizeof(USB_CONFIGURATION_DESCRIPTOR))
    {
        return NULL;
    }

    // Now request the entire Configuration Descriptor using a dynamically
    // allocated buffer which is sized big enough to hold the entire descriptor
    //
    nBytes = sizeof(USB_DESCRIPTOR_REQUEST) + configDesc->wTotalLength;

    configDescReq = (PUSB_DESCRIPTOR_REQUEST)ALLOC(nBytes);

    if (configDescReq == NULL)
    {
        return NULL;
    }

    configDesc = (PUSB_CONFIGURATION_DESCRIPTOR)(configDescReq+1);

    // Indicate the port from which the descriptor will be requested
    //
    configDescReq->ConnectionIndex = ConnectionIndex;

    //
    // USBHUB uses URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE to process this
    // IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION request.
    //
    // USBD will automatically initialize these fields:
    //     bmRequest = 0x80
    //     bRequest  = 0x06
    //
    // We must inititialize these fields:
    //     wValue    = Descriptor Type (high) and Descriptor Index (low byte)
    //     wIndex    = Zero (or Language ID for String Descriptors)
    //     wLength   = Length of descriptor buffer
    //
    configDescReq->SetupPacket.wValue = (USB_CONFIGURATION_DESCRIPTOR_TYPE << 8)
                                        | DescriptorIndex;

    configDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

    // Now issue the get descriptor request.
    //
    success = DeviceIoControl(hHubDevice,
                              IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION,
                              configDescReq,
                              nBytes,
                              configDescReq,
                              nBytes,
                              &nBytesReturned,
                              NULL);

    if (!success)
    {
        FREE(configDescReq);
        return NULL;
    }

    if (nBytes != nBytesReturned)
    {
        FREE(configDescReq);
        return NULL;
    }

    if (configDesc->wTotalLength != (nBytes - sizeof(USB_DESCRIPTOR_REQUEST)))
    {
        FREE(configDescReq);
        return NULL;
    }

    return configDescReq;
}


BOOL CUSBPorts::AreThereStringDescriptors (PUSB_DEVICE_DESCRIPTOR  DeviceDesc, PUSB_CONFIGURATION_DESCRIPTOR ConfigDesc)
{
    PUCHAR                  descEnd;
    PUSB_COMMON_DESCRIPTOR  commonDesc;

    //
    // Check Device Descriptor strings
    //

    if (DeviceDesc->iManufacturer ||
        DeviceDesc->iProduct      ||
        DeviceDesc->iSerialNumber
       )
    {
        return TRUE;
    }


    //
    // Check the Configuration and Interface Descriptor strings
    //

    descEnd = (PUCHAR)ConfigDesc + ConfigDesc->wTotalLength;

    commonDesc = (PUSB_COMMON_DESCRIPTOR)ConfigDesc;

    while ((PUCHAR)commonDesc + sizeof(USB_COMMON_DESCRIPTOR) < descEnd &&
           (PUCHAR)commonDesc + commonDesc->bLength <= descEnd)
    {
        switch (commonDesc->bDescriptorType)
        {
            case USB_CONFIGURATION_DESCRIPTOR_TYPE:
                if (commonDesc->bLength != sizeof(USB_CONFIGURATION_DESCRIPTOR))
                {
                    break;
                }
                if (((PUSB_CONFIGURATION_DESCRIPTOR)commonDesc)->iConfiguration)
                {
                    return TRUE;
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)(((PUCHAR)commonDesc)+commonDesc->bLength);
                continue;

            case USB_INTERFACE_DESCRIPTOR_TYPE:
                if (commonDesc->bLength != sizeof(USB_INTERFACE_DESCRIPTOR) &&
                    commonDesc->bLength != sizeof(USB_INTERFACE_DESCRIPTOR2))
                {
                    break;
                }
                if (((PUSB_INTERFACE_DESCRIPTOR)commonDesc)->iInterface)
                {
                    return TRUE;
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)(((PUCHAR)commonDesc)+commonDesc->bLength);
                continue;

            default:
                commonDesc = (PUSB_COMMON_DESCRIPTOR)(((PUCHAR)commonDesc)+commonDesc->bLength);
                continue;
        }
        break;
    }

    return FALSE;
}

PSTRING_DESCRIPTOR_NODE CUSBPorts::GetAllStringDescriptors (HANDLE                          hHubDevice,
                                                            ULONG                           ConnectionIndex,
                                                            PUSB_DEVICE_DESCRIPTOR          DeviceDesc,
                                                            PUSB_CONFIGURATION_DESCRIPTOR   ConfigDesc)
{
    PSTRING_DESCRIPTOR_NODE supportedLanguagesString;
    PSTRING_DESCRIPTOR_NODE stringDescNodeTail;
    ULONG                   numLanguageIDs;
    USHORT                  *languageIDs;

    PUCHAR                  descEnd;
    PUSB_COMMON_DESCRIPTOR  commonDesc;

    //
    // Get the array of supported Language IDs, which is returned
    // in String Descriptor 0
    //
    supportedLanguagesString = GetStringDescriptor(hHubDevice,
                                                   ConnectionIndex,
                                                   0,
                                                   0);

    if (supportedLanguagesString == NULL)
    {
        return NULL;
    }

    numLanguageIDs = (supportedLanguagesString->StringDescriptor->bLength - 2) / 2;

    languageIDs = (USHORT *)&supportedLanguagesString->StringDescriptor->bString[0];

    stringDescNodeTail = supportedLanguagesString;

    //
    // Get the Device Descriptor strings
    //

    if (DeviceDesc->iManufacturer)
    {
        stringDescNodeTail = GetStringDescriptors(hHubDevice,
                                                  ConnectionIndex,
                                                  DeviceDesc->iManufacturer,
                                                  numLanguageIDs,
                                                  languageIDs,
                                                  stringDescNodeTail);
    }

    if (DeviceDesc->iProduct)
    {
        stringDescNodeTail = GetStringDescriptors(hHubDevice,
                                                  ConnectionIndex,
                                                  DeviceDesc->iProduct,
                                                  numLanguageIDs,
                                                  languageIDs,
                                                  stringDescNodeTail);
    }

    if (DeviceDesc->iSerialNumber)
    {
        stringDescNodeTail = GetStringDescriptors(hHubDevice,
                                                  ConnectionIndex,
                                                  DeviceDesc->iSerialNumber,
                                                  numLanguageIDs,
                                                  languageIDs,
                                                  stringDescNodeTail);
    }


    //
    // Get the Configuration and Interface Descriptor strings
    //

    descEnd = (PUCHAR)ConfigDesc + ConfigDesc->wTotalLength;

    commonDesc = (PUSB_COMMON_DESCRIPTOR)ConfigDesc;

    while ((PUCHAR)commonDesc + sizeof(USB_COMMON_DESCRIPTOR) < descEnd &&
           (PUCHAR)commonDesc + commonDesc->bLength <= descEnd)
    {
        switch (commonDesc->bDescriptorType)
        {
            case USB_CONFIGURATION_DESCRIPTOR_TYPE:
                if (commonDesc->bLength != sizeof(USB_CONFIGURATION_DESCRIPTOR))
                {
                    break;
                }
                if (((PUSB_CONFIGURATION_DESCRIPTOR)commonDesc)->iConfiguration)
                {
                    stringDescNodeTail = GetStringDescriptors(
                                             hHubDevice,
                                             ConnectionIndex,
                                             ((PUSB_CONFIGURATION_DESCRIPTOR)commonDesc)->iConfiguration,
                                             numLanguageIDs,
                                             languageIDs,
                                             stringDescNodeTail);
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)(((PUCHAR)commonDesc)+commonDesc->bLength);
                continue;

            case USB_INTERFACE_DESCRIPTOR_TYPE:
                if (commonDesc->bLength != sizeof(USB_INTERFACE_DESCRIPTOR) &&
                    commonDesc->bLength != sizeof(USB_INTERFACE_DESCRIPTOR2))
                {
                    break;
                }
                if (((PUSB_INTERFACE_DESCRIPTOR)commonDesc)->iInterface)
                {
                    stringDescNodeTail = GetStringDescriptors(
                                             hHubDevice,
                                             ConnectionIndex,
                                             ((PUSB_INTERFACE_DESCRIPTOR)commonDesc)->iInterface,
                                             numLanguageIDs,
                                             languageIDs,
                                             stringDescNodeTail);
                }
                commonDesc = (PUSB_COMMON_DESCRIPTOR)(((PUCHAR)commonDesc)+commonDesc->bLength);
                continue;

            default:
                commonDesc = (PUSB_COMMON_DESCRIPTOR)(((PUCHAR)commonDesc)+commonDesc->bLength);
                continue;
        }
        break;
    }

    return supportedLanguagesString;
}


PSTRING_DESCRIPTOR_NODE CUSBPorts::GetStringDescriptor (HANDLE  hHubDevice,
                                                        ULONG   ConnectionIndex,
                                                        UCHAR   DescriptorIndex,
                                                        USHORT  LanguageID)
{
    BOOL    success;
    ULONG   nBytes;
    ULONG   nBytesReturned;

    UCHAR   stringDescReqBuf[sizeof(USB_DESCRIPTOR_REQUEST) +
                             MAXIMUM_USB_STRING_LENGTH];

    PUSB_DESCRIPTOR_REQUEST stringDescReq;
    PUSB_STRING_DESCRIPTOR  stringDesc;
    PSTRING_DESCRIPTOR_NODE stringDescNode;

    nBytes = sizeof(stringDescReqBuf);

    stringDescReq = (PUSB_DESCRIPTOR_REQUEST)stringDescReqBuf;
    stringDesc = (PUSB_STRING_DESCRIPTOR)(stringDescReq+1);

    // Zero fill the entire request structure
    //
    memset(stringDescReq, 0, nBytes);

    // Indicate the port from which the descriptor will be requested
    //
    stringDescReq->ConnectionIndex = ConnectionIndex;

    //
    // USBHUB uses URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE to process this
    // IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION request.
    //
    // USBD will automatically initialize these fields:
    //     bmRequest = 0x80
    //     bRequest  = 0x06
    //
    // We must inititialize these fields:
    //     wValue    = Descriptor Type (high) and Descriptor Index (low byte)
    //     wIndex    = Zero (or Language ID for String Descriptors)
    //     wLength   = Length of descriptor buffer
    //
    stringDescReq->SetupPacket.wValue = (USB_STRING_DESCRIPTOR_TYPE << 8)
                                        | DescriptorIndex;

    stringDescReq->SetupPacket.wIndex = LanguageID;

    stringDescReq->SetupPacket.wLength = (USHORT)(nBytes - sizeof(USB_DESCRIPTOR_REQUEST));

    // Now issue the get descriptor request.
    //
    success = DeviceIoControl(hHubDevice,
                              IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION,
                              stringDescReq,
                              nBytes,
                              stringDescReq,
                              nBytes,
                              &nBytesReturned,
                              NULL);

    //
    // Do some sanity checks on the return from the get descriptor request.
    //

    if (!success)
    {
        return NULL;
    }

    if (nBytesReturned < 2)
    {
        return NULL;
    }

    if (stringDesc->bDescriptorType != USB_STRING_DESCRIPTOR_TYPE)
    {
        return NULL;
    }

    if (stringDesc->bLength != nBytesReturned - sizeof(USB_DESCRIPTOR_REQUEST))
    {
        return NULL;
    }

    if (stringDesc->bLength % 2 != 0)
    {
        return NULL;
    }

    //
    // Looks good, allocate some (zero filled) space for the string descriptor
    // node and copy the string descriptor to it.
    //

    stringDescNode = (PSTRING_DESCRIPTOR_NODE)ALLOC(sizeof(STRING_DESCRIPTOR_NODE) +
                                                    stringDesc->bLength);

    if (stringDescNode == NULL)
    {
        return NULL;
    }

    stringDescNode->DescriptorIndex = DescriptorIndex;
    stringDescNode->LanguageID = LanguageID;

    memcpy(stringDescNode->StringDescriptor,
           stringDesc,
           stringDesc->bLength);

    return stringDescNode;
}


PSTRING_DESCRIPTOR_NODE CUSBPorts::GetStringDescriptors (HANDLE  hHubDevice,
                                                         ULONG   ConnectionIndex,
                                                         UCHAR   DescriptorIndex,
                                                         ULONG   NumLanguageIDs,
                                                         USHORT  *LanguageIDs,
                                                         PSTRING_DESCRIPTOR_NODE StringDescNodeTail)
{
    ULONG i;

    for (i=0; i<NumLanguageIDs; i++)
    {
        StringDescNodeTail->Next = GetStringDescriptor(hHubDevice,
                                                       ConnectionIndex,
                                                       DescriptorIndex,
                                                       *LanguageIDs);

        if (StringDescNodeTail->Next)
        {
            StringDescNodeTail = StringDescNodeTail->Next;
        }

        LanguageIDs++;
    }

    return StringDescNodeTail;
}


PTSTR CUSBPorts::GetExternalHubName (HANDLE Hub, ULONG ConnectionIndex)
{
    BOOL                        success;
    ULONG                       nBytes;
    USB_NODE_CONNECTION_NAME	extHubName;
    PUSB_NODE_CONNECTION_NAME   extHubNameW;
    PTSTR                       extHubNameA;

    extHubNameW = NULL;
    extHubNameA = NULL;

    // Get the length of the name of the external hub attached to the
    // specified port.
    //
    extHubName.ConnectionIndex = ConnectionIndex;

    success = DeviceIoControl(Hub,
                              IOCTL_USB_GET_NODE_CONNECTION_NAME,
                              &extHubName,
                              sizeof(extHubName),
                              &extHubName,
                              sizeof(extHubName),
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto GetExternalHubNameError;
    }

    // Allocate space to hold the external hub name
    //
    nBytes = extHubName.ActualLength;

    if (nBytes <= sizeof(extHubName))
    {
        goto GetExternalHubNameError;
    }

    extHubNameW = (PUSB_NODE_CONNECTION_NAME)ALLOC(nBytes);

    if (extHubNameW == NULL)
    {
        goto GetExternalHubNameError;
    }

    // Get the name of the external hub attached to the specified port
    //
    extHubNameW->ConnectionIndex = ConnectionIndex;

    success = DeviceIoControl(Hub,
                              IOCTL_USB_GET_NODE_CONNECTION_NAME,
                              extHubNameW,
                              nBytes,
                              extHubNameW,
                              nBytes,
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto GetExternalHubNameError;
    }

    // Convert the External Hub name
    //
    extHubNameA = WideStrToMultiStr(extHubNameW->NodeName);

    // All done, free the uncoverted external hub name and return the
    // converted external hub name
    //
    FREE(extHubNameW);

    return extHubNameA;


GetExternalHubNameError:
    // There was an error, free anything that was allocated
    //
    if (extHubNameW != NULL)
    {
        FREE(extHubNameW);
        extHubNameW = NULL;
    }

    return NULL;
}


PTSTR CUSBPorts::GetDriverKeyName(HANDLE Hub, ULONG ConnectionIndex)
{
    BOOL                                success;
    ULONG                               nBytes;
    USB_NODE_CONNECTION_DRIVERKEY_NAME  driverKeyName;
    PUSB_NODE_CONNECTION_DRIVERKEY_NAME driverKeyNameW;
    PTSTR                               driverKeyNameA;

    driverKeyNameW = NULL;
    driverKeyNameA = NULL;

    // Get the length of the name of the driver key of the device attached to
    // the specified port.
    //
    driverKeyName.ConnectionIndex = ConnectionIndex;

    success = DeviceIoControl(Hub,
                              IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME,
                              &driverKeyName,
                              sizeof(driverKeyName),
                              &driverKeyName,
                              sizeof(driverKeyName),
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto GetDriverKeyNameError;
    }

    // Allocate space to hold the driver key name
    //
    nBytes = driverKeyName.ActualLength;

    if (nBytes <= sizeof(driverKeyName))
    {
        goto GetDriverKeyNameError;
    }

    driverKeyNameW = (PUSB_NODE_CONNECTION_DRIVERKEY_NAME)ALLOC(nBytes);

    if (driverKeyNameW == NULL)
    {
        goto GetDriverKeyNameError;
    }

    // Get the name of the driver key of the device attached to
    // the specified port.
    //
    driverKeyNameW->ConnectionIndex = ConnectionIndex;

    success = DeviceIoControl(Hub,
                              IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME,
                              driverKeyNameW,
                              nBytes,
                              driverKeyNameW,
                              nBytes,
                              &nBytes,
                              NULL);

    if (!success)
    {
        goto GetDriverKeyNameError;
    }

    // Convert the driver key name
    //
    driverKeyNameA = WideStrToMultiStr(driverKeyNameW->DriverKeyName);

    // All done, free the uncoverted driver key name and return the
    // converted driver key name
    //
    FREE(driverKeyNameW);

    return driverKeyNameA;


GetDriverKeyNameError:
    // There was an error, free anything that was allocated
    //
    if (driverKeyNameW != NULL)
    {
        FREE(driverKeyNameW);
        driverKeyNameW = NULL;
    }

    return NULL;
}

bool CUSBPorts::GetParentDriverName(CString *pszDriverName, CString *pszParentDriverName)
{
    DEVINST     devInst;
    DEVINST     devInstNext;
    CONFIGRET   cr;
    ULONG       walkDone = 0;
    ULONG       len;
    TCHAR       ParentDriveName[MAX_USBNAME_LEN];
    // Get Root DevNode
    //
    cr = CM_Locate_DevNode(&devInst,
                           NULL,
                           0);

    if (cr != CR_SUCCESS)
    {
        return false;
    }

    // Do a depth first search for the DevNode with a matching
    // DriverName value
    //
    while (!walkDone)
    {
        // Get the DriverName value
        //
        len = sizeof(ParentDriveName) / sizeof(ParentDriveName[0]);
        cr = CM_Get_DevNode_Registry_Property(devInst,
                                              CM_DRP_DRIVER,
                                              NULL,
                                              ParentDriveName,
                                              &len,
                                              0);

        // If the DriverName value matches, return the DeviceDescription
        //
        if (cr == CR_SUCCESS && _tcsicmp(pszDriverName->GetBuffer(), ParentDriveName) == 0)
        {
            DEVINST     devInstParent;
            
            cr = CM_Get_Parent(&devInstParent, devInst, 0);
            if (cr != CR_SUCCESS)
            {
                return false;
            }

            len = sizeof(ParentDriveName) / sizeof(ParentDriveName[0]);
            cr = CM_Get_DevNode_Registry_Property(devInstParent,
                                              CM_DRP_DRIVER,
                                              NULL,
                                              ParentDriveName,
                                              &len,
                                              0);
            if (cr == CR_SUCCESS)
            {
                try 
                {
                    *pszParentDriverName = ParentDriveName;
                } 
                catch (...) 
                {
                    SetLastError(ERROR_OUTOFMEMORY);
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        // This DevNode didn't match, go down a level to the first child.
        //
        cr = CM_Get_Child(&devInstNext,
                          devInst,
                          0);

        if (cr == CR_SUCCESS)
        {
            devInst = devInstNext;
            continue;
        }

        // Can't go down any further, go across to the next sibling.  If
        // there are no more siblings, go back up until there is a sibling.
        // If we can't go up any further, we're back at the root and we're
        // done.
        //
        for (;;)
        {
            cr = CM_Get_Sibling(&devInstNext,
                                devInst,
                                0);

            if (cr == CR_SUCCESS)
            {
                devInst = devInstNext;
                break;
            }

            cr = CM_Get_Parent(&devInstNext,
                               devInst,
                               0);


            if (cr == CR_SUCCESS)
            {
                devInst = devInstNext;
            }
            else
            {
                walkDone = 1;
                break;
            }
        }
    }

    return false;
}

