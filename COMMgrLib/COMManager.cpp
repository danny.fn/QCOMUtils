#include "StdAfx.h"
#include "COMManager.h"
#include "Log.h"

#define COMMGR_USED_SYNC        (TRUE+1)

COMManager *COMManager::m_pCOMMgr = NULL;
int         COMManager::m_nRefCnt = 0;

int COMManager::Create(COMManager **ppCOMMgr)
{
    if(m_nRefCnt == 0)
    {
        m_pCOMMgr = new COMManager();
    }
    
    if(ppCOMMgr)
    {
        *ppCOMMgr = m_pCOMMgr;
    }

    m_nRefCnt++;
    return m_nRefCnt;
}

int COMManager::Release(void)
{
    if(m_nRefCnt == 0)
    {
        return 0;
    }
    else 
    {
        if(m_nRefCnt == 1 && m_pCOMMgr)
        {
            m_nRefCnt--;
            delete m_pCOMMgr;
            m_pCOMMgr = NULL;
        }
        else
        {
            m_nRefCnt--;
        }
    }
    return m_nRefCnt;
}

void COMManager::UpdatePhoneMode(int nPortID, int nNewMode, COMPortInfo myInfo)
{
    if(m_pCOMMgr && m_nRefCnt>0)
    {
        m_nRefCnt++;
        m_pCOMMgr->SetPhoneMode(nPortID, nNewMode, myInfo);
        Release();
    }
}

int COMManager::GetCurrentPhoneMode(int nPortID)
{
    if(m_pCOMMgr && m_nRefCnt>0)
    {
        int nRet;
        m_nRefCnt++;
        nRet = m_pCOMMgr->GetPhoneMode(nPortID);
        Release();
        return nRet;
    }
    return DIAG_PHONEMODE_NONE;
}

BOOL COMManager::GetCurrentPortUsed(int nPortID)
{
    if(m_pCOMMgr && m_nRefCnt>0)
    {
        BOOL bRet;
        m_nRefCnt++;
        bRet = m_pCOMMgr->GetUsed(nPortID);
        Release();
        return bRet;
    }
    return FALSE;
}

COMManager::COMManager(void)
{
    int i;

    m_hWaitEvent        = CreateEvent(NULL, TRUE, FALSE, NULL);
    m_bHandleCOMPoll    = TRUE;
    m_pSharedCOMState   = NULL;
    m_hMapFile          = NULL;
    m_pThread           = NULL;

    for(i = COM1;i < COM_MAX;i++)
    {
        m_pCBInfo[i] = NULL;
    }

    m_hMapFile = ::OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, COMMGR_FILMAPPING_NAME);
    if(m_hMapFile == NULL)
    {
        m_hMapFile = ::CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_READWRITE, 0, sizeof(SharedCOMState), COMMGR_FILMAPPING_NAME);
    }
    else
    {
        m_bHandleCOMPoll = FALSE;
    }
    
    if(m_hMapFile) 
    {
        m_pSharedCOMState = (SharedCOMState *) MapViewOfFile(m_hMapFile, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(SharedCOMState));
    }
    
    if(m_bHandleCOMPoll)
    {
        m_pSharedCOMState->m_bFreePollCOM = FALSE;
        for(i = COM1;i < COM_MAX;i++)
        {
            m_pSharedCOMState->m_nPhoneMode[i]  = DIAG_PHONEMODE_NOPORT;
            m_pSharedCOMState->m_bCOMUsed[i]    = FALSE;
            m_pSharedCOMState->m_COMPortInfo[i].m_dwHubIndex = 0;
            m_pSharedCOMState->m_COMPortInfo[i].m_dwPortIndex = 0;
        }
    }
    
    m_bMonitor = TRUE;
    ResetEvent(m_hWaitEvent);
    m_pThread = AfxBeginThread( COMManagerThread, this, THREAD_PRIORITY_NORMAL);
    m_pThread->m_bAutoDelete = TRUE;
}

COMManager::~COMManager(void)
{
    LOG_OUTPUT(_T("COMManager::~COMManager Start %d\n"),m_nRefCnt);
    m_bMonitor = FALSE;
    SetEvent(m_hWaitEvent);
    while(m_pThread) {Sleep(10);}
    CloseHandle(m_hWaitEvent);

    for(int i = COM1;i < COM_MAX;i++)
    {
        CBInfoRemove(i, NULL, NULL);
        m_PhoneDiag[i].StopDiag();
    }
    
    if(m_bHandleCOMPoll)
    {
        m_pSharedCOMState->m_bFreePollCOM = TRUE;
    }
    
    if(m_pSharedCOMState)
    {
        UnmapViewOfFile(m_pSharedCOMState);
        m_pSharedCOMState = NULL;
    }

    if(m_hMapFile)
    {
        CloseHandle(m_hMapFile);
        m_hMapFile = NULL;
    }
    LOG_OUTPUT(_T("COMManager::~COMManager End\n"));
}

BOOL COMManager::SetUsed(int nPortID, BOOL bUsed, LPVOID pUser)
{
    if(NULL == m_pSharedCOMState || nPortID == COM_MAX)
    {
        return FALSE;
    }

    LOG_OUTPUT(_T("COMManager::SetUsed %d COM%d\n"),bUsed,nPortID+1);
    if(bUsed)
    {
        if(m_pSharedCOMState->m_bCOMUsed[nPortID])
        {
            if(m_pSharedCOMState->m_pCOMUser[nPortID] == pUser)
            {
                return TRUE;
            }
            return FALSE;
        }

        if(!m_bHandleCOMPoll)
        {
            m_pSharedCOMState->m_bCOMUsed[nPortID] = COMMGR_USED_SYNC;
            m_pSharedCOMState->m_pCOMUser[nPortID] = pUser;
            while(m_pSharedCOMState->m_bCOMUsed[nPortID] == COMMGR_USED_SYNC)
            {
                Sleep(COMMGR_POLL_SHORT_TIME/2);
            }
        }
        else
        {
            m_pSharedCOMState->m_bCOMUsed[nPortID] = bUsed;
            m_pSharedCOMState->m_pCOMUser[nPortID] = pUser;
            m_PhoneDiag[nPortID].StopDiag();
        }
    }
    else
    {
        if(pUser == m_pSharedCOMState->m_pCOMUser[nPortID])
        {
            m_pSharedCOMState->m_bCOMUsed[nPortID] = bUsed;
            m_pSharedCOMState->m_pCOMUser[nPortID] = NULL;
        }
        else
        {
            return FALSE;
        }
    }
    return TRUE;
}

void COMManager::SetPhoneMode(int nPortID, int nNewMode, COMPortInfo myInfo)
{
    if(NULL == m_pSharedCOMState)
    {
        return;
    }

    if(nNewMode != m_pSharedCOMState->m_nPhoneMode[nPortID])
    {
        LOG_OUTPUT(_T("COMManager::SetPhoneMode %d COM%d\n"),nNewMode,nPortID+1);
        m_pSharedCOMState->m_nPhoneMode[nPortID] = nNewMode;
        m_pSharedCOMState->m_COMPortInfo[nPortID] = myInfo;
        CallCBInfo(nPortID, nNewMode);
    }
}

void COMManager::RegStateChangeCB(int nPortID, PFNSTATECHANGECB pfnStateChangeCB, LPVOID pUserParam)
{
    if(pfnStateChangeCB == NULL || nPortID >= COM_MAX || NULL == m_pSharedCOMState)
    {
        return;
    }
    
    LOG_OUTPUT(_T("COMManager::RegStateChangeCB COM%d 0x%x 0x%x\n"),nPortID+1,pfnStateChangeCB,pUserParam);
    if(nPortID == COM_ALL)
    {
        for(int i=COM1; i<COM_MAX; i++)
        {
            CBInfoAdd(i, pfnStateChangeCB, pUserParam);
            pfnStateChangeCB(pUserParam, i, m_pSharedCOMState->m_nPhoneMode[i]);
        }
    }
    else
    {
        CBInfoAdd(nPortID, pfnStateChangeCB, pUserParam);
        pfnStateChangeCB(pUserParam, nPortID, m_pSharedCOMState->m_nPhoneMode[nPortID]);
    }
    LOG_OUTPUT(_T("COMManager::RegStateChangeCB END COM%d 0x%x 0x%x\n"),nPortID+1,pfnStateChangeCB,pUserParam);
}

void COMManager::UnregStateChangeCB(int nPortID, PFNSTATECHANGECB pfnStateChangeCB, LPVOID pUserParam)
{
    if(nPortID >= COM_MAX)
    {
        return;
    }
    
    LOG_OUTPUT(_T("COMManager::UnregStateChangeCB COM%d 0x%x 0x%x\n"),nPortID+1,pfnStateChangeCB,pUserParam);
    if(nPortID == COM_ALL)
    {
        for(int i=COM1; i<COM_MAX; i++)
        {
            CBInfoRemove(i, pfnStateChangeCB, pUserParam);
        }
    }
    else
    {
        CBInfoRemove(nPortID, pfnStateChangeCB, pUserParam);
    }
    LOG_OUTPUT(_T("COMManager::UnregStateChangeCB END COM%d 0x%x 0x%x\n"),nPortID+1,pfnStateChangeCB,pUserParam);
}

void COMManager::RefreshCOMList(CComboBox &cCOMCombo, CStringArray &arModeString, int nDefCOMID)
{
    CString szPortState;
    int nAddIndex = 0;
    int nSel = cCOMCombo.GetCurSel();

    if(nSel<0)
    {
        nSel = 0;
    }

    cCOMCombo.ResetContent();
    
    for(int i = COM1;i < COM_MAX;i++)
    {
        if(nDefCOMID == i || m_pSharedCOMState->m_nPhoneMode[i] != DIAG_PHONEMODE_NOPORT)
        {
            if(nDefCOMID == i)
            {
                nSel = nAddIndex;
            }

            GetCOMString(i, szPortState, arModeString[m_pSharedCOMState->m_nPhoneMode[i]]);
            cCOMCombo.AddString(szPortState);
            cCOMCombo.SetItemData(nAddIndex,i);
            nAddIndex++;
        }
    }
    
    cCOMCombo.SetCurSel(nSel);
}

void COMManager::RefreshCOMSel(CComboBox &cCOMCombo, int nSel, CStringArray &arModeString)
{
    int nPortID = (int)cCOMCombo.GetItemDataPtr(nSel);
    CString szPortState;
    
    if(!cCOMCombo.GetDroppedState())
    {
        GetCOMString(nPortID, szPortState, arModeString[m_pSharedCOMState->m_nPhoneMode[nPortID]]);
        cCOMCombo.DeleteString(nSel);
        cCOMCombo.InsertString(nSel, szPortState);
        cCOMCombo.SetItemData(nSel,nPortID);
        cCOMCombo.SetCurSel(nSel);
    }
}

int COMManager::GetCOMPortID(CComboBox &cCOMCombo, int nCOMIdx)
{
    int nPortID = (int)cCOMCombo.GetItemDataPtr(nCOMIdx);
    return nPortID;
}

UINT COMManager::COMManagerThread(LPVOID pParam)
{
    COMManager *pThis = (COMManager *)pParam;
    int         nWaitTime = 0;
    
    LOG_OUTPUT(_T("COMManager::COMManagerThread Start\n"));
    pThis->UpdateCOMInfo(FALSE);
    //SetEvent(pThis->m_hEvent);
    while(pThis->m_bMonitor && WaitForSingleObject(pThis->m_hWaitEvent, COMMGR_POLL_SHORT_TIME) != WAIT_OBJECT_0)
    {
        nWaitTime+=COMMGR_POLL_SHORT_TIME;
        if(nWaitTime >= COMMGR_POLL_TIME)
        {
            pThis->UpdateCOMInfo(FALSE);
            nWaitTime=0;
        }
        else
        {
            pThis->UpdateCOMInfo(TRUE);
        }
    }

    pThis->ResetCOMManagerThread();
    LOG_OUTPUT(_T("COMManager::COMManagerThread End\n"));
    return 0;
}

void COMManager::UpdateCOMInfo(BOOL bOnlyUsedInfo)
{
    int i;
    
    if(!m_bHandleCOMPoll)
    {
        if(m_pSharedCOMState->m_bFreePollCOM)
        {
            m_pSharedCOMState->m_bFreePollCOM = FALSE;
            m_bHandleCOMPoll = TRUE;
            bOnlyUsedInfo    = FALSE;
        }
    }
    
    if(!m_bHandleCOMPoll)
    {
        for(i = COM1;i < COM_MAX;i++)
        {
            SetPhoneMode(i,m_pSharedCOMState->m_nPhoneMode[i],m_pSharedCOMState->m_COMPortInfo[i]);
        }
        return;
    }

    if(bOnlyUsedInfo)
    {
        for(i = COM1;i < COM_MAX;i++)
        {
            if(COMMGR_USED_SYNC == m_pSharedCOMState->m_bCOMUsed[i]) // 同步不同进程之间的SetUsed
            {
                m_PhoneDiag[i].StopDiag();
                m_pSharedCOMState->m_bCOMUsed[i] = TRUE;
            }
        }
    }
    else
    {
        COMPortInfo myInfo = {0,0};
        for(i = COM1;i < COM_MAX;i++)
        {
            if(FALSE == m_pSharedCOMState->m_bCOMUsed[i])
            {
                if(FALSE == m_PhoneDiag[i].StartDiag(i, TRUE))
                {
                    SetPhoneMode(i, DIAG_PHONEMODE_NOPORT,myInfo);
                }
            }
            else if(COMMGR_USED_SYNC == m_pSharedCOMState->m_bCOMUsed[i]) // 同步不同进程之间的SetUsed
            {
                m_PhoneDiag[i].StopDiag();
                m_pSharedCOMState->m_bCOMUsed[i] = TRUE;
            }
        }
    }
}

void COMManager::GetCOMString(int nPort, CString &szString, CString &szMode)
{
    szString.Format(_T("COM%d - %s"), nPort+1, szMode);
}

void COMManager::CBInfoAdd(int nPortID, PFNSTATECHANGECB pfnCB, LPVOID pUser)
{
    CBInfo **ppHead = &m_pCBInfo[nPortID];
    CBInfo *pNext   = NULL;
    
    // 查找是否有重复的记录
    CBInfo *pHead = m_pCBInfo[nPortID];
    while(pHead)
    {
        pNext = pHead->m_pNext;
        if(pHead->m_pfnStateChangeCB == pfnCB && pHead->m_pUserParam == pUser)
        {
            return;
        }
        pHead = pNext;
    }

    pNext = NULL;
    if(*ppHead)
    {
        pNext = (*ppHead)->m_pNext;
    }

    *ppHead = new CBInfo();
    (*ppHead)->m_pNext              = pNext;
    (*ppHead)->m_pfnStateChangeCB   = pfnCB;
    (*ppHead)->m_pUserParam         = pUser;
}

void COMManager::CBInfoRemove(int nPortID, PFNSTATECHANGECB pfnCB, LPVOID pUser)
{
    CBInfo **ppHead = &m_pCBInfo[nPortID];
    CBInfo **ppNext = NULL;
    CBInfo *pNext = NULL;
    
    if(pfnCB && pUser)
    {
        while(*ppHead)
        {
            pNext = (*ppHead)->m_pNext;
            if((*ppHead)->m_pfnStateChangeCB == pfnCB && (*ppHead)->m_pUserParam == pUser)
            {
                // remove
                delete (*ppHead);
                *ppHead = pNext;
            }
            else
            {
                ppHead = &((*ppHead)->m_pNext);
            }
        }
    }
    else if(pfnCB)
    {
        while(*ppHead)
        {
            pNext = (*ppHead)->m_pNext;
            if((*ppHead)->m_pfnStateChangeCB == pfnCB)
            {
                // remove
                delete (*ppHead);
                *ppHead = pNext;
            }
            else
            {
                ppHead = &((*ppHead)->m_pNext);
            }
        }
    }
    else if(pUser)
    {
        while(*ppHead)
        {
            pNext = (*ppHead)->m_pNext;
            if((*ppHead)->m_pUserParam == pUser)
            {
                // remove
                delete (*ppHead);
                *ppHead = pNext;
            }
            else
            {
                ppHead = &((*ppHead)->m_pNext);
            }
        }
    }
    else
    {
        while(*ppHead)
        {
            pNext = (*ppHead)->m_pNext;
            // remove
            delete (*ppHead);
            *ppHead = pNext;
        }
    }
}

void COMManager::CallCBInfo(int nPortID, int nNewMode)
{
    CBInfo *pHead = m_pCBInfo[nPortID];
    CBInfo *pNext = NULL;
    while(pHead)
    {
        pNext = pHead->m_pNext;
        pHead->m_pfnStateChangeCB(pHead->m_pUserParam, nPortID, nNewMode);
        pHead = pNext;
    }

    for(int i = COM1;i < COM_MAX;i++)
    {
        if(IsPortSame(i,nPortID) && i != nPortID)
        {
            pHead = m_pCBInfo[i];
            pNext = NULL;
            while(pHead)
            {
                pNext = pHead->m_pNext;
                pHead->m_pfnStateChangeCB(pHead->m_pUserParam, nPortID, nNewMode);
                pHead = pNext;
            }
        }
    }
}

BOOL COMManager::IsPortSame(int nPortID1, int nPortID2)
{
    if(nPortID1 == nPortID2)
    {
        return TRUE;
    }

    if(!m_pSharedCOMState)
    {
        return FALSE;
    }

    if(m_pSharedCOMState->m_COMPortInfo[nPortID1].m_dwHubIndex == 0 
        ||m_pSharedCOMState->m_COMPortInfo[nPortID1].m_dwPortIndex == 0)
    {
        return FALSE;
    }
    
    if(m_pSharedCOMState->m_COMPortInfo[nPortID2].m_dwHubIndex == 0 
        ||m_pSharedCOMState->m_COMPortInfo[nPortID2].m_dwPortIndex == 0)
    {
        return FALSE;
    }
    
    if((m_pSharedCOMState->m_COMPortInfo[nPortID1].m_dwHubIndex == m_pSharedCOMState->m_COMPortInfo[nPortID2].m_dwHubIndex )
        && (m_pSharedCOMState->m_COMPortInfo[nPortID1].m_dwPortIndex == m_pSharedCOMState->m_COMPortInfo[nPortID2].m_dwPortIndex))
    {
        return TRUE;
    }
    return FALSE;
}
