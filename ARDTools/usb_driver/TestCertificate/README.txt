============== How to Add Test Certificate ===============

Test certificate may need to be applied to Windows
before installing the USB host driver. A same certificate
will only need to be added once. Follow the steps listed
below to add test certificate to the system under test.

1. Launch Windows Explorer and locate the certificate
   file qcusbtest.cer

2. Right-click on qcusbtest.cer and choose Install Certificate.

3. Follow instructions and place the certificate in the
   following certificate stores:
      Trusted Publishers
      Trusted Root Certification Authorities
   Step 1 through 3 will need to be repeated to place the certificate
   in more than one certificate stores.

4. On Windows Vista, open a Command Prompt with "Run as
   administrator" and issue the following command:
      bcdedit -set testsigning on

   For pre-Vista Windows versions, skip this step.

5. Reboot the system.
