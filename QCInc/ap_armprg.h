#ifndef AP_ARMPRG_H
#define AP_ARMPRG_H
/*===========================================================================

     D M S S   F L A S H   P R O G R A M M E R   H E A D E R   F I L E

DESCRIPTION
   This file contains common definitions for the DMSS flash programmer
   utility.
   
   Some things are MSM specific and must be ported to a new architecture.

  Copyright (c) 2008 Qualcomm Incorporated. 
  All Rights Reserved.
  Qualcomm Confidential and Proprietary
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*============================================================================
                          EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //depot/asic/msmshared/tools/hostdl/HOSTDL.15.00/common/ap_armprg.h#4 $ $DateTime: 2008/07/20 21:42:45 $ $Author: opham $
  Integrated from P402:  //depot/common-sw/armprg/armprg-9.0/common/ap_armprg.h#3 
  
 when       who     what, where, why
 --------   ---     --------------------------------------------------------
 06/10/08    op      Add Factory and Raw modes
 06/16/08    rt      Cleanup debug print macro definitions
 03/18/08    op      Initial version
============================================================================*/

//--------------------------------------------------------------------------
// Include Files
//--------------------------------------------------------------------------

#include <string.h>
#include "comdef.h"


//--------------------------------------------------------------------------
// Defines
//--------------------------------------------------------------------------

/* For transport layer */
#define CHECK_FOR_DATA() (*disptbl->check)()
#define DRAIN_TRANSMIT_FIFO() (*disptbl->drain)()
#define TRANSMIT_BYTE(c) (*disptbl->xmit)(c)

/* For printing */

#if defined(COMPILE_PRINTF)
  /* When compiled with NOISY, these statements always print */
  #if defined(NOISY)
    #define DPRINTF(x) jprintf x
  #else
    #define DPRINTF(x) /**/
  #endif /* NOISY */

  /* When compiled with TNOISY, these statements will print depending
   * on level of TNOISY.  Only changeable at compile time
   */
  #if defined(TNOISY)
    #define TPRINTF(level, x) if (TNOISY >= level) {jprintf x;}
  #else
    #define TPRINTF(level, x) /**/
  #endif /* NOISY */
#else
  #define DPRINTF(x)        /**/
  #define TPRINTF(level, x) /**/
#endif


/* These macros scattered in strategic places in code are a very crude
 * way of profiling.  Each statement needs its own number or letter
 * and if all instances end up in the log file, all sections of code
 * were executed.
 */
#if defined(NOISY)  && defined(PROFILING)
#define PROF(x)  jprintf("  === ");jprintf(x);jprintf("\n");
#else
#define PROF(x) /**/
#endif

//--------------------------------------------------------------------------
// Type Declarations
//--------------------------------------------------------------------------

/* List to include all the supported interfaces for the software download */
typedef enum
{
  INTERFACE_INVALID,
  INTERFACE_UART,
  INTERFACE_USB
} interface_id_type;



/* List to include all the return types for partition table command */
typedef enum
{
  PARTI_TBL_ACCEPTED,
  PARTI_TBL_DIFFERS,
  PARTI_TBL_BAD_FORMAT,
  PARTI_TBL_ERASE_FAIL,
  PARTI_TBL_UNKNOWN_ERROR
} parti_tbl_type;


/* Defines for Open Multi command */
#define OPEN_MULTI_SUCCESS             0x00
#define OPEN_MULTI_LENGTH_EXCEEDED     0x01
#define OPEN_MULTI_PAYLOAD_NOT_ALLOWED 0x02
#define OPEN_MULTI_PAYLOAD_REQUIRED    0x03
#define OPEN_MULTI_UNKNOWN_ERROR       0x04
#define OPEN_MULTI_UNKNOWN_PARTITION   0x05

/* List to use for which image to open for */
typedef enum
{
  OPEN_MULTI_MODE_NONE        = 0x00,    /* Not opened yet                       */
  OPEN_MULTI_MODE_PBL         = 0x01,    /* Primary Boot Loader                  */
  OPEN_MULTI_MODE_QCSBLHDCFG  = 0x02,    /* QC 2ndary Boot Loader Header and     */
                                   /*      Config Data                     */
  OPEN_MULTI_MODE_QCSBL       = 0x03,    /* QC 2ndary Boot Loader                */
  OPEN_MULTI_MODE_OEMSBL      = 0x04,    /* OEM 2ndary Boot Loader               */
  OPEN_MULTI_MODE_AMSS        = 0x05,    /* AMSS modem executable                */
  OPEN_MULTI_MODE_APPS        = 0x06,    /* APPS executable                      */
  OPEN_MULTI_MODE_OBL         = 0x07,    /* OTP Boot Loader                      */
  OPEN_MULTI_MODE_FOTAUI      = 0x08,    /* FOTA UI binarh                       */
  OPEN_MULTI_MODE_CEFS        = 0x09,    /* Modem CEFS image                     */    
  OPEN_MULTI_MODE_APPSBL      = 0x0A,    /* APPS Boot Loader                     */
  OPEN_MULTI_MODE_APPS_CEFS   = 0x0B,    /* APPS CEFS image                      */    
  OPEN_MULTI_MODE_APPS_WM60   = 0x0C,    /* Flash.bin image for Windows mobile 6 */ 
  OPEN_MULTI_MODE_DSP1        = 0x0D,    /* DSP1 runtime image                   */ 
  OPEN_MULTI_MODE_CUSTOM      = 0x0E,    /* Image for user defined partition     */ 
  OPEN_MULTI_MODE_DBL         = 0x0F,    /* Device Boot Loader                   */
  OPEN_MULTI_MODE_OSBL        = 0x10,    /* Fail Safe Boot Loader                */
  OPEN_MULTI_MODE_FSBL        = 0x11,    /* OS Boot Loader                       */
  OPEN_MULTI_MODE_DSP2        = 0x12,    /* DSP2 runtime image                   */ 
  OPEN_MULTI_MODE_RAW         = 0x13,    /* APPS Raw image                       */
  OPEN_MULTI_MODE_MSIMAGE     = 0x21,    /* MSIMAGE image                        */
} open_multi_mode_type;

/* List to use for which image to open in Open mode. Modes 
 * Bootloader download (0x01), Bootable image download (0x02), 
 * CEFS Image Download (0x03) are not supported in OPEN Mode. 
 */

typedef enum
{
  OPEN_MODE_NONE        = 0x00,    /* Not opened yet                    */
  OPEN_BOOTLOADER = 0x01,    /* Bootloader Image                  */
  OPEN_BOOTABLE   = 0x02,    /* Bootable Image                    */
  OPEN_CEFS       = 0x03,    /* CEFS Image                        */
  OPEN_MODE_FACTORY = 0x04         /* Factory Image                     */

} open_mode_type;

/* Status Code Enumeration
 * This lists the status result codes passed around in the program.
 * This enum is used to index a table of response packets, so these
 * values map exactly to possible responses. 
 */

typedef enum
{
  ACK = 0x00,                 /* Successful                                  */
  RESERVED_1 = 0x01,          /* Reserved                                    */
  NAK_INVALID_DEST = 0x02,    /* Failure: destination address is invalid.    */
  NAK_INVALID_LEN = 0x03,     /* Failure: operation length is invalid.       */
  NAK_EARLY_END = 0x04,       /* Failure: packet was too short for this cmd. */
  NAK_INVALID_CMD = 0x05,     /* Failure: invalid command                    */
  RESERVED_6 = 0x06,          /* Reserved                                    */
  NAK_FAILED = 0x07,          /* Failure: operation did not succeed.         */
  NAK_WRONG_IID = 0x08,       /* Failure: intelligent ID code was wrong.     */
  NAK_BAD_VPP = 0x09,         /* Failure: programming voltage out of spec    */
  NAK_VERIFY_FAILED = 0x0A,   /* Failure: readback verify did not match      */
  RESERVED_0xB = 0x0B,        /* Reserved */
  NAK_INVALID_SEC_CODE = 0x0C,/* Failure: Incorrect security code            */
  NAK_CANT_POWER_DOWN = 0x0D, /* Failure: Cannot power down phone            */
  NAK_NAND_NOT_SUPP = 0x0E,   /* Failure: Download to NAND not supported     */
  NAK_CMD_OUT_SEQ = 0x0F,     /* Failure: Command out of sequence            */
  NAK_CLOSE_FAILED = 0x10,    /* Failure: Close command failed               */
  NAK_BAD_FEATURE_BITS = 0x11,/* Failure: Incompatible Feature Bits          */
  NAK_NO_SPACE = 0x12,        /* Failure: Out of space                       */
  NAK_INVALID_SEC_MODE = 0x13,/* Failure: Multi-Image invalid security mode  */
  NAK_MIBOOT_NOT_SUPP = 0x14, /* Failure: Multi-Image boot not supported     */
  NAK_PWROFF_NOT_SUPP = 0x15  /* Failure: Power off not supported            */
}
response_code_type;

/*
 *  Structure definition to hold function pointers for dispatching
 */
 typedef struct dispatch {
   boolean (*init)   (void);
   boolean (*active) (void);
   void    (*drain)  (void);
   void    (*check)  (void);
   void    (*xmit)   (byte chr);
 } DISP, *DISPPTR;

//--------------------------------------------------------------------------
// Extern Definitions
//--------------------------------------------------------------------------
/* global pointer to current function dispatch table */
extern DISPPTR disptbl;

#if defined(COMPILE_PRINTF)
extern int jprintf(const char *fmt,...);
extern void term_put(char);
#endif



//--------------------------------------------------------------------------
// Function Definitions
//--------------------------------------------------------------------------

/*===========================================================================

FUNCTION packet_loop

DESCRIPTION
  This function is the main loop implementing the DMSS Async Download
  Protocol.  It loops forever, processing packets as they arrive.

DEPENDENCIES
  All necessary initialization for normal CPU operation must have
  been performed, and the UART must have been initialized, before
  entering this function.

RETURN VALUE
  This function does not return.

SIDE EFFECTS
  None.

===========================================================================*/

extern void packet_loop (void);

/*===========================================================================

FUNCTION packet_init

DESCRIPTION
  This function initializes the receive packet list.
  It also copies some data from the global identification block so that
  the data in the block will be included by the linker and will be able
  to be seen in the resulting binary.

DEPENDENCIES
  None.

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

extern void packet_init (void);


/*===========================================================================

FUNCTION msm_reset

DESCRIPTION
  This function causes the MSM to reset

DEPENDENCIES
  None

RETURN VALUE
  None.

SIDE EFFECTS
  None.

===========================================================================*/

extern void msm_reset (void);


#ifdef USE_BUFFERED_TERMIO
extern void jprintf_init (void);
extern void term_flush (void);

#endif


#endif /* AP_ARMPRG_H */
