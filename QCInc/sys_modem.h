#ifndef SYS_MODEM_H
#define SYS_MODEM_H

/*===========================================================================

              S Y S T E M   H E A D E R   F I L E

DESCRIPTION
  This header file contains definitions that are shared between Call Manager,
  Call Manager clients and the GSM/WCDMA protocol stacks.

Copyright (c) 2009 by QUALCOMM INCORPORATED. All Rights Reserved.

Export of this technology or software is regulated by the U.S. Government.
Diversion contrary to U.S. law prohibited.

===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //source/qcom/qct/modem/mmode/cm/dev/DualSim/inc/sys_modem.h#6 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
12/23/10   rm      Tune-away changes
09/09/09   sv      Initial Version 
===========================================================================*/
#ifndef AEE_SIMULATOR
#ifndef _MSC_VER // Gemsea Modify
#include "msg.h"
#endif
#endif
/**
** Eumeration of Active Subscription ID's
*/
typedef enum {

  SYS_MODEM_AS_ID_NONE = -1,
    /**< Internal Only */

  SYS_MODEM_AS_ID_1,
    /**< Subscription ID 1 */

  SYS_MODEM_AS_ID_2,
    /**< Subscription ID 2 */

  SYS_MODEM_AS_ID_NO_CHANGE,
    /**< No Change in Subscription ID */

  SYS_MODEM_AS_ID_MAX
    /**< Internal Only */

} sys_modem_as_id_e_type;


#if defined(FEATURE_DUAL_SIM) || defined(FEATURE_GW_G_DUAL_STANDBY)
/* NUMBER OF Active Subscription IDs to work in hybrid manner */
#define MAX_AS_IDS 2
#else
#define MAX_AS_IDS 1
#endif


/** Enumeration of Daul Standby Preferences
 */
typedef enum {

  SYS_MODEM_DS_PREF_NONE,
    /**< Internal use, range checking */

  SYS_MODEM_DS_PREF_SINGLE_STANDBY,
    /**<
    ** Phone is in Single Standby mode
    */

  SYS_MODEM_DS_PREF_DUAL_STANDBY,
    /**<
    ** Phone is in Dual Standby mode
    */

  SYS_MODEM_DS_PREF_NO_CHANGE,
    /**<
    ** Do not change the Dual Standby preference.
    ** should be mapped to the existing value.
    ** Only temporary value and should never be written to NV.
    */

  SYS_MODEM_DS_PREF_DUAL_STANDBY_NO_TUNEAWAY,
    /**<
    ** Phone is in Dual Standby mode with no tune away.
    */

  SYS_MODEM_DS_PREF_MAX
    /**< Internal use for range checking 
    ** should never be written to NV.
    */

} sys_modem_dual_standby_pref_e_type;

#ifdef FEATURE_DUAL_SIM
#define MSG_HIGH_DS( sub , str, p1, p2, p3)\
    MSG_4 (MSG_SSID_DFLT, MSG_LEGACY_HIGH, "ds%d" str , (sub+1), (p1), (p2), (p3))
    
#define MSG_MED_DS( sub , str, p1, p2, p3)\
    MSG_4 (MSG_SSID_DFLT, MSG_LEGACY_MED, "ds%d" str , (sub+1), (p1), (p2), (p3))
    
#define MSG_LOW_DS( sub , str, p1, p2, p3)\
    MSG_4 (MSG_SSID_DFLT, MSG_LEGACY_LOW, "ds%d" str , (sub+1), (p1), (p2), (p3))
    
#define MSG_ERROR_DS( sub , str, p1, p2, p3)\
    MSG_4 (MSG_SSID_DFLT, MSG_LEGACY_ERROR, "ds%d" str , (sub+1), (p1), (p2), (p3))

#define MSG_FATAL_DS( sub , str, p1, p2, p3)\
    MSG_4 (MSG_SSID_DFLT, MSG_LEGACY_FATAL, "ds%d" str , (sub+1), (p1), (p2), (p3))
    
#else

/* If DUAL SIM is not defined, no need to print subscription id */
#define MSG_HIGH_DS( sub , str, p1, p2, p3)\
    MSG_HIGH (str , (p1), (p2), (p3))
    
#define MSG_MED_DS( sub , str, p1, p2, p3)\
    MSG_MED (str , (p1), (p2), (p3))
    
#define MSG_LOW_DS( sub , str, p1, p2, p3)\
    MSG_LOW (str , (p1), (p2), (p3))
    
#define MSG_ERROR_DS( sub , str, p1, p2, p3)\
    MSG_ERROR (str , (p1), (p2), (p3))

#define MSG_FATAL_DS( sub , str, p1, p2, p3)\
    MSG_FATAL (str , (p1), (p2), (p3))
    
#endif


#endif /* SYS_MODEM_H */
