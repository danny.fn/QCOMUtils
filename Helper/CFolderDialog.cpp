//------------------------------------------------------
// ʵ���ļ�

#include "stdafx.h"
#include "CFolderDialog.h"

static int CALLBACK BrowseDirectoryCallback( HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData ) 
{
    CFolderDialog* folderDialog = reinterpret_cast< CFolderDialog* >( lpData ); 
    folderDialog->OnCallback( hWnd, uMsg, lParam );            
    return 0;
}    

CFolderDialog::CFolderDialog( 
    int rootDirFlag, 
    const CString& focusDir,
    const CString& title,
    DWORD browseInfoFlag
    )
    : m_nRootDirFlag( rootDirFlag )
    , m_szFocusDir( focusDir )
    , m_szTitle( title )
    , m_dwBrowseInfoFlag( browseInfoFlag )
    , m_hDialog( NULL )
    , m_szFinalPath( _T( "" ) )
{
}

INT_PTR CFolderDialog::DoModal() 
{
    LPITEMIDLIST rootLoation;

    SHGetSpecialFolderLocation( NULL, m_nRootDirFlag, &rootLoation );
    if ( rootLoation == NULL ) {
        throw new CInvalidArgException();
    }

    BROWSEINFO browseInfo;
    ZeroMemory( &browseInfo, sizeof( browseInfo ) );
    browseInfo.pidlRoot     = rootLoation;
    browseInfo.ulFlags      = m_dwBrowseInfoFlag;
    browseInfo.lpszTitle    = m_szTitle;
    browseInfo.lpfn         = BrowseDirectoryCallback;
    browseInfo.lParam       = ( LPARAM )this;
    
    LPITEMIDLIST targetLocation = SHBrowseForFolder( &browseInfo );
    if ( targetLocation == NULL ) 
    {
        return IDCANCEL;
    }
    
    TCHAR targetPath[ MAX_PATH ] = { _T( '\0' ) };
    SHGetPathFromIDList( targetLocation, targetPath );
    m_szFinalPath = targetPath;
    return IDOK;
}

void CFolderDialog::OnCallback( HWND hWnd, UINT uMsg, LPARAM lParam ) 
{
    m_hDialog = hWnd;
    if ( uMsg == BFFM_INITIALIZED ) 
    {
        SetFocusDirectory();            
    }
}

void CFolderDialog::SetFocusDirectory() 
{
    ASSERT( m_hDialog != NULL );
    if ( m_szFocusDir != _T( "" ) ) 
    {
        ::SendMessage( m_hDialog, BFFM_SETSELECTION, TRUE, ( LPARAM )m_szFocusDir.GetString() ); 
    }
} 

