// Log.h: interface for the CLog class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOG_H_INCLUDED_)
#define AFX_LOG_H_INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifdef LOG_ENABLE
#include "DloadConfig.h"

class CLog  
{
public:
    static int              Create(CLog **ppCLog);
    static int              Release(void);
    static void             Log(LPCTSTR pszFormat, ...);
    static void             LogInfo(char *szFile, int nLine);
public:
    BOOL OpenLog();
	void XLog(LPCTSTR lpszLog);
    void XLogInfo( char *szFile, int nLine);
    void CloseLog( void );
	BOOL IsOpenLog();

public:
	CLog();
	 ~CLog();
private:
	FILE   *m_pLogFile;
    CDloadConfig   *m_pDloadConfig;

    // 只允许存在一个CLog实例
    static CLog  *m_pCLog;
    static int   m_nRefCnt;
};

#define LOG_OUTPUT      CLog::LogInfo(__FILE__,__LINE__);CLog::Log
#else
#define LOG_OUTPUT      TRACE
#endif
#endif // !defined(AFX_LOG_H_INCLUDED_)
