/*====================================================================================================

                                       ==MODIFY HISTOY FOR FILE==

  when         who     what, where, why
  ----------   ---     ----------------------------------------------------------
  03/08/2004   tyz      create
  
=====================================================================================================*/
#if !defined(_CHAECONVERT_H__)
#define _CHAECONVERT_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/*===========================================================================

                  INCLUDE FILES FOR MODULE
  
===========================================================================*/
//#include ""

/*===========================================================================

                  DEFINITIONS AND CONSTANTS
  
===========================================================================*/

/*===========================================================================

                     FUNCTION DECLARATIONS
  
===========================================================================*/


int UTF8TOTCHAR(TCHAR* dest, const char* pSource, int length);

//length is the dest string count in byte
int TCHARTOUTF8(char* dest, const TCHAR* pSource, int length);

BOOL IsMP3Valid(CString name);

BOOL RemoveDir(CString sourceDirName);

BOOL CopyDirectory(CString sourceDirName, CString destDirName);

LPCTSTR GetLpctstrFromCString(LPCTSTR lpszCString);
#endif // !defined(_CHAECONVERT_H__)