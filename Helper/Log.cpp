// Log.cpp: implementation of the CLog class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Log.h"

#include <stdarg.h>

CLog *CLog::m_pCLog     = NULL;
int   CLog::m_nRefCnt   = 0;

int CLog::Create(CLog **ppCLog)
{
    if(m_nRefCnt == 0)
    {
        m_pCLog = new CLog();
        if(!ppCLog) m_nRefCnt++;
        m_pCLog->OpenLog();
    }
    
    if(ppCLog)
    {
        *ppCLog = m_pCLog;
        m_nRefCnt++;
    }
    return m_nRefCnt;
}

void CLog::Log(LPCTSTR pszFormat, ...)
{
    if(!m_pCLog)
    {
        return;
    }

    if(!m_pCLog->IsOpenLog())
    {
        return;
    }

    va_list vlst;

    va_start( vlst, pszFormat );

    _TCHAR szBuf[512];

    _vstprintf( szBuf, pszFormat, vlst );

    va_end( vlst );

    m_pCLog->XLog(szBuf);
}

void CLog::LogInfo(char *szFile, int nLine)
{
    if(!m_pCLog)
    {
        return;
    }

    if(!m_pCLog->IsOpenLog())
    {
        return;
    }

    m_pCLog->XLogInfo(szFile,nLine);
}

int CLog::Release(void)
{
    if(m_nRefCnt == 0)
    {
        return 0;
    }
    else if(m_nRefCnt == 1 && m_pCLog)
    {
        delete m_pCLog;
        m_pCLog = NULL;
    }
    m_nRefCnt--;
    return m_nRefCnt;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLog::CLog()
{
	m_pLogFile = NULL;
    CDloadConfig::Create(&m_pDloadConfig);
}

CLog::~CLog()
{
    CloseLog();
    CDloadConfig::Release();
}

void CLog::XLog(LPCTSTR lpszLog)
{
    if( m_pLogFile != NULL )
    {
        int nNewLen = 0;
        _TCHAR szBuf[512];
        for(;;)
        {
            if(*lpszLog == 0) break;
            if(*lpszLog == _T('\r') || *lpszLog == _T('\n'))
            {
            }
            else
            {
                szBuf[nNewLen++] = *lpszLog;
            }
            lpszLog++;
        }
        szBuf[nNewLen] = 0;

		int nByte = WideCharToMultiByte(CP_ACP,0,szBuf,-1,NULL,0,NULL,NULL);
		char *psz = new char[nByte];
		WideCharToMultiByte(CP_ACP,0,szBuf,-1,psz,nByte,NULL,NULL);
        fprintf( m_pLogFile, "- %s\n", psz );
        TRACE(_T("- %s\n"), szBuf);
		delete []psz;
    }
}

void CLog::XLogInfo(char *szFile, int nLine)
{
    if( m_pLogFile != NULL )
    {
        char *szName = strrchr(szFile,'\\');
        if(szName)
        {
            szName++;
        }
        SYSTEMTIME  currentTime;
        GetLocalTime( &currentTime);
        fprintf( m_pLogFile, "%02d:%02d:%02d:%03d - %s %04d ",  
			currentTime.wHour,
			currentTime.wMinute,
			currentTime.wSecond,
            currentTime.wMilliseconds,
			szName, 
            nLine);
        TRACE("%02d:%02d:%02d:%03d - %s %04d ",  
			currentTime.wHour,
			currentTime.wMinute,
			currentTime.wSecond,
            currentTime.wMilliseconds,
			szName, 
            nLine);
    }
}

BOOL CLog::OpenLog()
{
    if(m_pDloadConfig->IsLogEnable())
    {
	    CloseLog();
    	
        _TCHAR szLogFileName[ MAX_PATH ];
        SYSTEMTIME  currentTime ;
        GetLocalTime( &currentTime);
        _stprintf( szLogFileName, _T(".\\log\\LOG_%d_%d_%d.txt"), 
		    currentTime.wYear,
		    currentTime.wMonth,
		    currentTime.wDay );
    	
        CreateDirectory(_T(".\\log\\"), NULL);
        m_pLogFile = _tfopen( szLogFileName, _T("a+") );  
        if( m_pLogFile != NULL )
        {
            return TRUE;
        }
    }
    return FALSE;
}

void CLog::CloseLog()
{
    if( m_pLogFile != NULL )
    {
        fclose( m_pLogFile );
        m_pLogFile = NULL;
    }    
}

BOOL CLog::IsOpenLog()
{
	if(m_pLogFile != NULL)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}