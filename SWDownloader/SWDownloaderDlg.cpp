// SWDownloaderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SWDownloader.h"
#include "SWDownloaderDlg.h"
#include "CIniFile.h"
#include "CFolderDialog.h"
#include "DloadSettingDlg.h"
#include "PortInfoDlg.h"
#include "Log.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_MY_UPDATEINFO    (WM_USER+1)
#define WM_MY_UPDATESTATE   (WM_USER+2)
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CSWDownloaderDlg dialog




CSWDownloaderDlg::CSWDownloaderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSWDownloaderDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSWDownloaderDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_COMBO_PATH, m_cPathCombo);
    DDX_Control(pDX, IDC_BUTTON_BROWSE, m_cBrowseButton);
    DDX_Control(pDX, IDC_COMBO_CHIPSET, m_cChipsetCombo);
    DDX_Control(pDX, IDOK, m_cOKButton);
    DDX_Control(pDX, IDCANCEL, m_cCancelButton);
    DDX_Control(pDX, IDC_LIST_DLOAD, m_cDloadListCtrl);
    DDX_Control(pDX, IDC_CHECK_AUTODLOAD, m_cAutoDLoadCheck);
    DDX_Control(pDX, IDC_BUTTON_SETTING, m_cSettingButton);
}

BEGIN_MESSAGE_MAP(CSWDownloaderDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
    ON_MESSAGE(WM_MY_UPDATEINFO, OnMyUpdateInfo) 
    ON_MESSAGE(WM_MY_UPDATESTATE, OnMyUpdateState) 
    ON_BN_CLICKED(IDOK, &CSWDownloaderDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CSWDownloaderDlg::OnBnClickedCancel)
    ON_WM_DESTROY()
    ON_WM_TIMER()
    ON_BN_CLICKED(IDC_BUTTON_BROWSE, &CSWDownloaderDlg::OnBnClickedButtonBrowse)
    ON_CBN_CLOSEUP(IDC_COMBO_CHIPSET, &CSWDownloaderDlg::OnCbnCloseupComboChipset)
    ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_DLOAD, &CSWDownloaderDlg::OnLvnItemchangedListDload)
    ON_NOTIFY(NM_CLICK, IDC_LIST_DLOAD, &CSWDownloaderDlg::OnNMClickListDload)
    ON_CBN_CLOSEUP(IDC_COMBO_PATH, &CSWDownloaderDlg::OnCbnCloseupComboPath)
    ON_CBN_DROPDOWN(IDC_COMBO_PATH, &CSWDownloaderDlg::OnCbnDropdownComboPath)
    ON_BN_CLICKED(IDC_BUTTON_SETTING, &CSWDownloaderDlg::OnBnClickedButtonSetting)
    ON_NOTIFY(NM_RCLICK, IDC_LIST_DLOAD, &CSWDownloaderDlg::OnNMRClickListDload)
    ON_BN_CLICKED(IDC_BUTTON_COMSTATE, &CSWDownloaderDlg::OnBnClickedButtonComstate)
    ON_BN_CLICKED(IDC_CHECK_AUTODLOAD, &CSWDownloaderDlg::OnBnClickedCheckAutodload)
    ON_WM_RBUTTONDOWN()
END_MESSAGE_MAP()


// CSWDownloaderDlg message handlers

BOOL CSWDownloaderDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Initialization here
    CXMLFile::Init();
    CLog::Create(&m_pCLog);
    COMManager::Create(&m_pCOMMgr);
    CDloadConfig::Create(&m_pDloadConfig);
    CChipset::Create(&m_pChipset);
    m_bStartDLoad = FALSE;
    m_cAutoDLoadCheck.SetCheck(m_pDloadConfig->IsAutoDLoad());

    m_ArraySrc.RemoveAll();
    CString szName;
    for(int i=0; i<COM_MAX; i++)
    {
        szName.Format(_T("COM%d"),i+1);
        m_ArraySrc.Add(szName);
    }
    
    m_pChipset->RefreshChipsetList(m_cChipsetCombo,m_pDloadConfig->GetChipsetIdx());
    m_nChipset = m_cChipsetCombo.GetCurSel();
    m_pDLoadThread      = NULL;
    m_nThreadNum        = 0;
    m_nAutoPortID       = COM1;
    m_nAutoIndex        = 0;

    UpdateComboPath();
    InitListCtrl();
    m_cSettingButton.EnableWindow(!m_pDloadConfig->IsLocked());
    m_cChipsetCombo.EnableWindow(!m_pDloadConfig->IsLocked());
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSWDownloaderDlg::OnDestroy()
{
    CDialog::OnDestroy();
    
    for(int i=0;i<m_nThreadNum;i++)
    {
        m_pDLoadThread[i].Unregister(DloadStateCB,this);
        m_pDLoadThread[i].Stop();
    }
    delete [] m_pDLoadThread;

    m_pCOMMgr->UnregStateChangeCB(COM_ALL, NULL, this);
    COMManager::Release();
    CChipset::Release();
    CDloadConfig::Release();
    CLog::Release();
    CXMLFile::CleanUp();
}

void CSWDownloaderDlg::OnCancel()
{
    LOG_OUTPUT(_T("CSWDownloaderDlg::OnCancel %d\n"),m_bStartDLoad);
    if(m_bStartDLoad)
    {
        CString szMsg;
        szMsg.LoadString(IDS_ERR_CANNOTEXIT);
        MessageBox(szMsg);
        return;
    }
    CDialog::OnCancel();
}

void CSWDownloaderDlg::OnOK()
{
    LOG_OUTPUT(_T("CSWDownloaderDlg::OnOK %d\n"),m_bStartDLoad);
    if(m_bStartDLoad)
    {
        CString szMsg;
        szMsg.LoadString(IDS_ERR_CANNOTEXIT);
        MessageBox(szMsg);
        return;
    }
    OnBnClickedOk();
}

void CSWDownloaderDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSWDownloaderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSWDownloaderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CSWDownloaderDlg::UpdateComboPath(void)
{
    CStringArray &arMBNHist = m_pDloadConfig->GetMBNPathHist();
    m_cPathCombo.ResetContent();
    for(int i=0; i<arMBNHist.GetCount(); i++)
    {
        m_cPathCombo.InsertString(i, arMBNHist[i]);
    }
    m_cPathCombo.SetCurSel(0);
}

void CSWDownloaderDlg::InitListCtrl(void)
{
    CString szTemp;
    
    LOG_OUTPUT(_T("CSWDownloaderDlg::InitListCtrl\n"));

    m_cDloadListCtrl.SetExtendedStyle(m_cDloadListCtrl.GetExtendedStyle()|LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT|LVS_EX_FLATSB);

	szTemp.LoadString(IDS_COL_COM);
	m_cDloadListCtrl.InsertColumn(DLOADLISTCTRL_COL_COM,szTemp,LVCFMT_LEFT,DLOADLISTCTRL_WIDTH_COM);
    szTemp.LoadString(IDS_COL_MODE);
	m_cDloadListCtrl.InsertColumn(DLOADLISTCTRL_COL_MODE,szTemp,LVCFMT_CENTER,DLOADLISTCTRL_WIDTH_MODE);
    szTemp.LoadString(IDS_COL_STEP);
	m_cDloadListCtrl.InsertColumn(DLOADLISTCTRL_COL_STEP,szTemp,LVCFMT_CENTER,DLOADLISTCTRL_WIDTH_STEP);
	szTemp.LoadString(IDS_COL_PROGRESS);
	m_cDloadListCtrl.InsertColumn(DLOADLISTCTRL_COL_PROGRESS,szTemp,LVCFMT_CENTER,DLOADLISTCTRL_WIDTH_PROGRESS, COLUMN_STYLE_PROGRESS);
    szTemp.LoadString(IDS_COL_STATE);
	m_cDloadListCtrl.InsertColumn(DLOADLISTCTRL_COL_STATE,szTemp,LVCFMT_LEFT,DLOADLISTCTRL_WIDTH_STATE);
    m_cDloadListCtrl.DeleteAllItems();

    m_imgList.Create(1,20,TRUE|ILC_COLOR24,1,0);
	m_cDloadListCtrl.SetImageList(&m_imgList,LVSIL_SMALL);
    
    CFont *pFont = m_cDloadListCtrl.GetFont();
    CFont myFont;
    LOGFONT lf;
    pFont->GetLogFont(&lf);
    lf.lfHeight = 20;
    myFont.CreateFontIndirect(&lf);
    m_cDloadListCtrl.SetFont(&myFont);
    GetDlgItem(IDOK)->SetFont(&myFont);

    UpdateListCtrl();
}

void CSWDownloaderDlg::UpdateListCtrl(void)
{
    int i;
    CString szTemp;
	
    if(m_nThreadNum == m_pDloadConfig->GetCOMNum())
    {
        RedrawWindow();
        return;
    }
    
    LOG_OUTPUT(_T("CSWDownloaderDlg::UpdateListCtrl 0x%x %d %d\n"),m_pDLoadThread,m_nThreadNum,m_pDloadConfig->GetCOMNum());
    if(m_pDLoadThread)
    {
        for(int i=0;i<m_nThreadNum;i++)
        {
            m_pDLoadThread[i].Unregister(DloadStateCB,this);
            m_pDLoadThread[i].Stop();
        }
        delete [] m_pDLoadThread;
    }
    
    m_nThreadNum        = m_pDloadConfig->GetCOMNum();
    m_pDLoadThread      = new DLoadThread[m_nThreadNum];
    for(int i=0;i<m_nThreadNum;i++)
    {
        m_pDLoadThread[i].Register(DloadStateCB,this,i);
    }

    m_cDloadListCtrl.DeleteAllItems();
    m_pCOMMgr->UnregStateChangeCB(COM_ALL, NULL, this);
    
    if(m_pDloadConfig->GetCOMNum() == 1) 
    {
        // 进入自动模式
        szTemp.LoadString(IDS_AUTO);
        m_cDloadListCtrl.InsertItem(0, szTemp);
        m_pCOMMgr->RegStateChangeCB(COM_ALL,PhoneStateCB,this);
    }
    else
    {
	    for(i = 0; i < m_pDloadConfig->GetCOMNum(); i++)
        {
            if(m_pDloadConfig->GetCOMPort(i) >= COM_MAX)
            {
                szTemp.LoadString(IDS_NOCOM);
                m_cDloadListCtrl.InsertItem(i, szTemp);
            }
            else
            {
                if(m_pDloadConfig->GetCOMAvailNum() == 1)
                {
                    m_nAutoIndex = i;
                    szTemp.LoadString(IDS_AUTO);
                }
                else
                {
                    szTemp.Format(_T("COM%d"),m_pDloadConfig->GetCOMPort(i)+1);
                }
                m_cDloadListCtrl.InsertItem(i, szTemp);
                m_cDloadListCtrl.SetItemText(i,DLOADLISTCTRL_COL_MODE,GetModeString(m_pCOMMgr->GetPhoneMode(m_pDloadConfig->GetCOMPort(i))));
            }
        }

        // 控件初始化完成之后再注册回调，这样可以及时刷新串口状态
        for(i = 0; i < m_pDloadConfig->GetCOMNum(); i++)
        {
            if(m_pDloadConfig->GetCOMPort(i) < COM_MAX)
            {
                if(m_pDloadConfig->GetCOMAvailNum() == 1)
                {
                    m_pCOMMgr->RegStateChangeCB(COM_ALL,PhoneStateCB,this);
                }
                else
                {
                    m_pCOMMgr->RegStateChangeCB(m_pDloadConfig->GetCOMPort(i),PhoneStateCB,this);
                }
            }
        }
    }

    RedrawWindow();
}

void CSWDownloaderDlg::UpdateCOMString(void)
{
    int i;
    CString szTemp;
        
    for(i = 0; i < m_pDloadConfig->GetCOMNum(); i++)
    {
        if(m_pDloadConfig->GetCOMPort(i) >= COM_MAX)
        {
            szTemp.LoadString(IDS_NOCOM);
            if(szTemp !=m_cDloadListCtrl.GetItemText(i, DLOADLISTCTRL_COL_COM))
            {
                m_cDloadListCtrl.SetItemText(i,DLOADLISTCTRL_COL_COM, szTemp);
            }
        }
        else
        {
            if(m_pDloadConfig->GetCOMAvailNum() == 1)
            {
                m_nAutoIndex = i;
                szTemp.LoadString(IDS_AUTO);
            }
            else
            {
                szTemp.Format(_T("COM%d"),m_pDloadConfig->GetCOMPort(i)+1);
            }
            
            if(szTemp != m_cDloadListCtrl.GetItemText(i, DLOADLISTCTRL_COL_COM))
            {
                m_cDloadListCtrl.SetItemText(i,DLOADLISTCTRL_COL_COM, szTemp);
            }
        }
    }
    RedrawWindow();
}

CString CSWDownloaderDlg::GetModeString(int nMode)
{
    CString szMode;
    switch(nMode){
    case DIAG_PHONEMODE_CDMA:
    case DIAG_PHONEMODE_WCDMA:
        szMode.LoadString(IDS_COM_CONNECT);
        break;
    
    case DIAG_PHONEMODE_DLOAD:
    case DIAG_PHONEMODE_EDLOAD:
        szMode.LoadString(IDS_COM_DLOAD);
        break;
    
    case DIAG_PHONEMODE_ARMPRG:
        szMode.LoadString(IDS_COM_ARMPRG);
        break;

    case DIAG_PHONEMODE_FASTBOOT:
        szMode.LoadString(IDS_COM_FASTBOOT);
        break;

    case DIAG_PHONEMODE_NONE:
    default:
        szMode.LoadString(IDS_COM_NOPHONE);
        break;
    }
    return szMode;
}

int CSWDownloaderDlg::GetStepIDS(int nStep)
{
    switch(nStep){
    case DLOAD_STEP_COMCONNECT: return IDS_STEP_COMCONNECT;
    case DLOAD_STEP_NVBACKUP:   return IDS_STEP_NVBACKUP;
    case DLOAD_STEP_EFSBACKUP:  return IDS_STEP_EFSBACKUP;
    case DLOAD_STEP_DLOADMODE:  return IDS_STEP_DLOADMODE;
    case DLOAD_STEP_DLOADEMMC:  return IDS_STEP_DLOADINGMBN;
    case DLOAD_STEP_RUNARMPRG:  return IDS_STEP_RUNARMPRG;
    case DLOAD_STEP_DLOADINGMBN:return IDS_STEP_DLOADINGMBN;
    case DLOAD_STEP_DLOADINGARD:return IDS_STEP_DLOADINGARD;
    case DLOAD_STEP_EFSRESTORE: return IDS_STEP_EFSRESTORE;
    case DLOAD_STEP_NVRESTORE:  return IDS_STEP_NVRESTORE;
    case DLOAD_STEP_RESET:      return IDS_STEP_RESET;
    case DLOAD_STEP_EXIT:       return IDS_STEP_EXIT;
    default:
        break;
    }
    return IDS_STEP_INIT;
}

int CSWDownloaderDlg::GetStatusIDS(int nStatus)
{
    switch(nStatus){
    case DLOAD_STATUS_FAILED:           return IDS_STATUS_FAILED;
    case DLOAD_STATUS_COMTIMEOUT:       return IDS_STATUS_COMTIMEOUT;
    case DLOAD_STATUS_FAILEDTODLOAD:    return IDS_STATUS_FAILEDTODLOAD;
    case DLOAD_STATUS_FAILEDTORUNARMPRG:return IDS_STATUS_FAILEDTORUNARMPRG;
    case DLOAD_STATUS_FAILEDTORESET:    return IDS_STATUS_FAILEDTORESET;
    case DLOAD_STATUS_INVAILDNVPATH:    return IDS_STATUS_INVAILDNVPATH;
    case DLOAD_STATUS_INVAILDEFSPATH:   return IDS_STATUS_INVAILDEFSPATH;
    case DLOAD_STATUS_INVAILDMBNPATH:   return IDS_STATUS_INVAILDMBNPATH;
    case DLOAD_STATUS_CANNOTOPENFILE:   return IDS_STATUS_CANNOTOPENFILE;
    case DLOAD_STATUS_NVBACKUP:         return IDS_STATUS_NVBACKUP;
    case DLOAD_STATUS_EFSBACKUP:        return IDS_STATUS_EFSBACKUP;
    case DLOAD_STATUS_NVRESTORE:        return IDS_STATUS_NVRESTORE;
    case DLOAD_STATUS_EFSRESTORE:       return IDS_STATUS_EFSRESTORE;
    case DLOAD_STATUS_USERCANCEL:       return IDS_STATUS_USERCANCEL;
    default:
        break;
    }
    return IDS_STATUS_SUCCESS;
}

int CSWDownloaderDlg::GetPortIDFromString(const CString &szCOMID)
{
    int nPortID;
    CString szTemp;
    szTemp.LoadString(IDS_NOCOM);

    if(szTemp == szCOMID || szCOMID.IsEmpty())
    {
        return COM_MAX;
    }
    
    const TCHAR *pStr = szCOMID;
    nPortID = _tcstol(pStr+3,NULL,10)-1;
    if(nPortID<0)
    {
        nPortID = COM_MAX;
    }
    return nPortID;
}

void CSWDownloaderDlg::PhoneStateChange(int nPortID, int nNewState)
{
    if(m_pDloadConfig->GetCOMNum() > 1)
    {
        int nItem = m_pDloadConfig->GetCOMIdxByID(nPortID);
        if(nItem < m_pDloadConfig->GetCOMNum())
        {
            m_cDloadListCtrl.SetItemText(nItem,DLOADLISTCTRL_COL_MODE,GetModeString(m_pCOMMgr->GetPhoneMode(nPortID)));
        }
    }

    switch(nNewState){
    case DIAG_PHONEMODE_CDMA:
    case DIAG_PHONEMODE_WCDMA:
    case DIAG_PHONEMODE_DLOAD:
    case DIAG_PHONEMODE_EDLOAD:
    case DIAG_PHONEMODE_ARMPRG:
    case DIAG_PHONEMODE_FASTBOOT:
        LOG_OUTPUT(_T("CSWDownloaderDlg::PhoneStateChange COM%d %d\n"),nPortID+1,nNewState);
        NewPhoneConnected(nPortID);
        break;

    case DIAG_PHONEMODE_NOPORT:
    case DIAG_PHONEMODE_NONE:
        PhoneDisConnected(nPortID);
        break;
    default:
        break;
    }
}

void CSWDownloaderDlg::PhoneStateCB(LPVOID pParam, int nPortID, int nNewState)
{
    CSWDownloaderDlg *pThis = (CSWDownloaderDlg *)pParam;
    LOG_OUTPUT(_T("CSWDownloaderDlg::PhoneStateCB COM%d %d AUTO=COM%d\n"),nPortID+1,nNewState,pThis->m_nAutoPortID+1);
    pThis->PostMessage(WM_MY_UPDATESTATE, (WPARAM)nPortID, (LPARAM)nNewState);
}

LRESULT CSWDownloaderDlg::OnMyUpdateState(WPARAM wParam, LPARAM lParam)
{
    PhoneStateChange((int)wParam,(int)lParam);
    return 0;
}

void CSWDownloaderDlg::DLoadStateChange(int nIndex)
{
    CString szTemp;
    if(nIndex >= m_pDloadConfig->GetCOMNum())
    {
        return;
    }
    
    if(m_pDLoadThread[nIndex].GetStep() == DLOAD_STEP_EXIT)
    {
        CString szTimeMS;
        DWORD dwMinutes,dwSeconds,dwMS;
        dwMS = m_pDLoadThread[nIndex].GetWorkTimeMS();
        dwSeconds   = dwMS/1000;
        dwMinutes   = dwSeconds/60;
        dwSeconds   = dwSeconds%60;
        dwMS        = dwMS%1000;
        szTimeMS.Format(_T("-%02d:%02d"),dwMinutes,dwSeconds);
        szTemp.LoadString(GetStepIDS(m_pDLoadThread[nIndex].GetStep()));
        szTemp += szTimeMS;
        m_cDloadListCtrl.SetItemText(nIndex, DLOADLISTCTRL_COL_STEP, szTemp);
        
        // 下载完成
        if(m_pDLoadThread[nIndex].GetError() == DLOAD_STATUS_SUCCESS)
        {
            // 下载成功
            szTemp.LoadString(IDS_SUCCESS);
            m_cDloadListCtrl.SetItemText(nIndex, DLOADLISTCTRL_COL_PROGRESS, szTemp);
        }
        else
        {
            // 下载失败
            szTemp.LoadString(IDS_FAILED);
            m_cDloadListCtrl.SetItemText(nIndex, DLOADLISTCTRL_COL_PROGRESS, szTemp);
        }
        szTemp.LoadString(GetStatusIDS(m_pDLoadThread[nIndex].GetError()));
        m_cDloadListCtrl.SetItemText(nIndex, DLOADLISTCTRL_COL_STATE, szTemp+_T("  ")+m_pDLoadThread[nIndex].GetString());
        if(FALSE == m_pDloadConfig->IsAutoDLoad())
        {
            int nDLoading = 0;
            for(int i=0;i<m_pDloadConfig->GetCOMNum();i++)
            {
                nDLoading += m_pDLoadThread[i].IsBusy();
            }

            if(nDLoading == 0)
            {
                StopDownload();
            }
        }
    }
    else
    {
        szTemp.LoadString(GetStepIDS(m_pDLoadThread[nIndex].GetStep()));
        m_cDloadListCtrl.SetItemText(nIndex, DLOADLISTCTRL_COL_STEP, szTemp);
        m_cDloadListCtrl.SetItemText(nIndex, DLOADLISTCTRL_COL_STATE, m_pDLoadThread[nIndex].GetString());
        if(m_pDLoadThread[nIndex].GetTotal()>0)
        {
            szTemp.Format(_T("%d/%d"),m_pDLoadThread[nIndex].GetCurr(),m_pDLoadThread[nIndex].GetTotal());
            m_cDloadListCtrl.SetItemText(nIndex, DLOADLISTCTRL_COL_PROGRESS, szTemp);
        }
        else
        {
            szTemp.Empty();
            m_cDloadListCtrl.SetItemText(nIndex, DLOADLISTCTRL_COL_PROGRESS, szTemp);
        }
    }
}

LRESULT CSWDownloaderDlg::OnMyUpdateInfo(WPARAM wParam, LPARAM lParam)
{
    int nIndex = (int)lParam;
    DLoadStateChange(nIndex);
    return 0;
}

void CSWDownloaderDlg::DloadStateCB(LPVOID pParam, int nIndex)
{
    CSWDownloaderDlg *pThis = (CSWDownloaderDlg *)pParam;
    pThis->PostMessage(WM_MY_UPDATEINFO, 0, (LPARAM)nIndex);
}

void CSWDownloaderDlg::NewPhoneConnected(int nPortID)
{
    m_nAutoPortID = nPortID;
    if(m_pDloadConfig->GetCOMNum() == 1)
    {
        m_cDloadListCtrl.SetItemText(0,DLOADLISTCTRL_COL_MODE,GetModeString(m_pCOMMgr->GetPhoneMode(nPortID)));
        LOG_OUTPUT(_T("NewPhoneConnected COM%d from COM%d %d %d\n"),nPortID+1,m_pDLoadThread[0].GetPortID()+1,m_bStartDLoad,m_pDLoadThread[0].IsBusy());
        if(m_bStartDLoad && !m_pDLoadThread[0].IsBusy())
        {
            if(m_pDloadConfig->IsAutoDLoad())
            {
                LOG_OUTPUT(_T("Auto Download COM%d\n"),nPortID+1);
                m_pDLoadThread[0].Start(nPortID);
            }
        }
        else if(m_bStartDLoad && m_pDLoadThread[0].IsBusy() && nPortID != m_pDLoadThread[0].GetPortID())
        {
            LOG_OUTPUT(_T("SwitchPort COM%d from COM%d\n"),nPortID+1,m_pDLoadThread[0].GetPortID()+1);
            m_pDLoadThread[0].SwitchPort(nPortID);
        }
    }
    else
    {
        if(m_pDloadConfig->GetCOMAvailNum() == 1)
        {
            m_cDloadListCtrl.SetItemText(m_nAutoIndex,DLOADLISTCTRL_COL_MODE,GetModeString(m_pCOMMgr->GetPhoneMode(nPortID)));
            LOG_OUTPUT(_T("NewPhoneConnected COM%d from COM%d %d %d\n"),nPortID+1,m_pDLoadThread[m_nAutoIndex].GetPortID()+1,m_bStartDLoad,m_pDLoadThread[m_nAutoIndex].IsBusy());
            if(m_bStartDLoad && !m_pDLoadThread[m_nAutoIndex].IsBusy())
            {
                if(m_pDloadConfig->IsAutoDLoad())
                {
                    LOG_OUTPUT(_T("Auto Download COM%d\n"),nPortID+1);
                    m_pDLoadThread[m_nAutoIndex].Start(nPortID);
                }
            }
            else if(m_bStartDLoad && m_pDLoadThread[m_nAutoIndex].IsBusy() && nPortID != m_pDLoadThread[m_nAutoIndex].GetPortID())
            {
                LOG_OUTPUT(_T("SwitchPort COM%d from COM%d\n"),nPortID+1,m_pDLoadThread[m_nAutoIndex].GetPortID()+1);
                m_pDLoadThread[m_nAutoIndex].SwitchPort(nPortID);
            }
        }
        else
        {
            int nIndex = m_pDloadConfig->GetCOMIdxByID(nPortID);
            if(nIndex > m_pDloadConfig->GetCOMNum())
            {
                return;
            }
            if(m_bStartDLoad && !m_pDLoadThread[nIndex].IsBusy())
            {
                if(m_pDloadConfig->IsAutoDLoad())
                {
                    LOG_OUTPUT(_T("Auto Download COM%d\n"),nPortID+1);
                    m_pDLoadThread[nIndex].Start(nPortID);
                }
            }
        }
    }
}

void CSWDownloaderDlg::PhoneDisConnected(int nPortID)
{
    if(m_pDloadConfig->GetCOMNum() == 1 && m_nAutoPortID == nPortID)
    {
        m_cDloadListCtrl.SetItemText(0,DLOADLISTCTRL_COL_MODE,GetModeString(m_pCOMMgr->GetPhoneMode(nPortID)));
    }
    else if(m_pDloadConfig->GetCOMAvailNum() == 1 && m_nAutoPortID == nPortID)
    {
        m_cDloadListCtrl.SetItemText(m_nAutoIndex,DLOADLISTCTRL_COL_MODE,GetModeString(m_pCOMMgr->GetPhoneMode(nPortID)));
    }
}

void CSWDownloaderDlg::StartDownload(void)
{
    CString szTemp;
    int i;
    
    LOG_OUTPUT(_T("CSWDownloaderDlg::StartDownload\n"));
    GetDlgItem(IDC_COMBO_PATH)->EnableWindow(FALSE);
    GetDlgItem(IDC_BUTTON_BROWSE)->EnableWindow(FALSE);
    GetDlgItem(IDC_COMBO_CHIPSET)->EnableWindow(FALSE);
    GetDlgItem(IDC_BUTTON_SETTING)->EnableWindow(FALSE);
    //GetDlgItem(IDCANCEL)->EnableWindow(!m_bStartDLoad);
    
    if(m_pDloadConfig->GetCOMNum() == 1)
    {
        m_pDLoadThread[0].Start(m_nAutoPortID);
    }
    else
    {
        for(i=0;i<m_pDloadConfig->GetCOMNum();i++)
        {
            if(m_pDloadConfig->GetCOMPort(i) < COM_MAX)
            {
                if(m_pDloadConfig->GetCOMAvailNum() == 1)
                {
                    m_pDLoadThread[i].Start(m_nAutoPortID);
                }
                else
                {
                    m_pDLoadThread[i].Start(m_pDloadConfig->GetCOMPort(i));
                }
            }
        }
    }
    szTemp.LoadString(IDS_CANCEL);
    GetDlgItem(IDOK)->SetWindowText(szTemp);
    m_bStartDLoad = TRUE;
}

void CSWDownloaderDlg::StopDownload(void)
{
    CString szTemp;
    int i;
    
    LOG_OUTPUT(_T("CSWDownloaderDlg::StopDownload\n"));
    GetDlgItem(IDC_COMBO_PATH)->EnableWindow(TRUE);
    GetDlgItem(IDC_BUTTON_BROWSE)->EnableWindow(TRUE);
    GetDlgItem(IDC_COMBO_CHIPSET)->EnableWindow(!m_pDloadConfig->IsLocked());
    GetDlgItem(IDC_BUTTON_SETTING)->EnableWindow(!m_pDloadConfig->IsLocked());
    //GetDlgItem(IDCANCEL)->EnableWindow(TRUE);
    
    for(i=0;i<m_pDloadConfig->GetCOMNum();i++)
    {
        m_pDLoadThread[i].Stop();
    }
    szTemp.LoadString(IDS_START);
    GetDlgItem(IDOK)->SetWindowText(szTemp);
    m_bStartDLoad = FALSE;
}

void CSWDownloaderDlg::OnBnClickedOk()
{
    if(m_bStartDLoad)
    {
        StopDownload();
    }
    else
    {
        StartDownload();
    }
}

void CSWDownloaderDlg::OnBnClickedCancel()
{
    // TODO: 在此添加控件通知处理程序代码
    OnCancel();
}

void CSWDownloaderDlg::OnTimer(UINT_PTR nIDEvent)
{
    // TODO: 在此添加消息处理程序代码和/或调用默认值
    CDialog::OnTimer(nIDEvent);
}

void CSWDownloaderDlg::OnBnClickedButtonBrowse()
{
    CString     szTemp;
    CString     szPath = m_pDloadConfig->GetMBNPath();
    szTemp.LoadString(IDS_TITLE_DLOADPATH);
    
    if(szPath.IsEmpty())
    {
        TCHAR myDir[MAX_PATH+1];
        GetCurrentDirectory(MAX_PATH,myDir);
        szPath = myDir;
    }

    CFolderDialog myFolderDlg(CSIDL_DESKTOP, szPath, szTemp, BIF_RETURNFSANCESTORS | BIF_RETURNONLYFSDIRS);
    if(myFolderDlg.DoModal() == IDOK)
    {
        m_pDloadConfig->SetMBNPath(myFolderDlg.GetPath());
        UpdateComboPath();
    }
}

void CSWDownloaderDlg::OnCbnCloseupComboChipset()
{
    if(m_nChipset != m_cChipsetCombo.GetCurSel())
    {
        LOG_OUTPUT(_T("CSWDownloaderDlg::OnCbnCloseupComboChipset %d\n"),m_cChipsetCombo.GetCurSel());
        m_nChipset = m_cChipsetCombo.GetCurSel();
        m_pDloadConfig->SetChipsetIdx(m_nChipset);
        UpdateListCtrl();
    }
}

void CSWDownloaderDlg::OnLvnItemchangedListDload(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
    // TODO: 在此添加控件通知处理程序代码
    *pResult = 0;

    if(pNMLV->uChanged & LVIF_TEXT)
    {
        int nItem = pNMLV->iItem;
        int nSubItem = pNMLV->iSubItem;
        
        if(nSubItem == DLOADLISTCTRL_COL_COM && m_pDloadConfig->GetCOMNum() > 0)
        {
            CString szTemp;
            szTemp.LoadString(IDS_AUTO);
            if(szTemp == m_cDloadListCtrl.GetItemText(nItem, nSubItem))
            {
                m_pCOMMgr->UnregStateChangeCB(m_pDloadConfig->GetCOMPort(nItem),PhoneStateCB,this);
                m_pCOMMgr->RegStateChangeCB(COM_ALL,PhoneStateCB,this);
                return;
            }

            int nPortID = GetPortIDFromString(m_cDloadListCtrl.GetItemText(nItem, nSubItem));
            if(nPortID != COM_MAX)
            {
                for(int i=0;i<m_pDloadConfig->GetCOMNum();i++)
                {
                    if(nItem != i)
                    {
                        if(m_pCOMMgr->IsPortSame(m_pDloadConfig->GetCOMPort(i) , nPortID))
                        {
                            nPortID = COM_MAX;
                            break;
                        }
                    }
                }
            }

            m_pCOMMgr->UnregStateChangeCB(m_pDloadConfig->GetCOMPort(nItem),PhoneStateCB,this);
            m_pDloadConfig->SetCOMPort(nItem,nPortID);
            if(m_pDloadConfig->GetCOMAvailNum() == 1)
            {
                m_pCOMMgr->RegStateChangeCB(COM_ALL,PhoneStateCB,this);
            }
            else
            {
                m_pCOMMgr->RegStateChangeCB(nPortID,PhoneStateCB,this);
            }
            UpdateCOMString();
            if(nPortID >= COM_MAX)
            {
                m_cDloadListCtrl.SetItemText(nItem,DLOADLISTCTRL_COL_MODE,_T(""));
            }
            else
            {
                m_cDloadListCtrl.SetItemText(nItem,DLOADLISTCTRL_COL_MODE,GetModeString(m_pCOMMgr->GetPhoneMode(nPortID)));
            }
            m_cDloadListCtrl.SetItemText(nItem,DLOADLISTCTRL_COL_STEP,_T(""));
            m_cDloadListCtrl.SetItemText(nItem,DLOADLISTCTRL_COL_PROGRESS,_T(""));
            m_cDloadListCtrl.SetItemText(nItem,DLOADLISTCTRL_COL_STATE,_T(""));
        }
    }
}

void CSWDownloaderDlg::OnNMClickListDload(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    // TODO: 在此添加控件通知处理程序代码
    *pResult = 0;

    int nItem = pNMItemActivate->iItem;
    int nSubItem = pNMItemActivate->iSubItem;
    
    if(nSubItem == DLOADLISTCTRL_COL_COM && m_pDloadConfig->GetCOMNum() > 0)
    {
        if(m_bStartDLoad || m_pDloadConfig->GetCOMNum() == 1)
        {
            m_cDloadListCtrl.UpdateColumn(DLOADLISTCTRL_COL_COM, COLUMN_STYLE_COMBOBOX, NULL);
        }
        else
        {
            int nPortID = GetPortIDFromString(m_cDloadListCtrl.GetItemText(nItem, nSubItem));
            if(nPortID < COM_MAX)
            {
                m_cDloadListCtrl.UpdateColumn(DLOADLISTCTRL_COL_COM, COLUMN_STYLE_COMBOBOX, &m_ArraySrc, nPortID);
            }
            else
            {
                m_cDloadListCtrl.UpdateColumn(DLOADLISTCTRL_COL_COM, COLUMN_STYLE_COMBOBOX, &m_ArraySrc);
            }
        }
    }
}

void CSWDownloaderDlg::OnCbnCloseupComboPath()
{
    CString szText;
    m_cPathCombo.GetWindowText(szText);
    m_pDloadConfig->SetMBNPath(szText);
}

void CSWDownloaderDlg::OnCbnDropdownComboPath()
{
    UpdateComboPath();
}

void CSWDownloaderDlg::OnBnClickedButtonSetting()
{
    LOG_OUTPUT(_T("CSWDownloaderDlg::OnBnClickedButtonSetting\n"));
    CDloadSettingDlg mySettingDlg(this);
    mySettingDlg.DoModal();
    UpdateListCtrl();
    m_cSettingButton.EnableWindow(!m_pDloadConfig->IsLocked());
    m_cChipsetCombo.EnableWindow(!m_pDloadConfig->IsLocked());
}

void CSWDownloaderDlg::OnNMRClickListDload(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
    // TODO: 在此添加控件通知处理程序代码
    *pResult = 0;
    OnBnClickedButtonComstate();
}

void CSWDownloaderDlg::OnBnClickedButtonComstate()
{
    // TODO: 在此添加控件通知处理程序代码
    CPortInfoDlg myPortDlg;
    myPortDlg.DoModal();
}

void CSWDownloaderDlg::OnBnClickedCheckAutodload()
{
    m_pDloadConfig->SetAutoDLoad(m_cAutoDLoadCheck.GetCheck());
}

void CSWDownloaderDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
    OnBnClickedButtonComstate();

    CDialog::OnRButtonDown(nFlags, point);
}
