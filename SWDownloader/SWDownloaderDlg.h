// SWDownloaderDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "Diagnostic.h"
#include "DLoadThread.h"
#include "COMManager.h"
#include "CChipset.h"
#include "DloadConfig.h"
#include "CListCtrlEx.h"
#include "Log.h"
#include "fastboot.h"

#define DLOADLISTCTRL_COL_COM           0
#define DLOADLISTCTRL_COL_MODE          1
#define DLOADLISTCTRL_COL_STEP          2
#define DLOADLISTCTRL_COL_PROGRESS      3
#define DLOADLISTCTRL_COL_STATE         4

#define DLOADLISTCTRL_WIDTH_COM         80
#define DLOADLISTCTRL_WIDTH_MODE        128
#define DLOADLISTCTRL_WIDTH_STEP        128
#define DLOADLISTCTRL_WIDTH_PROGRESS    300
#define DLOADLISTCTRL_WIDTH_STATE       800

// CSWDownloaderDlg dialog
class CSWDownloaderDlg : public CDialog
{
// Construction
public:
	CSWDownloaderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_SWDOWNLOADER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
    virtual void OnCancel();
    virtual void OnOK();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

protected:
    COMManager     *m_pCOMMgr;
    DLoadThread    *m_pDLoadThread;
    CChipset       *m_pChipset;
    CDloadConfig   *m_pDloadConfig;
    CLog           *m_pCLog;
    int             m_nChipset;
    CStringArray    m_ArraySrc;
    CImageList      m_imgList;
    BOOL            m_bStartDLoad;
    int             m_nThreadNum;
    int             m_nAutoPortID;
    int             m_nAutoIndex;
    
protected:
    void            PhoneStateChange(int nPortID, int nNewState);
    void            DLoadStateChange(int nIndex);
    void            NewPhoneConnected(int nPortID);
    void            PhoneDisConnected(int nPortID);
    void            StartDownload(void);
    void            StopDownload(void);
    void            InitListCtrl(void);
    void            UpdateListCtrl(void);
    void            UpdateCOMString(void);
    void            UpdateComboPath(void);
    CString         GetModeString(int nMode);
    int             GetPortIDFromString(const CString &szCOMID);
    int             GetStepIDS(int nStep);
    int             GetStatusIDS(int nStatus);

    static void     PhoneStateCB(LPVOID pParam, int nPortID, int nNewState);
    static void     DloadStateCB(LPVOID pParam, int nIndex);
    
public:
    CComboBox       m_cPathCombo;
    CButton         m_cBrowseButton;
    CComboBox       m_cChipsetCombo;
    CButton         m_cOKButton;
    CButton         m_cCancelButton;
    CListCtrlEx     m_cDloadListCtrl;
    CButton         m_cAutoDLoadCheck;
    CButton         m_cSettingButton;
    
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    afx_msg void OnDestroy();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnBnClickedButtonBrowse();
    afx_msg void OnCbnCloseupComboChipset();
    afx_msg void OnLvnItemchangedListDload(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnNMClickListDload(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnCbnCloseupComboPath();
    afx_msg void OnCbnDropdownComboPath();
    afx_msg void OnBnClickedButtonSetting();
    afx_msg void OnNMRClickListDload(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnBnClickedButtonComstate();
    afx_msg void OnBnClickedCheckAutodload();
    afx_msg LRESULT OnMyUpdateInfo(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnMyUpdateState(WPARAM wParam, LPARAM lParam);
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);

    CFastboot   m_cFastboot;
};
