#pragma once
#include "Diagnostic.h"
#include "HexFile.h"
#include "DloadConfig.h"
#include "COMManager.h"
#include "fastboot.h"
#include "USBPorts.h"
#include "CXMLFile.h"

#define DEFAULT_SECTOR_SIZE         512
#define WIRTE_BUFFER_LEN            (2048*DEFAULT_SECTOR_SIZE)
#define DLOAD_POLL_COM_TIME         1500
#define DLOAD_WAIT_TIMEOUT          (90*1000)
#define MOVE_TO_STEP(s)             {m_nPrevStep = m_nCurrStep; m_nCurrStep = s;m_nStepRet=DLOAD_STEPRET_INIT;}
#define STEP_RET(r)                 {m_nStepRet=r;}

enum {
    DLOAD_STATUS_SUCCESS = 0,
    DLOAD_STATUS_FAILED,
    DLOAD_STATUS_COMTIMEOUT,
    DLOAD_STATUS_FAILEDTODLOAD,
    DLOAD_STATUS_FAILEDTORUNARMPRG,
    DLOAD_STATUS_FAILEDTORESET,
    DLOAD_STATUS_INVAILDNVPATH,
    DLOAD_STATUS_INVAILDEFSPATH,
    DLOAD_STATUS_INVAILDMBNPATH,
    DLOAD_STATUS_CANNOTOPENFILE,
    DLOAD_STATUS_NVBACKUP,
    DLOAD_STATUS_EFSBACKUP,
    DLOAD_STATUS_NVRESTORE,
    DLOAD_STATUS_EFSRESTORE,
    DLOAD_STATUS_USERCANCEL,
    DLOAD_STATUS_MAX
};

enum {
    DLOAD_STEP_INIT = 0,
    DLOAD_STEP_COMCONNECT,
    DLOAD_STEP_NVBACKUP,
    DLOAD_STEP_EFSBACKUP,
    DLOAD_STEP_DLOADEMMC,
    DLOAD_STEP_DLOADMODE,
    DLOAD_STEP_RUNARMPRG,
    DLOAD_STEP_DLOADINGMBN,
    DLOAD_STEP_DLOADINGARD,
    DLOAD_STEP_EFSRESTORE,
    DLOAD_STEP_NVRESTORE,
    DLOAD_STEP_RESET,
    DLOAD_STEP_EXIT,
    DLOAD_STEP_MAX,
};

enum {
    DLOAD_STEPRET_INIT = 0,
    DLOAD_STEPRET_RETRY,
    DLOAD_STEPRET_MAX,
};

typedef void (*PFNPROGRESS)(LPVOID pParam, int nIndex);

class DLoadThread
{
public:
    DLoadThread(void);
    ~DLoadThread(void);
    
    BOOL            Start(int nPort);
    BOOL            Stop(void);
    void            Register(PFNPROGRESS pfnProgress, LPVOID pParam, int nIndex)    {m_pfnProgress = pfnProgress; m_pUserParam = pParam; m_nIndex = nIndex;};
    void            Unregister(PFNPROGRESS pfnProgress, LPVOID pParam)              {m_pfnProgress = NULL;};
    BOOL            IsBusy(void)							                        {return m_bRunning;};
    void            RunFSM(void);
    DWORD           GetCurr(void)                                                   {return m_Curr;};
    DWORD           GetTotal(void)                                                  {return m_Total;};
    int             GetStep(void)                                                   {return m_nCurrStep;};
    int             GetError(void)                                                  {return m_nStatus;};
    int             GetPortID(void)                                                 {return m_nPort;};
    CString        &GetString(void)                                                 {return m_szProgName;};
    DWORD           GetWorkTimeMS(void)                                             {return m_dwWorkTimeMS;};
    void            SetCurr(DWORD dwCurr);
    void            SetTotal(DWORD dwTotal)                                         {m_Total= dwTotal; m_Curr = 0;};
    void            WaitMS(UINT nWait);
    void            SwitchPort(int nPort);

private:
    static UINT     DLoadThreadProc(LPVOID lpParam);
    void            ResetDLoadThread(void)    { m_pThread = NULL;}

    // Step Functions
    BOOL            ProcessStep(int nStep);
    BOOL            StepInit(void);
    BOOL            StepCOMConnect(void);
    BOOL            StepNVBackup(void);
    BOOL            StepEFSBackup(void);
    BOOL            StepDLoadMode(void);
    BOOL            StepDLoadEMMC(void);
    BOOL            StepRunARMPRG(void);
    BOOL            StepDloadMBN(void);
    BOOL            StepDloadARD(void);
    BOOL            StepEFSRestore(void);
    BOOL            StepNVRestore(void);
    BOOL            StepReset(void);
    BOOL            StepExit(void);

    // Message Functions
    void            SendProgressMsg(void);
    void            SendCompleteMsg(void);

    // Wait Functions
    BOOL            WaitMsg(UINT nWait = DLOAD_POLL_COM_TIME,UINT nTimeOut = DLOAD_WAIT_TIMEOUT);

    // Feature Functions
    BOOL            NVBackup(CString &szPath);
    BOOL            EFSBackup(CString &szPath);
    BOOL            DLoadARMPRG(void);
    BOOL            DLoadEARMPRG(void);
    BOOL            DloadMBN(void);
    BOOL            DLoadEMMCImages(void);
    BOOL            DLoadEMMCImage(HANDLE hDev, CString &szName, DWORD num_partition_sectors, DWORD start_sector, DWORD file_sector_offset = 0);
    BOOL            DLoadEMMCPatch(HANDLE hDev, DWORD start_sector, DWORD byte_offset, DWORD size_in_bytes, BYTE *value);
    BOOL            DLoadEMMCZeroout(HANDLE hDev, CString &szName, DWORD start_sector, DWORD num_partition_sectors);
    BOOL            DLoadEMMCSaveImages(HANDLE hDev, CString &szSuffix, DWORD NUM_DISK_SECTORS);
    BOOL            DLoadEMMCSave(HANDLE hDev, CString &szName, DWORD start_sector, int32 nSize);
    BOOL            DLoadFindEMMC(DWORD nHubIndex, DWORD nPortIndex);
    BOOL            DLoadOneMBN(CString &szName, DWORD dwOffset);
    BOOL            DLoadMultiMBN(void);
    BOOL            DLoadOneMultiMBN(CString &szName, open_multi_mode_type mMode, UINT wMaxLen);
    BOOL            DLoadEraseOneMultiMBN(CString &szName, open_multi_mode_type mMode, UINT wMaxLen);
    BOOL            DloadARD(void);
    BOOL            DloadFastBoot(CString &szCmd);
    BOOL            EFSRestore(CString &szPath);
    BOOL            NVRestore(void);
    BOOL            SyncFileToPhone(CString &szName);
    BOOL            SyncDirectoryToPhone(CString &szPath);
    void            SyncMBNPath(void);
    CString        &GetMBNPath(void)    {if(m_szMBNPath.IsEmpty()) return m_pDloadConfig->GetMBNPath();else return m_szMBNPath;};
    BOOL            MemDump(void);
#if 0
    CString         GetQCSBLHDCFGName(CString &szOrigName, CString &szPath);
#endif
    HANDLE          OpenDisk(LPCTSTR filename);
    void            CloseDisk(HANDLE hDisk);
    BOOL            GetDisksProperty(HANDLE hDisk, PSTORAGE_DEVICE_DESCRIPTOR pDevDesc);
    BOOL            GetDiskGeometry(HANDLE hDisk, PDISK_GEOMETRY_EX lpGeometryEx, DWORD dwSize);
    BOOL            LockVolume(HANDLE hDisk);
    BOOL            UnlockVolume(HANDLE hDisk);
    int32           StringToInt(string &szExpre, char *szReplace = NULL, int32 nReplace = 0);

private:
    // Callback
    PFNPROGRESS     m_pfnProgress;
    LPVOID          m_pUserParam;
    int             m_nIndex;

    // FSM status
    int             m_nCurrStep;
    int             m_nPrevStep;
    int             m_nNextStep;
    int             m_nStepRet;
    
    // Common
    int             m_nStatus;
    int             m_nPort;
    UINT            m_nWaitTime;
    BOOL            m_bRunning;
    BOOL            m_bTimeOut;
    BOOL            m_bUserCancel;
    BOOL            m_bResetting;
    BOOL            m_bNVRestore;
    BOOL            m_bEFSRestore;
    HANDLE          m_WaitEvent;
    CWinThread	   *m_pThread;
    CDiagnostic     m_PhoneDiag;
    CDloadConfig   *m_pDloadConfig;
    COMManager     *m_pCOMMgr;
    DWORD           m_Curr;
    DWORD           m_Total;
    CString         m_szProgName;   
    CString         m_szNVAuto;
    DWORD           m_dwStartTimeMS;
    DWORD           m_dwWorkTimeMS;
    CString         m_szEFSRoot;
    CString         m_szFlashHD;
    CString         m_szVersion;
    CString         m_szMBNPath;
    BOOL            m_bNeedReset;
    BOOL            m_bUnframedSupport;
    BOOL            m_bWaitReset;
    BOOL            m_bDLoadedARMPrg;
    int             m_nSwitchPort;
    CFastboot       m_Fastboot;
    CUSBPorts       m_UsbPorts;
    CString         m_EMMCPath;
    BYTE            m_bData[WIRTE_BUFFER_LEN];
    DWORD           m_dwSectorSize;
    
    // Hello Param
    diag_ap_hello_rsp_type aphello;
};


