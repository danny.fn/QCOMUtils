// DloadSettingDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "SWDownloader.h"
#include "DloadSettingDlg.h"
#include "CFolderDialog.h"

// CDloadSettingDlg 对话框

IMPLEMENT_DYNAMIC(CDloadSettingDlg, CDialog)

CDloadSettingDlg::CDloadSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDloadSettingDlg::IDD, pParent)
{
    CDloadConfig::Create(&m_pDloadConfig);
}

CDloadSettingDlg::~CDloadSettingDlg()
{
    CDloadConfig::Release();
}

BOOL  CDloadSettingDlg::OnInitDialog()
{
    CDialog::OnInitDialog();
    CString szTitle;
    szTitle.LoadString(IDS_SETTINGS);
    SetWindowText(szTitle);
    SetTimer(CDLOADSETTING_REFRESH_TIMER,50,NULL);
    UpdateConfig();

    CString szCode;
    szCode.Format(_T("%x"),m_pDloadConfig->GetStartAddr());
    m_cAddrEdit.SetLimitText(8);
    m_cAddrEdit.SetWindowText(szCode);
    szCode.Format(_T("%x"),m_pDloadConfig->GetReadSize());
    m_cSizeEdit.SetLimitText(8);
    m_cSizeEdit.SetWindowText(szCode);
    m_cMemDumpCheck.SetCheck(m_pDloadConfig->IsMemDump());
    return TRUE;
}

void CDloadSettingDlg::UpdateConfig(void)
{
    m_cNVEdit.SetWindowText(m_pDloadConfig->GetNVPath());
    m_cNVFileEdit.SetWindowText(m_pDloadConfig->GetNVFile());
    m_cEFSEdit.SetWindowText(m_pDloadConfig->GetEFSPath());
    m_cNVBackupCheck.SetCheck(m_pDloadConfig->IsNVBackup());
    m_cNVRestoreCheck.SetCheck(m_pDloadConfig->IsNVRestore());
    m_cEFSBackupCheck.SetCheck(m_pDloadConfig->IsEFSBackup());
    m_cEFSRestoreCheck.SetCheck(m_pDloadConfig->IsEFSRestore());
    m_cMBNCheck.SetCheck(m_pDloadConfig->IsDloadMBN());
    m_cFACTCheck.SetCheck(m_pDloadConfig->IsDloadFACT());
    m_cEraseCheck.SetCheck(m_pDloadConfig->IsEraseFlash());

    m_cAMSSCheck.SetCheck(m_pDloadConfig->IsDloadAMSS());
    m_cCEFSCheck.SetCheck(m_pDloadConfig->IsDloadCEFS());
    m_cDBLCheck.SetCheck(m_pDloadConfig->IsDloadDBL());
    m_cFSBLCheck.SetCheck(m_pDloadConfig->IsDloadFSBL());
    m_cOSBLCheck.SetCheck(m_pDloadConfig->IsDloadOSBL());
    m_cAPPSBLCheck.SetCheck(m_pDloadConfig->IsDloadAPPSBL());
    m_cAPPSCheck.SetCheck(m_pDloadConfig->IsDloadAPPS());
    m_cAPPSCEFSCheck.SetCheck(m_pDloadConfig->IsDloadAPPSCEFS());
    m_cOBLCheck.SetCheck(m_pDloadConfig->IsDloadOBL());
    m_cPBLCheck.SetCheck(m_pDloadConfig->IsDloadPBL());
    m_cQCSBLCheck.SetCheck(m_pDloadConfig->IsDloadQCSBL());
    m_cOEMSBLCheck.SetCheck(m_pDloadConfig->IsDloadOEMSBL());
    m_cFOTAUICheck.SetCheck(m_pDloadConfig->IsDloadFOTAUI());
    m_cDSP1Check.SetCheck(m_pDloadConfig->IsDloadDSP1());
    m_cDSP2Check.SetCheck(m_pDloadConfig->IsDloadDSP2());
    m_cFLASHBINCheck.SetCheck(m_pDloadConfig->IsDloadFLASHBIN());
    m_cRAWCheck.SetCheck(m_pDloadConfig->IsDloadRAW());
    m_cCUSTOMCheck.SetCheck(m_pDloadConfig->IsDloadCUSTOM());
    m_cARDCheck.SetCheck(m_pDloadConfig->IsDloadARD());
    m_cARDBOOTCheck.SetCheck(m_pDloadConfig->IsDloadARDBOOT());
    m_cARDSYSCheck.SetCheck(m_pDloadConfig->IsDloadARDSYS());
    m_cARDUSERCheck.SetCheck(m_pDloadConfig->IsDloadARDUSER());
    m_cARDRCYCheck.SetCheck(m_pDloadConfig->IsDloadARDRCY());
    m_cARDCLRCACHECheck.SetCheck(m_pDloadConfig->IsDloadARDCLRCACHE());
    m_cLogEnableCheck.SetCheck(m_pDloadConfig->IsLogEnable());
    m_cLockedCheck.SetCheck(m_pDloadConfig->IsLocked());
    m_cMsimageCheck.SetCheck(m_pDloadConfig->IsDloadMsimage());

    m_cFACTCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()!=CHIPSET_MULTIBOOT_NONE&&!m_pDloadConfig->IsEMMC());
    m_cEraseCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()==CHIPSET_MULTIBOOT_NONE&&!m_pDloadConfig->IsEMMC());
    m_cDBLCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()==CHIPSET_MULTIBOOT_2&&!m_pDloadConfig->IsEMMC());
    m_cFSBLCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()==CHIPSET_MULTIBOOT_2&&!m_pDloadConfig->IsEMMC());
    m_cOSBLCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()==CHIPSET_MULTIBOOT_2&&!m_pDloadConfig->IsEMMC());
    m_cAMSSCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cCEFSCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cAPPSBLCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetAPPSBLName().IsEmpty()&&!m_pDloadConfig->IsEMMC());
    m_cAPPSCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN());
    m_cAPPSCEFSCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN());
    m_cOBLCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cPBLCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()==CHIPSET_MULTIBOOT_1&&!m_pDloadConfig->GetPBLName().IsEmpty()&&!m_pDloadConfig->IsEMMC());
    m_cQCSBLCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()==CHIPSET_MULTIBOOT_1&&!m_pDloadConfig->IsEMMC());
    m_cOEMSBLCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()==CHIPSET_MULTIBOOT_1&&!m_pDloadConfig->IsEMMC());
    m_cFOTAUICheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cDSP1Check.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cDSP2Check.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cFLASHBINCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cRAWCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cCUSTOMCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->IsEMMC());
    m_cARDCheck.EnableWindow(!m_pDloadConfig->GetARDBOOTName().IsEmpty());
    m_cARDBOOTCheck.EnableWindow(m_pDloadConfig->IsDloadARD()&&!m_pDloadConfig->GetARDBOOTName().IsEmpty());
    m_cARDSYSCheck.EnableWindow(m_pDloadConfig->IsDloadARD()&&!m_pDloadConfig->GetARDSYSName().IsEmpty());
    m_cARDUSERCheck.EnableWindow(m_pDloadConfig->IsDloadARD()&&!m_pDloadConfig->GetARDUSERName().IsEmpty());
    m_cARDRCYCheck.EnableWindow(m_pDloadConfig->IsDloadARD()&&!m_pDloadConfig->GetARDRCYName().IsEmpty());
    m_cARDCLRCACHECheck.EnableWindow(m_pDloadConfig->IsDloadARD());
    m_cAddrEdit.EnableWindow(m_pDloadConfig->IsMemDump());
    m_cSizeEdit.EnableWindow(m_pDloadConfig->IsMemDump());
    m_cMsimageCheck.EnableWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()==CHIPSET_MULTIBOOT_1&&m_pDloadConfig->IsEMMC());
    
    m_cAPPSCheck.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetAPPSName().IsEmpty());
    m_cAPPSCEFSCheck.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetAPPSName().IsEmpty());
    m_cOBLCheck.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetAPPSCEFSName().IsEmpty());
    m_cFOTAUICheck.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetFOTAUIName().IsEmpty());
    m_cDSP1Check.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetDSP1Name().IsEmpty());
    m_cDSP2Check.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetDSP2Name().IsEmpty());
    m_cFLASHBINCheck.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetFLASHBINName().IsEmpty());
    m_cRAWCheck.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetRAWName().IsEmpty());
    m_cCUSTOMCheck.ShowWindow(m_pDloadConfig->IsDloadMBN()&&m_pDloadConfig->IsMultiMBN()&&!m_pDloadConfig->GetCUSTOMName().IsEmpty());
}

void CDloadSettingDlg::SyncConfig(void)
{
    CString szText;
    m_cNVEdit.GetWindowText(szText);
    m_pDloadConfig->SetNVPath(szText);
    m_cNVFileEdit.GetWindowText(szText);
    m_pDloadConfig->SetNVFile(szText);
    m_cEFSEdit.GetWindowText(szText);
    m_pDloadConfig->SetEFSPath(szText);
    m_pDloadConfig->SetNVBackup(m_cNVBackupCheck.GetCheck());
    m_pDloadConfig->SetNVRestore(m_cNVRestoreCheck.GetCheck());
    m_pDloadConfig->SetEFSBackup(m_cEFSBackupCheck.GetCheck());
    m_pDloadConfig->SetEFSRestore(m_cEFSRestoreCheck.GetCheck());
    m_pDloadConfig->SetDloadMBN(m_cMBNCheck.GetCheck());
    m_pDloadConfig->SetDloadFACT(m_cFACTCheck.GetCheck());
    m_pDloadConfig->SetEraseFlash(m_cEraseCheck.GetCheck());
    m_pDloadConfig->SetDloadPBL(m_cPBLCheck.GetCheck());
    m_pDloadConfig->SetDloadQCSBL(m_cQCSBLCheck.GetCheck());
    m_pDloadConfig->SetDloadOEMSBL(m_cOEMSBLCheck.GetCheck());
    m_pDloadConfig->SetDloadAMSS(m_cAMSSCheck.GetCheck());
    m_pDloadConfig->SetDloadAPPS(m_cAPPSCheck.GetCheck());
    m_pDloadConfig->SetDloadOBL(m_cOBLCheck.GetCheck());
    m_pDloadConfig->SetDloadFOTAUI(m_cFOTAUICheck.GetCheck());
    m_pDloadConfig->SetDloadCEFS(m_cCEFSCheck.GetCheck());
    m_pDloadConfig->SetDloadAPPSBL(m_cAPPSBLCheck.GetCheck());
    m_pDloadConfig->SetDloadAPPSCEFS(m_cAPPSCEFSCheck.GetCheck());
    m_pDloadConfig->SetDloadFLASHBIN(m_cFLASHBINCheck.GetCheck());
    m_pDloadConfig->SetDloadDSP1(m_cDSP1Check.GetCheck());
    m_pDloadConfig->SetDloadCUSTOM(m_cCUSTOMCheck.GetCheck());
    m_pDloadConfig->SetDloadDBL(m_cDBLCheck.GetCheck());
    m_pDloadConfig->SetDloadFSBL(m_cFSBLCheck.GetCheck());
    m_pDloadConfig->SetDloadOSBL(m_cOSBLCheck.GetCheck());
    m_pDloadConfig->SetDloadDSP2(m_cDSP2Check.GetCheck());
    m_pDloadConfig->SetDloadRAW(m_cRAWCheck.GetCheck());
    m_pDloadConfig->SetDloadARD(m_cARDCheck.GetCheck());
    m_pDloadConfig->SetDloadARDBOOT(m_cARDBOOTCheck.GetCheck());
    m_pDloadConfig->SetDloadARDSYS(m_cARDSYSCheck.GetCheck());
    m_pDloadConfig->SetDloadARDUSER(m_cARDUSERCheck.GetCheck());
    m_pDloadConfig->SetDloadARDRCY(m_cARDRCYCheck.GetCheck());
    m_pDloadConfig->SetDloadARDCLRCACHE(m_cARDCLRCACHECheck.GetCheck());
    m_pDloadConfig->SetLogEnable(m_cLogEnableCheck.GetCheck());
    m_pDloadConfig->SetLocked(m_cLockedCheck.GetCheck());
    m_pDloadConfig->SetMemDump(m_cMemDumpCheck.GetCheck());
    m_pDloadConfig->SetDloadMsimage(m_cMsimageCheck.GetCheck());

    CString szCode;
    m_cAddrEdit.GetWindowText(szCode);
    m_pDloadConfig->SetStartAddr(_tcstol(szCode, NULL, 16));
    m_cSizeEdit.GetWindowText(szCode);
    m_pDloadConfig->SetReadSize(_tcstol(szCode, NULL, 16));
}

void CDloadSettingDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_NVPATH_EDIT, m_cNVEdit);
    DDX_Control(pDX, IDC_EFSPATH_EDIT, m_cEFSEdit);
    DDX_Control(pDX, IDC_NVBACKUP_CHECK, m_cNVBackupCheck);
    DDX_Control(pDX, IDC_NVRESTORE_CHECK, m_cNVRestoreCheck);
    DDX_Control(pDX, IDC_EFSBACKUP_CHECK, m_cEFSBackupCheck);
    DDX_Control(pDX, IDC_EFSRESTORE_CHECK, m_cEFSRestoreCheck);
    DDX_Control(pDX, IDC_MBN_CHECK, m_cMBNCheck);
    DDX_Control(pDX, IDC_ERASE_CHECK, m_cEraseCheck);
    DDX_Control(pDX, IDC_DBL_CHECK, m_cDBLCheck);
    DDX_Control(pDX, IDC_FSBL_CHECK, m_cFSBLCheck);
    DDX_Control(pDX, IDC_OSBL_CHECK, m_cOSBLCheck);
    DDX_Control(pDX, IDC_AMSS_CHECK, m_cAMSSCheck);
    DDX_Control(pDX, IDC_CEFS_CHECK, m_cCEFSCheck);
    DDX_Control(pDX, IDC_NVFILE_EDIT, m_cNVFileEdit);
    DDX_Control(pDX, IDC_APPSBL_CHECK, m_cAPPSBLCheck);
    DDX_Control(pDX, IDC_APPS_CHECK, m_cAPPSCheck);
    DDX_Control(pDX, IDC_APPSCEFS_CHECK, m_cAPPSCEFSCheck);
    DDX_Control(pDX, IDC_OBL_CHECK, m_cOBLCheck);
    DDX_Control(pDX, IDC_PBL_CHECK, m_cPBLCheck);
    DDX_Control(pDX, IDC_QCSBL_CHECK, m_cQCSBLCheck);
    DDX_Control(pDX, IDC_OEMSBL_CHECK, m_cOEMSBLCheck);
    DDX_Control(pDX, IDC_FOTAUI_CHECK, m_cFOTAUICheck);
    DDX_Control(pDX, IDC_DSP1_CHECK, m_cDSP1Check);
    DDX_Control(pDX, IDC_DSP2_CHECK, m_cDSP2Check);
    DDX_Control(pDX, IDC_FLASHBIN_CHECK, m_cFLASHBINCheck);
    DDX_Control(pDX, IDC_RAW_CHECK, m_cRAWCheck);
    DDX_Control(pDX, IDC_CUSTOM_CHECK, m_cCUSTOMCheck);
    DDX_Control(pDX, IDC_ARDBOOT_CHECK, m_cARDBOOTCheck);
    DDX_Control(pDX, IDC_ARDSYS_CHECK, m_cARDSYSCheck);
    DDX_Control(pDX, IDC_ARD_CHECK, m_cARDCheck);
    DDX_Control(pDX, IDC_ARDUSER_CHECK, m_cARDUSERCheck);
    DDX_Control(pDX, IDC_ARDRCY_CHECK, m_cARDRCYCheck);
    DDX_Control(pDX, IDC_LOGENABLE_CHECK, m_cLogEnableCheck);
    DDX_Control(pDX, IDC_LOCKED_CHECK, m_cLockedCheck);
    DDX_Control(pDX, IDC_MEMDUMP_CHECK, m_cMemDumpCheck);
    DDX_Control(pDX, IDC_ADDR_EDIT, m_cAddrEdit);
    DDX_Control(pDX, IDC_SIZE_EDIT, m_cSizeEdit);
    DDX_Control(pDX, IDC_ARDCLRCACHE_CHECK, m_cARDCLRCACHECheck);
    DDX_Control(pDX, IDC_FACT_CHECK, m_cFACTCheck);
    DDX_Control(pDX, IDC_MSIMAGE_CHECK, m_cMsimageCheck);
}


BEGIN_MESSAGE_MAP(CDloadSettingDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CDloadSettingDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDC_DEFAULT_BUTTON, &CDloadSettingDlg::OnBnClickedDefaultButton)
    ON_BN_CLICKED(IDC_NVPATH_BUTTON, &CDloadSettingDlg::OnBnClickedNvpathButton)
    ON_BN_CLICKED(IDC_EFS_BUTTON, &CDloadSettingDlg::OnBnClickedEfsButton)
    ON_BN_CLICKED(IDC_NVFILE_BUTTON, &CDloadSettingDlg::OnBnClickedNvfileButton)
    ON_WM_TIMER()
    ON_BN_CLICKED(IDC_MBN_CHECK, &CDloadSettingDlg::OnBnClickedMbnCheck)
//    ON_WM_SETFOCUS()
ON_WM_ACTIVATE()
ON_BN_CLICKED(IDC_ARD_CHECK, &CDloadSettingDlg::OnBnClickedArdCheck)
ON_EN_CHANGE(IDC_ADDR_EDIT, &CDloadSettingDlg::OnEnChangeAddrEdit)
ON_EN_CHANGE(IDC_SIZE_EDIT, &CDloadSettingDlg::OnEnChangeSizeEdit)
ON_BN_CLICKED(IDC_MEMDUMP_CHECK, &CDloadSettingDlg::OnBnClickedMemdumpCheck)
ON_BN_CLICKED(IDC_FACT_CHECK, &CDloadSettingDlg::OnBnClickedFactCheck)
END_MESSAGE_MAP()


// CDloadSettingDlg 消息处理程序

void CDloadSettingDlg::OnBnClickedOk()
{
    SyncConfig();
    OnOK();
}

void CDloadSettingDlg::OnBnClickedDefaultButton()
{
    // TODO: 在此添加控件通知处理程序代码
    m_pDloadConfig->ResetConfig();
    UpdateConfig();
}

void CDloadSettingDlg::OnBnClickedNvpathButton()
{
    CString     szTemp;
    szTemp.LoadString(IDS_TITLE_NVPATH);
    CFolderDialog myFolderDlg(CSIDL_DESKTOP, m_pDloadConfig->GetNVPath(), szTemp, BIF_RETURNFSANCESTORS | BIF_RETURNONLYFSDIRS);
    if(myFolderDlg.DoModal() == IDOK)
    {
        m_cNVEdit.SetWindowText(myFolderDlg.GetPath());
    }
}

void CDloadSettingDlg::OnBnClickedEfsButton()
{
    CString     szTemp;
    szTemp.LoadString(IDS_TITLE_EFSPATH);
    CFolderDialog myFolderDlg(CSIDL_DESKTOP, m_pDloadConfig->GetEFSPath(), szTemp, BIF_RETURNFSANCESTORS | BIF_RETURNONLYFSDIRS);
    if(myFolderDlg.DoModal() == IDOK)
    {
        m_cEFSEdit.SetWindowText(myFolderDlg.GetPath());
    }
}

void CDloadSettingDlg::OnBnClickedNvfileButton()
{
	CString     szFileName;
	CString     szFilter;
	CString     szFilterTemp;
	
    szFilterTemp.LoadStringW(IDS_FILTER_QCN);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    szFilterTemp.LoadStringW(IDS_FILTER_QCN_EXT);
	szFilter += szFilterTemp;
    szFilter += _T("|");
    
    //no more so append final pipe
	szFilter += _T("|");

	m_cNVFileEdit.GetWindowText(szFileName);
    CFileDialog myFileDlg( TRUE, 
		                   NULL, 
						   szFileName, 
						   OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR,
						   szFilter,
						   NULL);

    if(myFileDlg.DoModal() == IDOK)
    {
        m_cNVFileEdit.SetWindowText(myFileDlg.GetPathName());
    }
}

void CDloadSettingDlg::OnTimer(UINT_PTR nIDEvent)
{
    if(CDLOADSETTING_REFRESH_TIMER == nIDEvent)
    {
        m_cMBNCheck.EnableWindow(FALSE);
        m_cMBNCheck.EnableWindow(TRUE);
        m_cARDCheck.EnableWindow(FALSE);
        m_cARDCheck.EnableWindow(!m_pDloadConfig->GetARDBOOTName().IsEmpty());
        KillTimer(CDLOADSETTING_REFRESH_TIMER);
    }
    CDialog::OnTimer(nIDEvent);
}

void CDloadSettingDlg::OnBnClickedMbnCheck()
{
    m_pDloadConfig->SetDloadMBN(m_cMBNCheck.GetCheck());
    SyncConfig();
    UpdateConfig();
}

//void CDloadSettingDlg::OnSetFocus(CWnd* pOldWnd)
//{
//    CDialog::OnSetFocus(pOldWnd);
//
//    SetTimer(CDLOADSETTING_REFRESH_TIMER,50,NULL);
//}

void CDloadSettingDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
    CDialog::OnActivate(nState, pWndOther, bMinimized);

    SetTimer(CDLOADSETTING_REFRESH_TIMER,50,NULL);
}

void CDloadSettingDlg::OnBnClickedArdCheck()
{
    m_pDloadConfig->SetDloadARD(m_cARDCheck.GetCheck());
    SyncConfig();
    UpdateConfig();
}

void CDloadSettingDlg::OnEnChangeAddrEdit()
{
    // TODO:  如果该控件是 RICHEDIT 控件，则它将不会
    // 发送该通知，除非重写 CDialog::OnInitDialog()
    // 函数并调用 CRichEditCtrl().SetEventMask()，
    // 同时将 ENM_CHANGE 标志“或”运算到掩码中。
    CString szCode;
	
	m_cAddrEdit.GetWindowText(szCode);
    if(!IsCodeAvaliable(szCode))
    {
		if(szCode.GetLength()>0)
		{
			szCode.Delete(szCode.GetLength()-1);
			m_cAddrEdit.SetWindowText(szCode);
			m_cAddrEdit.SetSel(szCode.GetLength(),szCode.GetLength());
		}
	}
}

void CDloadSettingDlg::OnEnChangeSizeEdit()
{
    // TODO:  如果该控件是 RICHEDIT 控件，则它将不会
    // 发送该通知，除非重写 CDialog::OnInitDialog()
    // 函数并调用 CRichEditCtrl().SetEventMask()，
    // 同时将 ENM_CHANGE 标志“或”运算到掩码中。
	CString szCode;
	
	m_cSizeEdit.GetWindowText(szCode);
    if(!IsCodeAvaliable(szCode))
    {
		if(szCode.GetLength()>0)
		{
			szCode.Delete(szCode.GetLength()-1);
			m_cSizeEdit.SetWindowText(szCode);
			m_cSizeEdit.SetSel(szCode.GetLength(),szCode.GetLength());
		}
	}
}

bool CDloadSettingDlg::IsCodeAvaliable(CString &szCode)
{
	if(szCode.IsEmpty())
	{
		return false;
	}

	if(szCode.GetLength()>8)
	{
		return false;
	}
	
	for(int i=0; i<szCode.GetLength(); i++)
	{
		TCHAR c = szCode.GetAt(i);
		if((c>_T('9') || c<_T('0'))&&(c>_T('F') || c<_T('A')))
		{
			return false;
		}
	}
	return true;
}
void CDloadSettingDlg::OnBnClickedMemdumpCheck()
{
    m_pDloadConfig->SetMemDump(m_cMemDumpCheck.GetCheck());
    SyncConfig();
    UpdateConfig();
}

void CDloadSettingDlg::OnBnClickedFactCheck()
{
    m_pDloadConfig->SetDloadFACT(m_cFACTCheck.GetCheck());
    SyncConfig();
    UpdateConfig();
}
