// PortInfoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "SWDownloader.h"
#include "PortInfoDlg.h"

#define REFRESH_TIMER_ID    0x80000000
#define REFRESH_TIME_MS     1000

#define PORTINFO_COL_COM        0
#define PORTINFO_COL_MODE       1
#define PORTINFO_COL_USED       2
#define PORTINFO_COL_HUBIDX     3
#define PORTINFO_COL_PORTIDX    4
// CPortInfoDlg 对话框

IMPLEMENT_DYNAMIC(CPortInfoDlg, CDialog)

CPortInfoDlg::CPortInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPortInfoDlg::IDD, pParent)
{
    COMManager::Create(&m_pCOMMgr);
}

CPortInfoDlg::~CPortInfoDlg()
{
    COMManager::Release();
}

BOOL CPortInfoDlg::OnInitDialog()
{
    CDialog::OnInitDialog();
    CString szTemp;
    szTemp.LoadString(IDS_PORTS_INFORMATION);
    SetWindowText(szTemp);

    m_cPortInfoList.SetExtendedStyle(m_cPortInfoList.GetExtendedStyle()|LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT|LVS_EX_FLATSB);

    szTemp.LoadString(IDS_COL_COM);
    m_cPortInfoList.InsertColumn(PORTINFO_COL_COM,szTemp,LVCFMT_CENTER,60);
    szTemp.LoadString(IDS_COL_MODE);
    m_cPortInfoList.InsertColumn(PORTINFO_COL_MODE,szTemp,LVCFMT_CENTER,120);
    szTemp.LoadString(IDS_USED);
    m_cPortInfoList.InsertColumn(PORTINFO_COL_USED,szTemp,LVCFMT_CENTER,60);
    szTemp.LoadString(IDS_HUBINDEX);
    m_cPortInfoList.InsertColumn(PORTINFO_COL_HUBIDX,szTemp,LVCFMT_CENTER,80);
    szTemp.LoadString(IDS_PORTINDEX);
    m_cPortInfoList.InsertColumn(PORTINFO_COL_PORTIDX,szTemp,LVCFMT_CENTER,80);
    UpdatePortInfo();
    SetTimer(REFRESH_TIMER_ID,REFRESH_TIME_MS,NULL);
    return TRUE;
}

void CPortInfoDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_PORTINFO_LIST, m_cPortInfoList);
}


BEGIN_MESSAGE_MAP(CPortInfoDlg, CDialog)
    ON_WM_TIMER()
END_MESSAGE_MAP()


// CPortInfoDlg 消息处理程序

void CPortInfoDlg::OnTimer(UINT_PTR nIDEvent)
{
    if(nIDEvent == REFRESH_TIMER_ID)
    {
        UpdatePortInfo();
    }
    CDialog::OnTimer(nIDEvent);
}

void CPortInfoDlg::UpdatePortInfo(void)
{
    CString szTemp;
    int nIndex = 0;

    m_cPortInfoList.DeleteAllItems();
    for(int i = COM1;i < COM_MAX;i++)
    {
        if(m_pCOMMgr->IsPortExist(i))
        {
            szTemp.Format(_T("COM%d"),i+1);
            m_cPortInfoList.InsertItem(nIndex,szTemp);
            m_cPortInfoList.SetItemText(nIndex,PORTINFO_COL_MODE,GetModeString(m_pCOMMgr->GetPhoneMode(i)));
            if(m_pCOMMgr->GetUsed(i))
            {
                szTemp.LoadString(IDS_YES);
            }
            else
            {
                szTemp.LoadString(IDS_NO);
            }
            m_cPortInfoList.SetItemText(nIndex,PORTINFO_COL_USED,szTemp);
            szTemp.Format(_T("%d"),m_pCOMMgr->GetHubIndex(i));
            m_cPortInfoList.SetItemText(nIndex,PORTINFO_COL_HUBIDX,szTemp);
            szTemp.Format(_T("%d"),m_pCOMMgr->GetPortIndex(i));
            m_cPortInfoList.SetItemText(nIndex,PORTINFO_COL_PORTIDX,szTemp);
            nIndex++;
        }
    }
}

CString CPortInfoDlg::GetModeString(int nMode)
{
    CString szMode;
    switch(nMode){
    case DIAG_PHONEMODE_CDMA:
    case DIAG_PHONEMODE_WCDMA:
        szMode.LoadString(IDS_COM_CONNECT);
        break;
        
    case DIAG_PHONEMODE_DLOAD:
    case DIAG_PHONEMODE_EDLOAD:
        szMode.LoadString(IDS_COM_DLOAD);
        break;
        
    case DIAG_PHONEMODE_ARMPRG:
        szMode.LoadString(IDS_COM_ARMPRG);
        break;
        
    case DIAG_PHONEMODE_FASTBOOT:
        szMode.LoadString(IDS_COM_FASTBOOT);
        break;
        
    case DIAG_PHONEMODE_NONE:
    default:
        szMode.LoadString(IDS_COM_NOPHONE);
        break;
    }
    return szMode;
}
