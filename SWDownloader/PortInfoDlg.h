#pragma once
#include "afxcmn.h"
#include "COMManager.h"

// CPortInfoDlg 对话框

class CPortInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CPortInfoDlg)

public:
	CPortInfoDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CPortInfoDlg();
    virtual BOOL OnInitDialog();
    

// 对话框数据
	enum { IDD = IDD_PORTINFO_DIALOG };
private:
    void            UpdatePortInfo(void);
    CString         GetModeString(int nMode);
    
    COMManager     *m_pCOMMgr;
    
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
    CListCtrl m_cPortInfoList;
    afx_msg void OnTimer(UINT_PTR nIDEvent);
};
